# guild.xyz interface

Open source interface for Guild.xyz -- a tool for token-curated communities, powered by Agora.xyz.

- Website: [guild.xyz](https://guild.xyz)
- Docs: [docs.guild.xyz](https://docs.guild.xyz/)
- Twitter: [@Guildxyz](https://twitter.com/guildxyz)
- Email: [contact@agora.space](mailto:contact@agora.space)
- Discord: [Guild.xyz](https://discord.gg/guildxyz)

## Contributions

For steps on local deployment, development, and code contribution, please see [CONTRIBUTING](./CONTRIBUTING.md).

## Dependencies overview

- Next.js
- Chakra UI
- State management:
  - SWR for server and blockchain state (fetching and caching)
  - React Hook Form for form state
- Web3 stuff:
  - ethers.js
  - web3-react for connection management

SSL User : web2 app login:sso id,user name ,email
MPC User : keypair user:sso id,user name,email,private key,public key,ethAddr,cotaAddr
SSO User + MPC User -> User

User ->Groups ->NFT Define
              ->NFT Define2

Group: name, desc,id,total user,cur totol user
NFT Define:img url,name,desc,total