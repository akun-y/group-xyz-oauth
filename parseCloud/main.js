//require docker 服务器路径
const membership = require('/parse-server/cloud/membership.js');
const utils = require('/parse-server/cloud/utils.js');
const user = require('/parse-server/cloud/user.js');
const group = require('/parse-server/cloud/group.js');
const pinata = require('/parse-server/cloud/pinata.js');

var serverIP = "YOUR_SERVER_IP_HERE";
var serviceName = "YOUR_SERVER_NAME_HERE";
var apiKey = "YOUR_API_KEY_HERE";
var sessionToken = "SESSION_TOKEN_HERE";
let results = [];
//--------------------------------------------------------------
//Parase server cloud main.js 使用中的代码
//--------------------------------------------------------------

//通过 ownerId(ssoID) 或 urlName 查询用户群组信息,只会返回一个
Parse.Cloud.define("queryGroup", (request) => group.query(request));

Parse.Cloud.define("deleteGroup", (request) => group.del(request));
Parse.Cloud.define("groupDetail", (request) => group.detail(request));

//测试返回指定的field(结果有用，email为特殊之段，不输出)
Parse.Cloud.define("getFriends", function (request, response) {
    var userId = request.params.UserKey;
    var query = new Parse.Query(Parse.User);
    query.ascending("updatedAt");
    query.select("username", "email", "isSuperAdmin"); // replace with your required fields
    return query.find()
});
//测试返回指定的field(结果有用)
Parse.Cloud.define("getGroupTest", function (request, response) {
    var query = new Parse.Query("Groups");
    query.ascending("updatedAt");
    query.select("name", "urlName", "description"); // replace with your required fields
    return query.first()
});
//返回所有group list
Parse.Cloud.define("queryGroups", (request) => group.queryAll(request));
//查询用户的所有群组
Parse.Cloud.define("queryMembership", function (request) {
    console.info("queryMembership:", request.params);
    return membership.getMemberShip();
});
//查询Access 
//输入参数：groupId,address
//返回：access
Parse.Cloud.define("groupAccess", async function (request) {
    console.info("groupAccess:", request.params);

    let results = [{ "roleId": 1904, "access": true, "requirements": [{ "requirementId": 5363, "access": true, "amount": 2 }] }];
    // const query = new Parse.Query("Access");
    // if (!!request.params.ownerId) {
    //     query.equalTo("ownerId", request.params.ownerId);
    //     if (request.params.platformName) {
    //         query.containedIn("platforms", [request.params.platformName]);
    //     }
    //     results = await query.find();
    // }

    console.info("queryGroups results.length:" + results?.length);

    return results;
});
Parse.Cloud.define("pinata-key", (request) => pinata.genkey(request));
//通过 id 或 urlName查询用户群组详细信息
Parse.Cloud.define("groupsDetail", async function (request) {
    console.info("queryGroupsDetail:", request.params);

    let results = [];
    const query = new Parse.Query("GroupsDetail");
    if (!!request.params.id) {
        query.equalTo("objectId", request.params.id);
        results = await query.find();
    } else if (!!request.params.urlName) {
        query.equalTo("urlName", request.params.urlName);
        results = await query.find();
    }
    console.info("results.length:" + results?.length);

    return results?.[0];
});
Parse.Cloud.define("updateGroup", (request) => group.update(request));

Parse.Cloud.define("userStatusUpdate", (request) => user.statusUpdate(request));

//通过address 或者 gateId查询用户信息
Parse.Cloud.define("queryUser", (request) => user.query(request))
//用户加入群组
Parse.Cloud.define("userJoin", (request) => user.join(request.params));

//是否超级用户
Parse.Cloud.define("isSuperAdmin", async function (request) {
    console.info("isSuperAdmin:", request.params);
    let results = [];
    if (!!request.params.ssoID) {
        const query = new Parse.Query("Users");
        query.equalTo("ssoID", request.params.ssoID);
        results = await query.find();
    } else if (!!request.params.address) {
        const query = new Parse.Query("Users");
        query.containedIn('addresses', [request.params.address]);
        results = await query.find();
    }
    console.info("results.length:" + results?.length);
    if (results?.length > 0) {
        return true;
    }
    return false;
});
//test
Parse.Cloud.define("test123", async function (request) {
    console.info("test123:", request.params);


    var dietPlan = ParseObject('DietPlan')
    dietPlan.set('Name', 'Ketogenic')
    dietPlan.set('Fat', 65);

    dietPlan.setIncrement('count', 1);
    await dietPlan.save();
    //dietPlan.set<int>('RandomInt', 8);
    //var randomInt = dietPlan.get<int>('RandomInt');




    return "test123";
});

//--------------------------------------------------------------
//Event
Parse.Cloud.beforeSave("Users", async (request) => {
    console.log("Users request: " + request);
    const myObject = request.object;

    if (!myObject.isNew()) {
        return "Users is not new";
    }
    //确保用户不会重复创建，关键字：ssoID
    const query = new Parse.Query("Users");
    query.equalTo("ssoID", myObject.get("ssoID"));
    return query.count().then(async (count) => {
        if (count > 0) {
            throw "Users Gate ID already exists";
        }
        //实现自增的Number ID
        const id = await utils.getUID("Users");
        myObject.increment("id", id);
    });
}, {
    fields: {
        ssoID: {
            required: true,
            options: ssoID => {
                console.log("ssoID: " + ssoID);
                if (ssoID?.length < 36) {
                    throw "ssoID is required, and length must be greater than 36";
                }

            },
            error: "ssoID is required, and length must be greater than 36"
        }
    }
});
// Parse.Cloud.afterSave("Users", (request) => {
//     const query = new Parse.Query("ggg");
//     query.get("Bies6y2PRp")
//         .then(function (post) {
//             console.info("---------------2:", post);
//             post.increment("userId");
//             return post.save();
//         })
//         .catch(function (error) {
//             console.error("Got an error " + error.code + " : " + error.message);
//         });
// });
Parse.Cloud.beforeSave("Groups", (request) => {
    console.log("===beforSave Groups request: " + JSON.stringify(request));

    const myObject = request.object;
    if (!myObject.isNew())
        return "Groups is not new";
    const { v4: uuidv4 } = require('uuid');
    // only check new records, else its a update
    var query = new Parse.Query("Groups");
    query.equalTo("name", myObject.get("name"));
    return query.count().then(async (count) => {
        if (count > 0) {
            throw "Group name already exists";
        }
        myObject.set("urlName", uuidv4());
        //实现自增的Number ID
        const id = await utils.getUID("Groups");
        myObject.increment("id", id);
    });
}, {
    fields: {
        name: {
            required: true,
            options: name => {
                console.log("name: " + name);
                if (name == null || name == undefined
                    || name == "" || name.length < 5) {
                    throw "Group name is required";
                }

            },
            error: "Group name is required and must be at least 5 characters long"
        }
    }
});
Parse.Cloud.beforeSave("Roles", (request) => {
    console.log("===beforSave Roles request: " + JSON.stringify(request));

    const myObject = request.object;
    if (!myObject.isNew())
        return "Role is not new";
    var query = new Parse.Query("Roles");
    query.equalTo("name", myObject.get("name"));
    return query.count().then(async (count) => {
        if (count > 0) {
            //throw "Role name already exists";//允许名字重复，每个role属于不同的group
            console.error("Role name already exists");
        }
        //实现自增的Number ID
        const id = await utils.getUID("Roles");
        myObject.increment("id", id);
    });
}, {
    fields: {
        name: {
            required: true,
            options: name => {
                console.log("name: " + name);
                if (name == null || name == undefined
                    || name == "" || name.length < 5) {
                    throw "Role name is required";
                }

            },
            error: "Roles name is required and must be at least 5 characters long"
        }
    }
});
//已经改由 group-api-server 实现
//保存或变更role时，需要更新rewards的roleIds,用于删除reward时，确保已经无role引用。
//添加roleIds发生在新建reward和查询role时
//删除roleIds发生在保存role时，也就是这个函数
// Parse.Cloud.afterSave("Roles", (request) => {
//     console.log("===afterSave Roles request: " + JSON.stringify(request));
//     const myObject = request.object;

//     const roleId = myObject.get("id")?.toString();
//     const rewardList = myObject.get("rolePlatforms");
//     if (!rewardList || !roleId)
//         return;

//     const query = new Parse.Query("Rewards");
//     rewardList.forEach(async (r) => {
//         const rewardObj = await query.equalTo("id", r.guildPlatformId);
//         const roleIds = rewardObj?.get("roleIds");

//         //更新roleIds中的 roleId
//         const newIds = [];
//         roleIds.forEach(async (roleId) => {
//             if (roleId == roleId) {
//                 console.info("forEach next: " + roleId);
//                 newIds.push(roleId);
//                 next;
//             }

//             console.info("roleIds: " + roleId);
//             const query = new Parse.Query("Roles");
//             query.equalTo("id", Number(roleId));
//             const role = await query.first();
//             const rewardList = role?.get("rolePlatforms");
//             if (!rewardList) {
//                 const rewardIds = rewardList.map(r => r.guildPlatformId);
//                 if (rewardIds.includes(roleId)) {
//                     roleIds.push(roleId)
//                 }
//             }
//         });
//         rewardObj.set("roleIds", newIds);
//         await rewardObj.save();
//     });
// });
Parse.Cloud.beforeSave("Rewards", async (request) => {
    console.log("===beforSave Rewards request: " + JSON.stringify(request));

    const myObject = request.object;
    if (!myObject.isNew())
        return "Rewards is not new";

    //实现自增的Number ID
    const id = await utils.getUID("Rewards");
    myObject.increment("id", id);
}, {
    fields: {
        groupId: {
            required: true,
            options: groupId => {
                if (!groupId) {
                    throw "groupId is required";
                }

            },
            error: "Reward groupId is required"
        }
    }
});
//--------------------------------------------------------------
//Parse.Cloud.job
//--------------------------------------------------------------
// Parse.Cloud.job("sendReport", function(request, response) {
//     Parse.Cloud.httpRequest({
//     method: 'POST',
//     headers: {
//      'Content-Type': 'application/json',
//     },
//     url: "https://example.com/url/", // Webhook url
//     body: "body goes here",
//     success: function(httpResponse) {
//         console.log("Successfully POSTed to the webhook");
//         },
//     error: function(httpResponse) {
//         console.error("Couldn't POST to webhook: " + httpResponse);
//         }
//     });
//   });
//--------------------------------------------------------------
//Parase server cloud main.js 使用中的代码
//--------------------------------------------------------------
/*
    NoSQL Based
*/
Parse.Cloud.define("averageStars", async (request) => {
    const query = new Parse.Query("Review");
    query.equalTo("movie", request.params.movie);
    const results = await query.find();
    let sum = 0;
    for (let i = 0;i < results.length;++i) {
        sum += results[i].get("stars");
    }
    return sum / results.length;
});
/*

    Function number: 1
    Function name: cloudCodeTest
    Parameters type: -
    Parameters description: no-params
    Description: Function to test if cloud functions are successfullt deployed and running.
    Response type: String
    Response description: "Hello world"

*/

Parse.Cloud.define("cloudCodeTest", function (request) {
    //response.success("Hello World");
    console.log("Hello World");
    console.log("Server IP: " + serverIP);
    console.log("request: " + request);
    const query = new Parse.Query("Review");
    return "Hello World";
});

Parse.Cloud.define("getGroupsCount", async function (request) {
    const query = new Parse.Query("Groups");
    const results = await query.find();
    console.log("Successfully retrieved " + results.length + " groups.");

    return results?.length
});

Parse.Cloud.beforeSave("Groups2", function (request) {
    var myObject = request.object;

    if (myObject.isNew()) {  // only check new records, else its a update
        var query = new Parse.Query("Groups");
        query.equalTo("name", myObject.get("name"));
        query.count({
            success: function (number) { //record found, don't save
                //sResult = number;
                console.log("Number of records: " + number);
                if (number > 0) {
                    console.log("Group name already exists");
                    throw false;
                } else {
                    return "Record does not exist";
                }
            },
            error: function (error) { // no record found -> save
                throw "error";
            }
        })
    } else {
        return "Record is not new";
    }
});
/*

    Function number: 2
    Function name: moviesList
    Parameters type: -
    Parameters description: no-params
    Description: Function returns list of all Movies
    Response type: ArrayList<ParseObject> (Android)
    Response description: Contains list of ParseObject (Android)

*/

Parse.Cloud.define("moviesList", function (request, response) {
    const query = new Parse.Query("Movies");
    query
        .find()
        .then(results => {
            response.success(results);
        })
        .catch(() => {
            response.error("movie lookup failed");
        });
});

/*

    Function number: 3
    Function name: longMoviesList
    Parameters type: -
    Parameters description: no-params
    Description: Function returns list of Long ( duration >= 2 ) Movies
    Response type: Array of Parse Object, ArrayList<ParseObject> (Android)
    Response description: Contains list of all Long ( duration >= 2 ) Movies of type ParseObject (Android)

*/
// Parse.Cloud.define("longMoviesList", function(request, response) {
//     const query = new Parse.Query("Movies");
//     query.find()
//       .then((results) => {
//         for (let i = 0; i < results.length; ++i) {
//             if(results[i].get("duration") < 2){
//                 //To  Remove an array element
//                 results.splice(i, 1);
//             }
//           }
//           response.success(results);
//   })
//     .catch(() =>  {
//         response.error("movie lookup failed");
//       });
// });


//Improved Version
/*

    Function number: 4
    Function name: longMoviesList (Improved Version)
    Parameters type: -
    Parameters description: no-params
    Description: Function returns list of Long ( duration > 2 ) Movies
    Response type: Array of Parse Object, ArrayList<ParseObject> (Android)
    Response description: Contains list of all Long ( duration > 2 ) Movies of type ParseObject (Android)

*/
Parse.Cloud.define("longMoviesList", function (request, response) {
    let query = new Parse.Query("Movies");
    query.greaterThan("duration", 2);
    query
        .find()
        .then(results => {
            response.success(results);
        })
        .catch(() => {
            response.error("movie lookup failed");
        });
});

/*

    Function number: 5
    Function name: avgMovieDuration
    Parameters type: -
    Parameters description: no-params
    Description: Function returns Average Duration of all Movies
    Response type: Double (Android)
    Response description: Average Duration of all movies

*/
Parse.Cloud.define("avgMovieDuration", function (request, response) {
    const query = new Parse.Query("Movies");
    query
        .find()
        .then(results => {
            let sum = 0;
            for (let i = 0;i < results.length;++i) {
                sum += results[i].get("duration");
            }
            response.success(sum / results.length);
        })
        .catch(() => {
            response.error("movie lookup failed");
        });
});


/*

    Function number: 6
    Function name: profilePhotos
    Parameters type: -
    Parameters description: no-params
    Description: Function gives all the profile photos in ProfilePhotos class
    Response type:  Array of Parse Object, ArrayList<ParseObject> (Android)
    Response description: List containing Parse Objects having Profile Photos of ParseFile type with key(profile_photo) .

*/
Parse.Cloud.define("profilePhotos", function (request, response) {
    const query = new Parse.Query("ProfilePhotos");
    query
        .find()
        .then(results => {
            response.success(results);
            // response.success(results.length);
        })
        .catch(() => {
            response.error("movie lookup failed");
        });
});


/*

    Function number: 7
    Function name: cloudCodePutTest
    Parameters type: String
    Parameters description: Any String
    Description: Function gives back the string passed
    Response type:  String
    Response description: String passed in parameter.

*/

Parse.Cloud.define("cloudCodePutTest", function (request, response) {
    response.success(request.params.name);
});


//Put Statements

/*
    Function number: 8
    Function name: addMovie
    Parameters type: Hashmap (non-generic)
    Parameters description: Hashmap  mapping - ( name , movieName ) and ( duration , movieDuration )
    Description: Function adds Movie to Movies Class
    Response type:  String
    Response description: SUCCESSFUL OR NOT.

*/

// Parse.Cloud.define("addMovie", function(request, response) {
//     let name = request.params.name;
//     let duration = request.params.duration;

//     let Movies = Parse.Object.extend("Movies");
//     let movies = new Movies();

//     movies.set("name",request.params.name);
//     movies.set("duration",request.params.duration);

//     movies.save(null, {
//         success: function(gameScore) {
//             response.success("SUCCESSFULLY SAVED");
//         },
//         error: function(gameScore, error) {
//           response.error("Error Saving " + error);
//         }
//     });
// });

//Version 2 using ParseObject
//IMPORTANT: Cannot Pass ParseObject

// Parse.Cloud.define("addMovie", function(request, response) {

//     let Movies = Parse.Object.extend("Movies");
//     let movies = new Movies();

//     let movie = request.params.movie;

//     movies.set("name",movie.getString("name"));
//     movies.set("duration",movie.getNumber("duration"));

//     movies.save(null, {
//         success: function(gameScore) {
//             response.success("SUCCESSFULLY SAVED");
//         },
//         error: function(gameScore, error) {
//           response.error("Error Saving " + error);
//         }
//     });
// });

//Passing ParseFile
/*
    Function number: 9
    Function name: addFile
    Parameters type: Hashmap (non-generic)
    Parameters description: Hashmap  mapping - ( file , file )
    Description: Function adds File to File Class
    Response type:  String
    Response description: SUCCESSFUL OR NOTs.

*/
Parse.Cloud.define("addFile", function (request, response) {
    let Files = Parse.Object.extend("Files");
    let files = new Files();

    let file = request.params.file;

    files.set("files", file);
    // movies.set("duration",request.params.duration);

    files.save(null, {
        success: function (files) {
            response.success("SUCCESSFULLY SAVED");
        },
        error: function (files, error) {
            response.error("Error Saving " + error);
        }
    });
});

//To MySQL Server
// Parse.Cloud.define("getAllTableNames", function(request, response) {
//     Parse.Cloud.httpRequest({
//         url:
//             "http://" +
//             serverIP +
//             "/api/v2/" +
//             serviceName +
//             "/_table?api_key=" +
//             apiKey +
//             "&session_token=" +
//             sessionToken
//     }).then(
//         function(httpResponse) {
//             // success
//             console.log(httpResponse.text);

//             // response.success(httpResponse.text);

//             var res = JSON.parse(httpResponse.text);

//             console.log(res.resource.length);

//             response.success(res.resource.length);
//         },
//         function(httpResponse) {
//             // error
//             console.error(
//                 "Request failed with response code " + httpResponse.status
//             );

//             response.error(
//                 "Request failed with response code " + httpResponse.status
//             );
//         }
//     );
// });

Parse.Cloud.define("getAllTableNames", function (request, response) {
    Parse.Cloud.httpRequest({
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken
    }).then(
        function (httpResponse) {
            // success
            console.log(httpResponse.text);

            // response.success(httpResponse.text);

            var res = JSON.parse(httpResponse.text);

            var resource = res.resource;

            for (let i = 0;i < resource.length;i++) {
                const element = resource[i];
                console.log(element);
            }

            response.success(resource);
        },
        function (httpResponse) {
            // error
            console.error(
                "Request failed with response code " + httpResponse.status
            );

            response.error(
                "Request failed with response code " + httpResponse.status
            );
        }
    );
});

// var tableName = "student";

Parse.Cloud.define("getAllTableRecords", function (request, response) {
    let tableName = request.params.tableName;

    Parse.Cloud.httpRequest({
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken
    }).then(
        function (httpResponse) {
            // success
            console.log(httpResponse.text);

            // response.success(httpResponse.text);

            var res = JSON.parse(httpResponse.text);

            var resource = res.resource;

            response.success(resource);
        },
        function (httpResponse) {
            // error
            console.error(
                "Request failed with response code " + httpResponse.status
            );

            response.error(
                "Request failed with response code " + httpResponse.status
            );
        }
    );
});

Parse.Cloud.define("addStudentRecord", function (request, response) {
    let tableName = "student";
    let name = request.params.name;
    let age = request.params.age;

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: [
                {
                    name: name,
                    age: age
                }
            ]
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

Parse.Cloud.define("addStudentRecords", function (request, response) {
    let tableName = "student";
    let names = request.params.name;
    let ages = request.params.age;

    let length = names.length;

    let students = new Array();

    for (let i = 0;i < length;i++) {
        let map = {};

        map["name"] = names[i];
        map["age"] = ages[i];

        students.push(map);
    }

    // let name = names[0];
    // let age = ages[0];

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: students
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

//Not Working
// Parse.Cloud.define("addStudentRecords", function(request, response) {

//     let tableName = "student";
//     let students = request.params.students;

//     let name = students[0].get("name");
//     let age = students[0].get("name");

//     Parse.Cloud.httpRequest({
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         method: "POST",
//         url: "http://" + serverIP + "/api/v2/" + serviceName + "/_table/" + tableName + "?api_key=" + apiKey + "&session_token=" + sessionToken,
//         body: {
//             resource: [{
//                 name: name,
//                 age: age
//             }]
//         }
//     }).then(function(httpResponse) {

//         response.success("Successful");

//     }, function(httpResponse) {
//         response.error("Error: " + httpResponse.status)
//     });
// });

//UPDATE Based on ID
Parse.Cloud.define("updateStudentRecordById", function (request, response) {
    let tableName = "student";
    let id = request.params.id;
    let name = request.params.name;
    let age = request.params.age;

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "PUT",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: [
                {
                    id: id,
                    name: name,
                    age: age
                }
            ]
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

//Update Based on name
Parse.Cloud.define("updateStudentRecordByName", function (request, response) {
    let tableName = "student";
    let oldName = request.params.oldName;
    let name = request.params.name;
    let age = request.params.age;

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "PUT",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?filter=" +
            encodeURIComponent("name like " + oldName) +
            "&api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: [
                {
                    name: name,
                    age: age
                }
            ]
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

//UPDATE Based on ID
Parse.Cloud.define("updateStudentRecordsById", function (request, response) {
    let tableName = "student";
    let ids = request.params.ids;
    let names = request.params.names;
    let ages = request.params.ages;

    let length = ids.length;

    let students = new Array();

    for (let i = 0;i < length;i++) {
        let map = {};

        map["id"] = ids[i];
        map["name"] = names[i];
        map["age"] = ages[i];

        students.push(map);
    }

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "PUT",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: students
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

//UPDATE Based on Name
Parse.Cloud.define("updateStudentRecordsByName", function (request, response) {
    let tableName = "student";
    let oldNames = request.params.oldNames;
    let names = request.params.names;
    let ages = request.params.ages;

    let length = oldNames.length;

    for (let i = 0;i < length;i++) {
        let students = new Array();

        let map = {};

        let oldName = oldNames[i];

        map["name"] = names[i];
        map["age"] = ages[i];

        students.push(map);

        Parse.Cloud.httpRequest({
            headers: {
                "Content-Type": "application/json"
            },
            method: "PUT",
            url:
                "http://" +
                serverIP +
                "/api/v2/" +
                serviceName +
                "/_table/" +
                tableName +
                "?filter=" +
                encodeURIComponent("name like " + oldName) +
                "&api_key=" +
                apiKey +
                "&session_token=" +
                sessionToken,
            body: {
                resource: students
            }
        }).then(function (httpResponse) {

            // response.success("Successful");

        }, function (httpResponse) {
            response.error("Error: " + httpResponse.status)
        });;
    }
});

//Update Based on name
Parse.Cloud.define("updateStudentRecordByName", function (request, response) {
    let tableName = "student";
    let oldName = request.params.oldName;
    let name = request.params.name;
    let age = request.params.age;

    Parse.Cloud.httpRequest({
        headers: {
            "Content-Type": "application/json"
        },
        method: "PUT",
        url:
            "http://" +
            serverIP +
            "/api/v2/" +
            serviceName +
            "/_table/" +
            tableName +
            "?filter=" +
            encodeURIComponent("name like " + oldName) +
            "&api_key=" +
            apiKey +
            "&session_token=" +
            sessionToken,
        body: {
            resource: [
                {
                    name: name,
                    age: age
                }
            ]
        }
    }).then(
        function (httpResponse) {
            response.success("Successful");
        },
        function (httpResponse) {
            response.error("Error: " + httpResponse.status);
        }
    );
});

// Parse.Cloud.define("updateStudentRecords", function(request, response) {

//     let tableName = "student";
//     let names = request.params.name;
//     let ages = request.params.age;

//     let length = names.length;

//     let students = new Array();

//     for (let i = 0; i < length; i++) {
//         let map = {};

//         map['name'] = names[i];
//         map['age'] = ages[i];

//         students.push(map)
//     }

//     // let name = names[0];
//     // let age = ages[0];

//     Parse.Cloud.httpRequest({
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         method: "POST",
//         url: "http://" + serverIP + "/api/v2/" + serviceName + "/_table/" + tableName + "?api_key=" + apiKey + "&session_token=" + sessionToken,
//         body: {
//             resource: students
//         }
//     }).then(function(httpResponse) {

//         response.success("Successful");

//     }, function(httpResponse) {
//         response.error("Error: " + httpResponse.status)
//     });
// });
