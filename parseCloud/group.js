const utils = require('/parse-server/cloud/utils.js');
const className = "Groups";

function verifySign(params, sig) {
    console.info("===>verifySign===", "Input:", params, "sig:", sig);

    //return sign === sig;
    return true;
}
const getGroupJson = (group) => {
    return {
        id: group?.get("id"),
        name: group?.get("name"),
        urlName: group?.get("urlName"),
        description: group?.get("description"),
        ownerId: group?.get("ownerId"),
        platforms: group?.get("platforms"),
        admins: group?.get("admins"),
        theme: group?.get("theme"),
        poaps: group?.get("poaps"),
        guildPlatforms: group?.get("guildPlatforms"),
        ssoID: group?.get("ssoID"),
        urlName: group?.get("urlName"),
        imageUrl: group?.get("imageUrl"),
        ownerAddr: group?.get("ownerAddr"),
        showMembers: group?.get("showMembers"),
        hideFromExplore: group?.get("hideFromExplore"),
        onboardingComplete: group?.get("onboardingComplete"),
        memberCount: group?.get("memberCount"),
        objectId: group?.get("objectId"),


        isDeleted: group?.get("isDeleted"),
        createdAt: group?.get("createdAt"),
        updatedAt: group?.get("updatedAt"),

        roles: group?.get("roles"),
    };
}
async function query(request) {
    console.info("queryGroup:", request.params);
    let results = [];
    const query = new Parse.Query(className);
    query.equalTo("isDeleted", false);
    if (request?.params?.id) {
        query.equalTo("id", request.params.id);
        results = await query.find({ useMasterKey: true });
    } else if (request?.params?.urlName) {
        query.equalTo("urlName", request.params.urlName);
        results = await query.find({ useMasterKey: true });
    }
    const result = results?.[0];
    const roles = result?.get("roles");
    const roleResults = []
    if (roles?.length > 0) {
        for (let i = 0;i < roles.length;i++) {
            const roleQuery = new Parse.Query("Roles");
            roleQuery.equalTo("objectId", roles[i].id);
            const rs = await roleQuery.find({ useMasterKey: true });
            console.info("queryGroup roleResults:", rs);
            const r = rs?.[0];
            roleResults.push(r);
        }
        console.info("queryGroup roleResults:", roleResults);
    }
    result?.set("roles", roleResults);
    return getGroupJson(result)
}
//queryDetail groupDetail
async function detail(request) {
    console.log("===>group detail:" + JSON.stringify(request.params));

    const group = request.params;
    const groupId = group.id;
    const urlName = group.urlName;

    let groupQuery = new Parse.Query(className);
    if (groupId) groupQuery.equalTo("id", groupId);
    else if (urlName) groupQuery.equalTo("urlName", urlName);

    return groupQuery.first().then(async (group) => {
        if (!group) {
            console.log("group detail error: group not found");
            return null;
        }
        console.log("group detail:" + JSON.stringify(group));
        const roles = group?.get("roles");
        const roleResults = []
        if (roles?.length > 0) {
            for (let i = 0;i < roles.length;i++) {
                const roleQuery = new Parse.Query("Roles");
                roleQuery.equalTo("objectId", roles[i].id);
                const rs = await roleQuery.find({ useMasterKey: true });
                console.info("queryGroup roleResults:", rs);
                const r = rs?.[0];
                roleResults.push(r);
            }
            console.info("queryGroup roleResults:", roleResults);
        }
        group.set("roles", roleResults);
        return getGroupJson(group);
    }).catch((error) => {
        console.log("group detail error:" + error);
        throw error;
    });
    //return result
}
//查询用户名下的所有group
async function queryAll(request) {
    console.info("queryGroups:", request.params);

    let results = [];
    const query = new Parse.Query("Groups");

    if (!!request.params.ownerId) {
        query.equalTo("ownerId", request.params.ownerId);
        query.equalTo("isDeleted", false);
        if (request.params.platformName) {
            query.containedIn("platforms", [request.params.platformName]);
        }
        results = await query.find();
    }

    console.info("queryGroups results.length:" + results?.length);

    return results;
}
// type GroupAdmin = {
//     id?: number,
//     ssoID: string
//     address: string
//     cotaAddress: string
//     isOwner: boolean
// }
//由于admins 更新时只提供了address，所以需要构建为Adminguild后保存
async function updateAdmins(oldAdmins, inAddresses) {
    let results = []
    const oldAddresses = oldAdmins.map(x => x.address)
    console.log("===>group update inAddresses:", inAddresses);
    //找出 oldAdmins,inAddresses 中的address交集
    results = oldAdmins.filter(x => inAddresses.includes(x.address));
    console.log("===>group update Intersection:", results);

    const Difference = inAddresses.filter(x => !oldAddresses.includes(x));
    console.log("===>group update Difference:", Difference);

    for (let i = 0;i < Difference.length;i++) {
        console.info("===>group update inAddresses:2", Difference[i]);
        const diffAddr = Difference[i];

        const query = new Parse.Query("Users");
        query.containedIn("addresses", [diffAddr]);
        const user = await query.first()
        console.info("===>group update user:", user, results);
        if (user?.id) {
            const newAdmin = {
                id: user.get("id"),
                ssoID: user.get("ssoID"),
                address: diffAddr,
                cotaAddress: user.get("cotaAddresses")?.[0],
                isOwner: false
            }
            results.push(newAdmin);
        } else {
            console.info("===>group update user not found:", diffAddr);
            const newAdmin = {
                id: 123,
                ssoID: "",
                address: diffAddr,
                cotaAddress: "",
                isOwner: false
            }
            results.push(newAdmin);
        }
    }
    return results
}
//根据group id 更新group的信息
async function update(request) {
    const params = request.params;
    console.log("===>group update:", JSON.stringify(params));
    console.log("===>group update<===");

    const sigParams = params.params;
    const sig = params.sig;
    if (!verifySign(sigParams, sig)) {
        throw new Error('request signature error');
    }

    const payload = params.payload;
    const groupId = payload.id;
    console.info("===>group update groupId:", groupId);

    let Groups = Parse.Object.extend(className);
    let groupQuery = new Parse.Query(Groups);

    groupQuery.equalTo("id", groupId);
    const group = await groupQuery.first()
        .catch((error) => {
            console.log("group update error:" + error);
            return error;
        });
    if (group) {


        //获取需要添加的admin address（过滤掉已经登记的,string 转换为 GroupAdmin)
        if (payload?.data?.admins?.length > 0) {
            const oldAdmins = await group.get("admins");
            const inAddresses = payload?.data?.admins?.map((x) => x);
            const results = await updateAdmins(oldAdmins, inAddresses)

            group.set(payload?.data);
            group.set("admins", results);

            console.info("===>group update admins:", results);
        } else {
            group.set(payload?.data);
        }
        return group.save();
    } else {
        throw new Error("group update failed");
    }
}
//加个删除标识
function del(request) {
    const params = request.params;
    console.log("===>group del:", JSON.stringify(params));
    console.log("===>group del<===");

    const sigParams = params.params;
    const sig = params.sig;
    if (!verifySign(sigParams, sig)) {
        throw new Error('request signature error');
    }

    const payload = params.payload;
    const groupId = payload.id;
    const groupQuery = new Parse.Query(className);
    groupQuery.equalTo("id", groupId);
    console.log("===>group del groupId:", groupId);
    return groupQuery.first().then((group) => {
        //return group.destroy();
        console.info("===>group info:", group)
        group.set("isDeleted", true);
        return !!group.save()
    }).catch((error) => {
        console.log("group del error:" + error);
        return results.push(error);
    });
}


module.exports = {
    detail,
    query,
    queryAll,
    update,
    del
}

