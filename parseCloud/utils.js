//实现并获取class的自增ID
const getUID = async function (className) {
    const query = new Parse.Query("IncrementIDS");
    query.equalTo("class", className);
    results = await query.find();
    if (results?.length == 0) throw "IncrementIDS not found";
    const id = await results[0].get("uid");
    results[0].increment("uid");
    results[0].save();
    return id;
}

//找出两个数组中相同的元素
const intersection = function (a, b) {
    const s = new Set(b);
    return a.filter(x => s.has(x));
}
//找出数组a中不在数组b中的元素
const difference = function (a, b) {
    const s = new Set(b);
    return a.filter(x => !s.has(x));
}

//找出数组a和数组b中不同的元素
const symmetricDifference = function (a, b) {
    const sA = new Set(a);
    const sB = new Set(b);
    return [...a.filter(x => !sB.has(x)), ...b.filter(x => !sA.has(x))];
}

module.exports = {
    getUID,
    intersection,
    difference,
    symmetricDifference
}