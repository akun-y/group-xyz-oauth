const className = "Users";
//TODO:
function join(request) {
    console.log("join request.params:" + JSON.stringify(request.params));
    let results = { "success": true, "platformResults": [] }


    console.info("user join length:" + results?.length);
    return results;
}
//TODO:
async function statusUpdate(request) {
    console.log("===>statusUpdate request.params:", JSON.stringify(request.params));
    const body = request.params;
    const id = body.id;
    const account = body.account;

    const query = new Parse.Query(className);

    if (id) query.equalTo("id", id);
    else if (account) query.containedIn("addresses", [account]);
    return await query.first().catch((error) => {
        console.log("user statusUpdate error:" + error);
    });
}
async function query(request) {
    console.log("===>user query request.params:", JSON.stringify(request.params));

    let result = {};

    if (!!request.params.ssoID) {
        const query = new Parse.Query("Users");
        query.equalTo("ssoID", request.params.ssoID);
        result = await query.first();
    } else if (!!request.params.address) {
        const query = new Parse.Query("Users");
        query.containedIn('addresses', [request.params.address]);
        result = await query.first();
    }

    if (result) {
        if (!result.get("isSuperAdmin")) result.set("isSuperAdmin", false);
    }
    return {
        ssoID: result?.get("ssoID"),
        id: result?.get("id"),
        objectId: result?.id,
        addresses: result?.get("addresses"),
        cotaAddresses: result?.get("cotaAddresses"),
        isSuperAdmin: result?.get("isSuperAdmin"),
        isDeleted: result?.get("isDeleted"),
        platformUsers: result?.get("platformUsers"),
        publicKey: result?.get("publicKey"),
    };
}


//export
module.exports = {
    query,
    statusUpdate,
    join
}

