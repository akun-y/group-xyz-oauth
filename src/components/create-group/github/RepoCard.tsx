import { HStack, Skeleton, Text, VStack } from "@chakra-ui/react"
import Button from "components/common/Button"
import Card from "components/common/Card"
import Link from "components/common/Link"
import useGroupByPlatformId from "components/guard/setup/hooks/useDiscordGroupByPlatformId"
import NextLink from "next/link"
import getRandomInt from "utils/getRandomInt"
import useCreateGroup from "../hooks/useCreateMyGroup"

const RepoCard = ({
  onSelection,
  platformGroupId,
  repositoryName,
  description,
}: {
  onSelection: (platformGroupId: string) => void
  platformGroupId: string
  repositoryName: string
  description?: string
}) => {
  const {
    onSubmit: onCreateGroup,
    isLoading: isCreationLoading,
    isSigning: isCreationSigning,
    signLoadingText,
  } = useCreateGroup()

  const { id, isLoading, urlName } = useGroupByPlatformId(
    "GITHUB",
    encodeURIComponent(platformGroupId)
  )

  const handleClick = () => {
    onCreateGroup({
      name: repositoryName,
      description,
      imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
      theme: { color: "#4d4d4d" },
      groupPlatforms: [
        {
          platformName: "GITHUB",
          platformGroupId: encodeURIComponent(platformGroupId),
        },
      ],
      roles: [
        {
          name: "Member",
          logic: "AND",
          imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
          requirements: [{ type: "FREE" }],
          rolePlatforms: [
            {
              groupPlatformIndex: 0,
            },
          ],
        },
      ],
    })
  }

  const RepoName = () => (
    <Link href={`https://github.com/${platformGroupId}`} isExternal>
      <Text fontWeight={"bold"}>{platformGroupId}</Text>
    </Link>
  )

  return (
    <Card padding={4}>
      <HStack justifyContent={"space-between"} w="full" h="full">
        {description?.length > 0 ? (
          <VStack spacing={0} alignItems="start">
            <RepoName />

            <Text
              color="gray"
              maxW={"3xs"}
              textOverflow="ellipsis"
              overflow={"hidden"}
              whiteSpace={"nowrap"}
            >
              {description}
            </Text>
          </VStack>
        ) : (
          <RepoName />
        )}

        {isLoading ? (
          <Button isLoading />
        ) : id ? (
          <NextLink href={`/${urlName}`} passHref>
            <Button
              as="a"
              colorScheme="gray"
              data-dd-action-name="Go to group [gh repo setup]"
            >
              Go to group
            </Button>
          </NextLink>
        ) : (
          <Button
            isLoading={isCreationLoading || isCreationSigning}
            loadingText={signLoadingText || "Saving data"}
            colorScheme="GITHUB"
            onClick={onSelection ? () => onSelection(platformGroupId) : handleClick}
          >
            Gate repo
          </Button>
        )}
      </HStack>
    </Card>
  )
}

const RepoSkeletonCard = () => (
  <Card padding={4}>
    <HStack justifyContent={"space-between"} w="full" h="full">
      <Skeleton h={4} w={200} />
      <Skeleton h={10} borderRadius="xl" w={110} opacity={0.4} />
    </HStack>
  </Card>
)

export default RepoCard
export { RepoSkeletonCard }
