import { HStack, Skeleton, Text, VStack } from "@chakra-ui/react"
import Button from "components/common/Button"
import Card from "components/common/Card"
import Link from "components/common/Link"
import useGroupByPlatformId from "components/guard/setup/hooks/useDiscordGroupByPlatformId"
import NextLink from "next/link"
import { GroupBase } from "types"
import getRandomInt from "utils/getRandomInt"
import useCreateGroup from "../hooks/useCreateMyGroup"
import { Tag, TagLabel, TagLeftIcon, Tooltip, Wrap } from "@chakra-ui/react"
import DisplayCard from "components/common/DisplayCard"

import { Users } from "phosphor-react"
import { GroupBase } from "types"
import pluralize from "utils/pluralize"

type Props = {
  group: GroupBase
}
const GroupCard = ({ group }: Props): JSX.Element => {
  //console.info("group------------:", group)
  return(
    <Link
      href={`/${group.urlName}`}
      prefetch={false}
      _hover={{ textDecor: "none" }}
      borderRadius="2xl"
      w="full"
      h="full"
    >
      <DisplayCard image={group.imageUrl} title={group.name}>
        <Wrap zIndex="1">
          <Tag as="li">
            <TagLeftIcon as={Users} />
            <TagLabel>{group.memberCount ?? 0}</TagLabel>
          </Tag>
          <Tooltip label={group.roles?.join(", ")}>
            <Tag as="li">
              <TagLabel>{pluralize(group.roles?.length ?? 0, "role")}</TagLabel>
            </Tag>
          </Tooltip>
        </Wrap>
      </DisplayCard>
    </Link>
  )
}

const GroupSkeletonCard = () => (
  <Card padding={4}>
    <HStack justifyContent={"space-between"} w="full" h="full">
      <Skeleton h={4} w={200} />
      <Skeleton h={10} borderRadius="xl" w={110} opacity={0.4} />
    </HStack>
  </Card>
)

export default GroupCard
export { GroupSkeletonCard }
