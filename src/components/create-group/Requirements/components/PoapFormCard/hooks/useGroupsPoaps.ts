import useSWRImmutable from "swr/immutable"
import { Poap } from "types"
import fetcher from "utils/fetcher"

const fetchData = async (...fancyIds: Array<string>) => {
  const promises = fancyIds.map((fancyId) => fetcher(`/assets/poap/${fancyId}`))
  return Promise.all(promises)
}

const useGroupsPoaps = (
  fancyIds: Array<string>
): { groupsPoaps: Array<Poap>; isGroupsPoapsLoading: boolean } => {
  const { data, isValidating } = useSWRImmutable(
    fancyIds?.length ? fancyIds : null,
    fetchData
  )

  return { groupsPoaps: data, isGroupsPoapsLoading: isValidating }
}

export default useGroupsPoaps
