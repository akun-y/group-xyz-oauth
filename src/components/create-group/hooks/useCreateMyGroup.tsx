import useJsConfetti from "components/create-group/hooks/useJsConfetti"
import processConnectorError from "components/[group]/JoinModal/utils/processConnectorError"
import useDatadog from "components/_app/Datadog/useDatadog"
import useMatchMutate from "hooks/useMatchMutate"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { useRouter } from "next/router"
import { Group, PlatformType, Requirement } from "types"
import fetcher from "utils/fetcher"
import replacer from "utils/groupJsonReplacer"
import preprocessRequirements from "utils/preprocessRequirements"

// TODO: better types
type RoleOrGroup = Group & { requirements?: Array<Requirement> }

const useCreateMyGroup = () => {
  const { addDatadogAction, addDatadogError } = useDatadog()
  const matchMutate = useMatchMutate()

  const toast = useToast()
  const showErrorToast = useShowErrorToast()
  const triggerConfetti = useJsConfetti()
  const router = useRouter()

  const fetchData = async ({
    validation,
    data,
  }: WithValidation<RoleOrGroup>): Promise<RoleOrGroup> =>
    fetcher("/group", {
      validation,
      body: data,
    })

  const useSubmitResponse = useSubmitWithSign<any, RoleOrGroup>(fetchData, {
    onError: (error_) => {
      addDatadogError(`Group creation error`, { error: error_ })

      const processedError = processConnectorError(error_)
      showErrorToast(processedError || error_)
    },
    onSuccess: (response_) => {
      addDatadogAction(`Successful group creation`)
      triggerConfetti()

      toast({
        title: `Group successfully created!`,
        description: "You're being redirected to its page",
        status: "success",
      })
      router.push(`/${response_.urlName}`)

      matchMutate(/^\/group\/address\//)
      matchMutate(/^\/group\?order/)
    },
  })

  return {
    ...useSubmitResponse,
    onSubmit: (data_) => {
      const data = {
        ...data_,
        // prettier-ignore
        ...(data_.groupPlatforms?.[0]?.platformId === PlatformType.TELEGRAM && data_.requirements?.length && {
            requirements: undefined,
            roles: [
              {
                name: "Member",
                imageUrl: data_.imageUrl,
                requirements: preprocessRequirements(data_.requirements),
                rolePlatforms: [
                  {
                    groupPlatformIndex: 0,
                  },
                ],
              },
            ],
          }),
      }

      return useSubmitResponse.onSubmit(JSON.parse(JSON.stringify(data, replacer)))
    },
  }
}

export default useCreateMyGroup
