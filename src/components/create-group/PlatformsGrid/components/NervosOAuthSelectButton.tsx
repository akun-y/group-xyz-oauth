import { ButtonProps, useToast } from "@chakra-ui/react"
import Button from "components/common/Button"
import useGenMPCUser, { useMPCAccessToken } from "components/[group]/hooks/useGenMPCUser"
import useUser, { createUserToParse, useUserParse } from "components/[group]/hooks/useUser"
import useOAuthWithCallback from "components/[group]/JoinModal/hooks/useOAuthWithCallback"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useGateables from "hooks/useGateables"
import useShowErrorToast from "hooks/useShowErrorToast"
import dynamic from "next/dynamic"
import { ArrowSquareIn, CaretRight } from "phosphor-react"
import { useEffect, useMemo, useState } from "react"
import { mutate } from "swr"
import { PlatformName, StorgeItemName } from "types"
import { useFetcherWithSign } from "utils/fetcher"
import isEmpty from "utils/isEmpty"

type Props = {
  onSelection: (platform: PlatformName) => void
  platform: PlatformName
  buttonText: string
  scope?: string
} & ButtonProps

const {SSOID,SSOTOKEN,USERSSO,MPCTOKEN,ACCOUNT,COTAADDRESS,SSOREFRESHTOKEN,SSONAME}=StorgeItemName
/**
 * Started as a general abstraction, but is only used for GitHub so got some GitHub
 * specific stuff in it (scope, readonly). Don't know if we want to generalize it in
 * the future or not so keeping it like this for now
 */
const NervosOAuthSelectButton = ({
  onSelection,
  platform,
  buttonText,
  scope,
  ...buttonProps
}: Props) => {
  ////console.info("onSelection:", onSelection)
  const { ssoID, ssoToken, userSSO } = useWeb25React()
  const { access_token: mpc_access_token } = useMPCAccessToken(ssoToken, userSSO?.name)
  const { data: userMPC, isLoading: isMpcLoading, mutate: mpcMutate,
    //  error
  } = useGenMPCUser(mpc_access_token)
  const [isCreateLoading, setIsCreateUserLoading] = useState(false)

  const { data: userParse, mutate: mpcParse } = useUserParse(ssoID)

   //console.info("mpc_access_token:", userSSO, mpc_access_token)
   //console.info("userMpc:", userMPC, isMpcLoading)
   //console.info("userParse:", userParse,platform)

  const showErrorToast = useShowErrorToast()
  const toast = useToast()
  const user = useUser()
  const isPlatformConnected = user?.platformUsers?.some(
    ({ platformName, platformUserData }) =>
      platformName === platform && !platformUserData?.readonly
  )
  // //console.info("isPlatformConnected:", isPlatformConnected, user)
  const { mutate: mutateGateables } = useGateables(platform)

  const fetcherWithSign = useFetcherWithSign()
  const signLoadingText = false


  useEffect(() => {
    if (isEmpty(userMPC?.name)) {
      mpcMutate()
      mutate("/ckb/gen_address")
    } 
    if (isEmpty(user.platformUsers)) user.mutate()
  }, [])
  useEffect(() => {
    if (mpc_access_token?.length > 0) {
      window.localStorage.setItem(MPCTOKEN, mpc_access_token)
    }
  }, [mpc_access_token])
  //根据sso用户信息判断parse server是否存在该用户，不存在则创建
  useEffect(() => {
    //console.info("NervosOAuthSelectButton:1-", ssoID, userMPC, userSSO)
    ////console.info("NervosOAuthSelectButton:2-", userParse)
    ////console.info("NervosOAuthSelectButton:3-",isEmpty(userParse), isEmpty(userMPC), isEmpty(userSSO))
    if (isEmpty(ssoID) || !isEmpty(userParse) ||
      isCreateLoading || isEmpty(userSSO?.name) || isEmpty(userMPC?.name))
      return

    ////console.info("NervosOAuthSelectButton:3-", userSSO, userMPC)
    setIsCreateUserLoading(true)
    createUserToParse(userSSO, userMPC)
      .then(async (e) => {
        console.log("createUserToParse success!:", e)
        await mutate("/parse/functions/queryGroups")//进入下一个页面前，刷新下group数据
        //onSelection(platform)
      })
      .catch((e) => {
        console.error("createUserToParse fail:", e)
        showErrorToast(e.error)
      })
    mpcParse()


  }, [ssoID, userMPC, userSSO])

  // const getUserInfo = (data) =>
  //   fetch(`http://127.0.0.1:8000/api/get-account?accessToken=${data?.accessToken}`)


  // const { onSubmit, isLoading } = useSubmitWithSign(
  //   getUserInfo,
  //   {
  //     onSuccess: async () => {
  //       await user.mutate()
  //       await mutateGateables()
  //       // onSelection(platform)
  //     },
  //     onError: async () => {
  //       await user.mutate()
  //       await mutateGateables()
  //       toast({
  //         status: "error",
  //         title: `Failed to get account info`,
  //         description: "Please try again",
  //       })
  //       //onSelection(platform)
  //     },
  //   }
  // )

  const { callbackWithOAuth, isAuthenticating, authData } = useOAuthWithCallback(
    platform,
    scope,
    async () => {
      if (!isPlatformConnected) {
        if (!ssoToken) {
          //  onSelection(platform)
        }
      }
    }
  )
  const DynamicCtaIcon = useMemo(
    () => dynamic(async () => (!isPlatformConnected ? ArrowSquareIn : CaretRight)),
    [isPlatformConnected]
  )

  return (
    <Button
      onClick={isPlatformConnected ? () => onSelection(platform) : callbackWithOAuth}
      isLoading={user.isLoading || isAuthenticating}
      loadingText={
        signLoadingText ??
        ((isAuthenticating && "Check the popup window") ||
          (user.isLoading && "Checking account") ||
          "Connecting")
      }
      rightIcon={<DynamicCtaIcon />}
      {...buttonProps}
    >
      {buttonText}
    </Button>
  )
}

export default NervosOAuthSelectButton
