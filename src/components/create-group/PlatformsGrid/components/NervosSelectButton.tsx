import { PlatformName } from "types"
import NervosOAuthSelectButton from "./NervosOAuthSelectButton"

type Props = {
  onSelection: (platform: PlatformName) => void
}

const NervosSelectButton = ({ onSelection }: Props) => (
  <NervosOAuthSelectButton
    scope="repo,read:user"
    buttonText="Create Group"
    colorScheme="GITHUB"
    onSelection={onSelection}
    platform="GATESSO"
  />
)

export default NervosSelectButton
