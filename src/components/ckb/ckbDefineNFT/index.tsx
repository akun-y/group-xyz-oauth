import { Button } from "@chakra-ui/react"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { useEffect, useState } from "react"
import { cotaService } from "utils/ckb"
import fetcher, { fetcherMPC } from "utils/fetcher"
import RegistryCota from "../ckbRegister"


const CkbDefineNFT = () => {
  const { mpcToken } = useWeb25React()
  const [cotaId, setCotaId] = useState<string>("")
  const [nftInfo, setNftInfo] = useState<any>(null)
  const [userInfo, setUserInfo] = useState<any>(null)

  const postDefineNFT = async () => {
    const res = await fetcherMPC("/ckb/test/define_nft", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${mpcToken}`,
      },
      body: JSON.stringify({
        total: 123456,
        name: "test",
        symbol: "test",
        description: "test",
        image: "test",
        attributes: "test",
        metaCharacteristic: "test",
      }),
    })
    const data = await res.json()
    //console.info("postDefineNFT data:", data)
    const json = JSON.parse(data?.data)

    if (data?.statusCode === 0) setCotaId(json?.cotaId)
  }
  const mintNFT = async () => {
    const res = await fetcherMPC("/ckb/test/mint_nft", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${mpcToken}`,
      },
      body: JSON.stringify({
        toaddress: 'ckt1qyqdjgaft4tmh3tm5l7rpjt7qntsxvqp7zvqav5qt5',
        cotaId,
        lockScript: {
          code_hash: "0x00000000000000000000000000000000000000000000000000545950455f4944",
          hash_type: "type",
          args: "0x0000000000000000000000000000000000000000",
        },
        typeScript: {
          code_hash: "0x00000000000000000000000000000000000000000000000000545950455f4944",
          hash_type: "type",
          args: "0x0000000000000000000000000000000000000000",
        },
      }),
    })
    const data = await res.json()
    //console.info("mintNFT data:", data)
  }
  const getDefineInfo = async () => {
    const aggregator = cotaService.aggregator
    const nftInfo = await aggregator.getDefineInfo({
      cotaId: (cotaId?.length > 0) ? cotaId : '0x32fb65b1ee4267ec89e6b3a4da20557e54bf5b1c',
    })
    //setIssued(nftInfo?.issued)
    //console.info("getDefineInfo data:", nftInfo)
    setNftInfo(nftInfo)
    if (Number(nftInfo?.configure) > 0) {
      //console.info("getDefineInfo configure > 0 ,data:", nftInfo)
    }
  }
  const getUserInfo = async () => {
    fetcherMPC("/users/userinfo", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${mpcToken}`,
      },
      body:{}
    }).then(async (res) => {
      //console.info("getUserInfo data:", res)
      setUserInfo(res)
    })
  }

  useEffect(() => {
    getDefineInfo()
    getUserInfo()
  }, [])
  return (<div>
    <h2>CKB Register COTA</h2>
    <RegistryCota user={userInfo} token={mpcToken} />
    <h2>CKB Define NFT</h2>
    <Button marginTop={10} onClick={() => postDefineNFT()}>
      define NFT</Button>{cotaId}
    <br />
    {/* <Button onClick={ckbLogin} marginTop={10}>Login</Button> */}
    {mpcToken}
    <br />
    <Button onClick={() => mintNFT()} marginTop={10}>mint nft</Button>
    <br />
    <Button onClick={() => getDefineInfo()} marginTop={10}>get define info</Button>
    <br />
    cotaId: {cotaId}  <br />
    name:{nftInfo?.name}<br />
    total:{nftInfo?.total}<br />
    issued:{nftInfo?.issued}<br />
    configure:{nftInfo?.configure}<br />
    image:{nftInfo?.image}<br />
    <Button onClick={() => getUserInfo()} marginTop={10}>get user info</Button>
    <br />
    id:{userInfo?.id}<br />
    mainnetAddr:{userInfo?.mainnetAddr}<br />
    testnetAddr:{userInfo?.testnetAddr}<br />
    username:{userInfo?.username}<br />
    email:{userInfo?.email}<br />
    publicKey:{userInfo?.publicKey}<br />
    privateKey:{userInfo?.privateKey}<br />


  </div>)
}
export default CkbDefineNFT
