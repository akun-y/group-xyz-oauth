import { Card, CardBody } from '@chakra-ui/card'
import { Button, Heading, Image, Stack, Text } from "@chakra-ui/react"
import { addressToScript, serializeScript } from "@nervosnetwork/ckb-sdk-utils"

import { useEffect, useState } from "react"
import { cotaService } from "utils/ckb"

const cotaAddress = "ckt1qyqvn2d78jkmwz60vejhk849ge7dc3j0999seu5pgy"
//使用Bar形式显示，视觉效果较好
const CotaNFTBarList = () => {
  const [holdingNFTs, setHoldingNFTs] = useState([])
  const [withdrawableNFTs, setWithdrawableNFTs] = useState([])
  const lockScript = serializeScript(addressToScript(cotaAddress))

  const fetchHoldingNFTs = async () => {
    const holds = await cotaService.aggregator.getHoldCotaNft({
      lockScript,
      page: 0,
      pageSize: 5,
    })
    //console.info("holds:", holds, lockScript)
    if (holds?.nfts) setHoldingNFTs(holds?.nfts as any)
  }
  const fetchWithdrawableNFTs = async () => {
    const withdraws = await cotaService.aggregator.getWithdrawCotaNft({
      lockScript,
      page: 0,
      pageSize: 50,
    })
    //console.info("withdraws:", withdraws)
    if (withdraws?.nfts) setWithdrawableNFTs(withdraws?.nfts as any)
  }

  useEffect(() => {
    fetchHoldingNFTs()
    fetchWithdrawableNFTs()
  }, [])

  //console.info("withdrawableNFTs:", withdrawableNFTs)
  const NFTBar = ({ nft }) => {
    return (
      <Card direction='row' overflow='hidden' variant='outline' margin={2}
        backgroundColor='#222' maxH='200px'>
        <Image
          objectFit='cover'
          maxW='200px'
          src={nft.image}
          alt={nft.cotaId}
        />
        <Stack>
          <CardBody>
            <Heading size='md'>{nft.name}</Heading>
            <Text py='2'
              //border='1px solid blue'
            >
              {nft.description}
              <br />[{nft.cotaId}({nft.tokenIndex})]
              <br />


            </Text>
            <Button variant='solid' colorScheme='blue' marginTop='30px'>2324</Button>
          </CardBody>
          {/* <CardFooter>
            <Button variant='solid' colorScheme='blue'>
              Buy Latte
            </Button>
          </CardFooter> */}
        </Stack>
      </Card>)
  }
  return (
    <div>
      <h2 style={{ color: 'greenyellow' }}>CKB Withdraw NFTs({withdrawableNFTs.length})</h2>
      <div>
        {withdrawableNFTs?.map((nft) => (
          <NFTBar nft={nft} />
        ))}
      </div>
      <div style={{ height: '20px' }} />
      <h2 style={{ color: 'orange' }}>CKB Holding NFTs({holdingNFTs.length})</h2>
      <div>
        {holdingNFTs?.map((nft) => (
          <NFTBar nft={nft} />
        ))}
      </div>
    </div>
  )
}
export default CotaNFTBarList