import { Card, CardBody } from '@chakra-ui/card'
import { Button, Heading, Image, Spinner, Stack, Text } from "@chakra-ui/react"
import { addressToScript, serializeScript } from "@nervosnetwork/ckb-sdk-utils"
import AddCard from 'components/common/AddCard'
import CategorySection from 'components/explorer/CategorySection'
import CotaNFTCard from 'components/explorer/CotaNFTCard'
import ExplorerCardMotionWrapper from 'components/explorer/ExplorerCardMotionWrapper'

import { useEffect, useState } from "react"
import useSWR from 'swr'
import { cotaService } from "utils/ckb"


//const cotaAddress = "ckt1qyqvn2d78jkmwz60vejhk849ge7dc3j0999seu5pgy"
//使用Card形式显示，空间占用上较为紧凑
const CotaNFTCardList = (prop: { cotaAddress: string }) => {
  const {cotaAddress} = prop
  const [holdingNFTs, setHoldingNFTs] = useState([])
  const [withdrawableNFTs, setWithdrawableNFTs] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const fetchHoldingNFTs = async (cotaAddress: string) => {
    const lockScript = serializeScript(addressToScript(cotaAddress))
    const holds = await cotaService.aggregator.getHoldCotaNft({
      lockScript, page: 0, pageSize: 50,
    })
    //console.info("holds:", holds, lockScript)
    return { data: holds?.nfts }
  }
  const fetchWithdrawableNFTs = async (cotaAddress: string) => {
    const lockScript = serializeScript(addressToScript(cotaAddress))
    const withdraws = await cotaService.aggregator.getWithdrawCotaNft({
      lockScript, page: 0, pageSize: 50,
    })
    return { data: withdraws?.nfts }
  }

  useEffect(() => {
    fetchWithdrawableNFTs(cotaAddress).then(
      (res) => res?.data && setWithdrawableNFTs(res.data)
    )
    fetchHoldingNFTs(cotaAddress).then(
      (res) => res?.data && setHoldingNFTs(res.data)
    )

  }, [])


  const nftCount = withdrawableNFTs?.length + holdingNFTs?.length
  return (
    <CategorySection
      title={"Your NFTs(" + nftCount + ")"}
      titleRightElement={
        isLoading && <Spinner size="sm" />
      }
      fallbackText={`No results for ...`}
    >
      {holdingNFTs?.length && (
        holdingNFTs.map((nft, index) => (
          <ExplorerCardMotionWrapper key={"hold" + index}>
            <CotaNFTCard nftData={nft} bgColor='#2f4f4f' />
          </ExplorerCardMotionWrapper>
        ))
      )}
      {withdrawableNFTs?.length && (
        withdrawableNFTs
          .map((nft, index) => (
            <ExplorerCardMotionWrapper key={"withdraw" + index}>
              <CotaNFTCard nftData={nft} />
            </ExplorerCardMotionWrapper>
          ))
      )}
      {nftCount === 0 && (
        <ExplorerCardMotionWrapper key="create-group">
          <AddCard text="Create group" link="/create-group" />
        </ExplorerCardMotionWrapper>
      )}
    </CategorySection>
  )
}
export default CotaNFTCardList