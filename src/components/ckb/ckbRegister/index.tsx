import { Button } from "@chakra-ui/react"
import { Props } from "chakra-react-select"

interface Reginfo extends Props {
  user: any
  token:string
}

const RegistryCota = (props: Reginfo) => {
  //console.info("RegistryCota props:", props)
  const register = async () => {
    fetch("http://localhost:8001/ckb/test/registry_address", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${props.token}`,
      }
    }).then(res => res.json()).then(data => {
      //console.info("register data:", data)
    }).catch(err => {
      console.error("register err:", err)
    })
  }
  return (
    <>
      <p>Register Cota Account</p>
      <p>{props?.user?.testnetAddr}</p>
      <Button margin={10} onClick={register}>Register</Button></>
  )
}
export default RegistryCota