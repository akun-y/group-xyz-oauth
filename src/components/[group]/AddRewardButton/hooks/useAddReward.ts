import useGroup from "components/[group]/hooks/useGroup"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import fetcher from "utils/fetcher"

const useAddReward = (onSuccess?) => {
  const { id, mutateGroup } = useGroup()
  const showErrorToast = useShowErrorToast()
  const toast = useToast()

  const fetchData = async ({ validation, data }: WithValidation<any>) =>
    fetcher(`/group/${id}/platform`, {
      validation,
      body: data,
    })

  const { onSubmit, ...rest } = useSubmitWithSign(fetchData, {
    onError: (err) => showErrorToast(err),
    onSuccess: () => {
      toast({ status: "success", title: "Reward successfully added" })
      mutateGroup()
      onSuccess?.()
    },
  })

  return {
    onSubmit: (data) =>
      onSubmit({
        ...data.rolePlatforms[0].groupPlatform,
        roleIds: data.roleIds.filter((roleId) => !!roleId),
      }),
    ...rest,
  }
}

export default useAddReward
