import useGroup from "components/[group]/hooks/useGroup"
import useSWRImmutable from "swr/immutable"
import { Role } from "types"
import fetcher from "utils/fetcher"

const fetchGroupRoles = (...roleIds: number[]) =>
  Promise.all(roleIds.map((roleId) => fetcher(`/role/${roleId}`)))

const useGroupRoles = (): { groupRoles: Role[]; isGroupRolesLoading: boolean } => {
  const { roles } = useGroup()
  const roleIds = roles.map((role) => role.id)

  const { data: groupRoles, isValidating: isGroupRolesLoading } = useSWRImmutable(
    roleIds?.length ? roleIds : null,
    fetchGroupRoles
  )

  return { groupRoles, isGroupRolesLoading }
}

export default useGroupRoles
