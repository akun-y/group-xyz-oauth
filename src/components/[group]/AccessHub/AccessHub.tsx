import {
  Alert,
  AlertDescription,
  AlertTitle,
  Icon,
  SimpleGrid,
  Stack,
} from "@chakra-ui/react"
import Card from "components/common/Card"
import useMemberships from "components/explorer/hooks/useMemberships"
import { StarHalf } from "phosphor-react"
import platforms from "platforms"
import { PlatformType } from "types"
import useGroup from "../hooks/useGroup"
import useGroupPermission from "../hooks/useGroupPermission"
import PlatformCard from "../RolePlatforms/components/PlatformCard"
import PlatformCardButton from "./components/PlatformCardButton"

// prettier-ignore
const useAccessedGroupPlatforms = () => {
  const { id, groupPlatforms, roles } = useGroup()
  const { isAdmin } = useGroupPermission()
  const memberships = useMemberships()

  if (isAdmin) return groupPlatforms
  
  const accessedRoleIds = memberships?.find((membership) => membership.groupId === id)?.roleIds
  if (!accessedRoleIds) return []

  const accessedRoles = roles.filter(role => accessedRoleIds.includes(role.id))
  const accessedRolePlatforms = accessedRoles.map(role => role.rolePlatforms).flat().filter(rolePlatform => !!rolePlatform)
  const accessedGroupPlatformIds = [...new Set(accessedRolePlatforms.map(rolePlatform => rolePlatform.groupPlatformId))]
  const accessedGroupPlatforms = groupPlatforms.filter(groupPlatform => accessedGroupPlatformIds.includes(groupPlatform.id))

  return accessedGroupPlatforms
}

const AccessHub = (): JSX.Element => {
  const accessedGroupPlatforms = useAccessedGroupPlatforms()
  const { isAdmin } = useGroupPermission()
  //console.info("AccessHub:0", accessedGroupPlatforms,isAdmin)
  return (
    <SimpleGrid
      templateColumns={{
        base: "repeat(auto-fit, minmax(250px, 1fr))",
        md: "repeat(auto-fit, minmax(250px, .5fr))",
      }}
      gap={4}
      mb="10"
    >
      {accessedGroupPlatforms?.length ? (
        accessedGroupPlatforms.map((platform) => {
          //console.info("AccessHub:1", platform)
          const {
            cardPropsHook: useCardProps,
            cardMenuComponent: PlatformCardMenu,
            cardWarningComponent: PlatformCardWarning,
          } = platforms[PlatformType[platform.platformId]]

          return (
            <PlatformCard
              usePlatformProps={useCardProps}
              groupPlatform={platform}
              key={platform.id}
              cornerButton={
                PlatformCardWarning ? (
                  <PlatformCardWarning groupPlatform={platform} />
                ) : (
                  isAdmin &&
                  PlatformCardMenu && (
                    <PlatformCardMenu platformGroupId={platform.platformGroupId} />
                  )
                )
              }
            >
              <PlatformCardButton platform={platform} />
            </PlatformCard>
          )
        })
      ) : (
        <Card>
          <Alert status="info">
            <Icon as={StarHalf} boxSize="5" mr="2" mt="1px" weight="regular" />
            <Stack>
              <AlertTitle>No accessed reward</AlertTitle>
              <AlertDescription>
                You're member of the group, but your roles don't give you any
                auto-managed rewards. The owner might add some in the future or
                reward you another way!
              </AlertDescription>
            </Stack>
          </Alert>
        </Card>
      )}
    </SimpleGrid>
  )
}

export default AccessHub
