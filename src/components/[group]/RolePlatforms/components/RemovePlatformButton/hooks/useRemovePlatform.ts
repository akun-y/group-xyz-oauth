import { GroupPlatform } from 'types';
import useGroup from "components/[group]/hooks/useGroup"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { useFieldArray, useFormContext, useFormState } from "react-hook-form"
import fetcher from "utils/fetcher"
import { useRolePlatform } from "../../RolePlatformProvider"

type Data = {
  removePlatformAccess: boolean
}

const useRemovePlatform = () => {
  const { mutateGroup } = useGroup()
  const toast = useToast()
  const showErrorToast = useShowErrorToast()
  const { index, groupPlatformId,groupPlatform, roleId } = useRolePlatform()
  const { dirtyFields } = useFormState()
  const { reset } = useFormContext()
  const { remove } = useFieldArray({
    name: "rolePlatforms",
  })
  const rewardId = groupPlatform?.id ?? groupPlatformId
  const submit = async ({ validation, data }: WithValidation<Data>) =>
    fetcher(`/role/${roleId}/platform/${rewardId}`, {
      method: "DELETE",
      body: data,
      validation,
    })

  return useSubmitWithSign<Data, any>(submit, {
    onSuccess: () => {
      toast({
        title: `Platform removed!`,
        status: "success",
      })
      remove(index)
      if (!Object.keys(dirtyFields).length) reset(undefined, { keepValues: true })

      mutateGroup()
    },
    onError: (error) => showErrorToast(error),
  })
}

export default useRemovePlatform
