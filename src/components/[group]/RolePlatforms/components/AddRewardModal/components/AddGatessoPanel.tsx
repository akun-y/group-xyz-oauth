import GitHubGroupSetup from "components/common/GitHubGroupSetup"
import { FormProvider, useFieldArray, useForm } from "react-hook-form"

type Props = {
  onSuccess: () => void
}

const defaultValues = {
  platformGroupId: null,
}

const AddGatessoPanel = ({ onSuccess }: Props) => {
  const methods = useForm({ mode: "all", defaultValues })

  const { append } = useFieldArray({
    name: "rolePlatforms",
  })

  return (
    <FormProvider {...methods}>
      <GitHubGroupSetup
        onSelection={(platformGroupId) => {
          append({
            groupPlatform: {
              platformName: "GATESS0",
              platformGroupId: encodeURIComponent(platformGroupId),
            },
            isNew: true,
          })
          onSuccess()
        }}
      />
    </FormProvider>
  )
}

export default AddGatessoPanel
