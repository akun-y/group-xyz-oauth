import GoogleGroupSetup from "components/common/GoogleGroupSetup"
import { FormProvider, useFieldArray, useForm } from "react-hook-form"

type Props = {
  onSuccess: () => void
  skipSettings?: boolean
}

const defaultValues = {
  platformGroupId: null,
}

const AddGooglePanel = ({ onSuccess, skipSettings }: Props): JSX.Element => {
  const methods = useForm({
    mode: "all",
    defaultValues,
  })

  const { append } = useFieldArray({
    name: "rolePlatforms",
  })

  return (
    <FormProvider {...methods}>
      <GoogleGroupSetup
        defaultValues={defaultValues}
        onSelect={(newPlatform) => {
          const { platformRoleData, ...groupPlatformData } = newPlatform
          append({
            groupPlatform: { ...groupPlatformData, platformName: "GOOGLE" },
            platformRoleData,
            isNew: true,
          })
          onSuccess?.()
        }}
        skipSettings={skipSettings}
      />
    </FormProvider>
  )
}

export default AddGooglePanel
