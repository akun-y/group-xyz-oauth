import Button from "components/common/Button"
import TelegramGroup from "components/create-group/TelegramGroup"
import { FormProvider, useFieldArray, useForm, useWatch } from "react-hook-form"

type Props = {
  onSuccess: () => void
}

const AddTelegramPanel = ({ onSuccess }: Props) => {
  const methods = useForm({
    mode: "all",
    defaultValues: {
      platformGroupId: null,
    },
  })

  const platformGroupId = useWatch({
    name: "platformGroupId",
    control: methods.control,
  })

  const { append } = useFieldArray({
    name: "rolePlatforms",
  })

  return (
    <FormProvider {...methods}>
      <TelegramGroup fieldName={`platformGroupId`}>
        <Button
          colorScheme={"green"}
          onClick={() => {
            append({
              groupPlatform: {
                platformName: "TELEGRAM",
                platformGroupId,
              },
              isNew: true,
            })
            onSuccess()
          }}
        >
          Add Telegram
        </Button>
      </TelegramGroup>
    </FormProvider>
  )
}

export default AddTelegramPanel
