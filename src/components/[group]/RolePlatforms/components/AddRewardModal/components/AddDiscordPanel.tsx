import DiscordGroupSetup from "components/common/DiscordGroupSetup"
import DiscordRoleVideo from "components/common/DiscordRoleVideo"
import { FormProvider, useFieldArray, useForm, useWatch } from "react-hook-form"

type Props = {
  onSuccess: () => void
}

const defaultValues = {
  platformGroupId: null,
}

const AddDiscordPanel = ({ onSuccess }: Props) => {
  const methods = useForm({ mode: "all", defaultValues })

  const { append } = useFieldArray({
    name: "rolePlatforms",
  })

  const rolePlatforms = useWatch({ name: "rolePlatforms" })

  const platformGroupId = useWatch({
    control: methods.control,
    name: `platformGroupId`,
  })

  return (
    <FormProvider {...methods}>
      <DiscordGroupSetup
        rolePlatforms={rolePlatforms}
        fieldName={`platformGroupId`}
        selectedServer={platformGroupId}
        defaultValues={defaultValues}
        onSubmit={() => {
          append({
            groupPlatform: { platformName: "DISCORD", platformGroupId },
            isNew: true,
            platformRoleId: null,
          })
          onSuccess()
        }}
      >
        <DiscordRoleVideo />
      </DiscordGroupSetup>
    </FormProvider>
  )
}

export default AddDiscordPanel
