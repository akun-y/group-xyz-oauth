import GitHubGroupSetup from "components/common/GitHubGroupSetup"
import { FormProvider, useFieldArray, useForm } from "react-hook-form"

type Props = {
  onSuccess: () => void
}

const defaultValues = {
  platformGroupId: null,
}

const AddGithubPanel = ({ onSuccess }: Props) => {
  const methods = useForm({ mode: "all", defaultValues })

  const { append } = useFieldArray({
    name: "rolePlatforms",
  })

  return (
    <FormProvider {...methods}>
      <GitHubGroupSetup
        onSelection={(platformGroupId) => {
          append({
            groupPlatform: {
              platformName: "GITHUB",
              platformGroupId: encodeURIComponent(platformGroupId),
            },
            isNew: true,
          })
          onSuccess()
        }}
      />
    </FormProvider>
  )
}

export default AddGithubPanel
