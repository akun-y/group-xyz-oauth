import useIsTGBotIn from "components/create-group/TelegramGroup/hooks/useIsTGBotIn"
import { GroupPlatform, PlatformName } from "types"

const useTelegramCardProps = (groupPlatform: GroupPlatform) => {
  const {
    data: { groupIcon, name },
  } = useIsTGBotIn(groupPlatform.platformGroupId)

  return {
    type: "TELEGRAM" as PlatformName,
    name: (name as string) || "",
    image: (groupIcon as string) || "/default_telegram_icon.png",
  }
}

export default useTelegramCardProps
