import { GroupPlatform, PlatformName } from "types"

const useGithubCardProps = (groupPlatform: GroupPlatform) => ({
  type: "GITHUB" as PlatformName,
  name: decodeURIComponent(groupPlatform.platformGroupId),
  link: `https://github.com/${decodeURIComponent(groupPlatform.platformGroupId)}`,
})

export default useGithubCardProps
