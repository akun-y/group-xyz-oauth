import { GroupPlatform, PlatformName } from "types"

const useNervosCardProps = (groupPlatform: GroupPlatform) => ({
  type: "GATESSO" as PlatformName,
  name: decodeURIComponent(groupPlatform.platformGroupId),
  link: `https://XXXXXXX.com/${decodeURIComponent(groupPlatform.platformGroupId)}`,
})

export default useNervosCardProps
