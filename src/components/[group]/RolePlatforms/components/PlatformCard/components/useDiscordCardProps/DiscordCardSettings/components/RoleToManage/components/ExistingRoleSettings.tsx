import {
  Box,
  FormControl,
  FormErrorMessage,
  FormLabel,
  HStack,
} from "@chakra-ui/react"
import StyledSelect from "components/common/StyledSelect"
import useGroup from "components/[group]/hooks/useGroup"
import { useRolePlatform } from "components/[group]/RolePlatforms/components/RolePlatformProvider"
import useServerData from "hooks/useServerData"
import { useMemo } from "react"
import { useController, useFormContext, useFormState } from "react-hook-form"
import { SelectOption } from "types"
import pluralize from "utils/pluralize"
import useDiscordRoleMemberCounts from "../hooks/useDiscordRoleMemberCount"

const ExistingRoleSettings = () => {
  const { errors, dirtyFields } = useFormState()
  const { setValue } = useFormContext()
  const { roles: groupRoles } = useGroup()
  const { groupPlatform, index } = useRolePlatform()
  const {
    data: { roles: discordRoles },
  } = useServerData(groupPlatform.platformGroupId)

  const { memberCounts } = useDiscordRoleMemberCounts(
    discordRoles?.map((role) => role.id)
  )

  const {
    field: { name, onBlur, onChange, ref, value },
  } = useController({ name: `rolePlatforms.${index}.platformRoleId` })

  const options = useMemo(() => {
    if (!memberCounts || !discordRoles || !groupRoles) return undefined

    const groupifiedRoleIds = groupRoles.map(
      (role) =>
        role.rolePlatforms?.find(
          (platform) => platform.groupPlatformId === groupPlatform.id
        )?.platformRoleId
    )
    const notGroupifiedRoles = discordRoles.filter(
      (discordRole) => !groupifiedRoleIds.includes(discordRole.id)
    )

    return notGroupifiedRoles.map((role) => ({
      label: role.name,
      value: role.id,
      details:
        memberCounts[role.id] === null
          ? "Failed to count members"
          : pluralize(memberCounts[role.id], "member"),
    }))
  }, [discordRoles, memberCounts])

  return (
    <Box px="5" py="4">
      <FormControl isDisabled={!discordRoles?.length}>
        <HStack mb={2} alignItems="center">
          <FormLabel m={0}>Select role</FormLabel>
        </HStack>

        <Box maxW="sm">
          <StyledSelect
            name={name}
            ref={ref}
            options={options}
            value={options?.find((option) => option.value === value)}
            onChange={(selectedOption: SelectOption) => {
              if (!dirtyFields.name) {
                setValue("name", selectedOption?.label, { shouldDirty: false })
              }
              onChange(selectedOption?.value)
            }}
            onBlur={onBlur}
            isLoading={!options}
          />
        </Box>
        <FormErrorMessage>
          {errors.rolePlatforms?.[index]?.platformRoleId?.message}
        </FormErrorMessage>
      </FormControl>
    </Box>
  )
}

export default ExistingRoleSettings
