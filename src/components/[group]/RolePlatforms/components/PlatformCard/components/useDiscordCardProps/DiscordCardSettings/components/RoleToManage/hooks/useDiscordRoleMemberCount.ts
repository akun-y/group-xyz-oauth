import { useRolePlatform } from "components/[group]/RolePlatforms/components/RolePlatformProvider"
import useSWR from "swr"

const useDiscordRoleMemberCounts = (roleIds?: string[]) => {
  const { groupPlatform } = useRolePlatform()

  const shouldFetch =
    groupPlatform.platformGroupId?.length > 0 && Array.isArray(roleIds)

  const { isValidating, data, error } = useSWR(
    shouldFetch
      ? [
          `/discord/memberCount/${groupPlatform.platformGroupId}`,
          { method: "POST", body: { roleIds } },
        ]
      : null
  )

  return {
    memberCounts: data,
    error,
    isLoading: isValidating,
  }
}

export default useDiscordRoleMemberCounts
