import {
  Icon,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/react"
import useGroup from "components/[group]/hooks/useGroup"
import { WarningCircle } from "phosphor-react"
import { memo } from "react"
import { GroupPlatform } from "types"

type Props = {
  groupPlatform: GroupPlatform
  roleMemberCount?: number
  size?: "sm" | "md"
}

const GoogleCardWarning = memo(
  ({ groupPlatform, roleMemberCount, size = "md" }: Props): JSX.Element => {
    const { roles } = useGroup()
    const rolesWithPlatform = roles.filter((role) =>
      role.rolePlatforms?.some(
        (rolePlatform) => rolePlatform.groupPlatformId === groupPlatform?.id
      )
    )
    // const eligibleMembers = useUniqueMembers(rolesWithPlatform)

    // if (eligibleMembers.length < 600) return null
    if (
      roleMemberCount < 0 ||
      !rolesWithPlatform?.some((role) => role.memberCount >= 600)
    )
      return null

    return (
      <Popover trigger="hover" openDelay={0}>
        <PopoverTrigger>
          <Icon
            as={WarningCircle}
            color="orange.300"
            weight="fill"
            // boxSize={size === "sm" ? 5 : 6}
            boxSize={6}
            p={size === "sm" ? "2px" : 0}
            tabIndex={0}
          />
        </PopoverTrigger>
        <PopoverContent>
          <PopoverArrow />
          <PopoverBody>
            {/* {`Google limits documentum sharing to 600 users, and there're already ${eligibleMembers.length}
          eligible members, so you might not get access to this reward.`} */}
            {`Google limits documentum sharing to 600 users, and there're already ${
              roleMemberCount ??
              rolesWithPlatform?.find((role) => role.memberCount >= 600)?.memberCount
            }
          eligible members, so you might not get access to this reward.`}
          </PopoverBody>
        </PopoverContent>
      </Popover>
    )
  }
)

export default GoogleCardWarning
