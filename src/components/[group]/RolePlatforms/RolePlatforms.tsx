import {
  CloseButton,
  SimpleGrid,
  Spacer,
  Text,
  useBreakpointValue,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react"
import AddCard from "components/common/AddCard"
import Button from "components/common/Button"
import Section from "components/common/Section"
import TransitioningPlatformIcons from "components/[group]/RolePlatforms/components/TransitioningPlatformIcons"
import { Plus } from "phosphor-react"
import platforms from "platforms"
import { useFieldArray, useWatch } from "react-hook-form"
import { GroupPlatform, PlatformType } from "types"
import useGroup from "../hooks/useGroup"
import AddRewardModal from "./components/AddRewardModal"
import PlatformCard from "./components/PlatformCard"
import RemovePlatformButton from "./components/RemovePlatformButton"
import { RolePlatformProvider } from "./components/RolePlatformProvider"

type Props = {
  roleId?: number
}

const RolePlatforms = ({ roleId }: Props) => {
  const { groupPlatforms } = useGroup()
  const { remove } = useFieldArray({
    name: "rolePlatforms",
  })

  /**
   * Using fields like this with useWatch because the one from useFieldArray is not
   * reactive to the append triggered in the add platform button
   */
  const fields = useWatch({ name: "rolePlatforms" })
  //console.info("RolePlatforms:0", fields)
  //console.info("RolePlatforms:2", remove)
  //console.info("RolePlatforms:3", groupPlatforms)

  const { isOpen, onOpen, onClose } = useDisclosure()

  const cols = useBreakpointValue({ base: 1, md: 2 })
  const removeButtonColor = useColorModeValue("gray.700", "gray.400")
  const rewardsLabel = useBreakpointValue({
    base: "/ accesses",
    sm: "/ platform accesses",
  })

  return (
    <Section
      title="Rewards"
      spacing="6"
      titleRightElement={
        <>
          <Text as="span" fontSize="sm" colorScheme={"gray"}>
            {rewardsLabel}
          </Text>
          <Spacer />
          <Button
            variant="ghost"
            size="sm"
            leftIcon={<Plus />}
            rightIcon={<TransitioningPlatformIcons boxSize="4" />}
            onClick={onOpen}
          >
            Add reward
          </Button>
        </>
      }
    >
      <SimpleGrid columns={cols} spacing={{ base: 5, md: 6 }}>
        {!fields || fields?.length <= 0 ? (
          <AddCard text={"Add reward"} onClick={onOpen} />
        ) : (
          fields.map((rolePlatform: any, index) => {
            let groupPlatform: GroupPlatform, type
            if (rolePlatform.groupPlatformId) {
              groupPlatform = groupPlatforms.find(
                (platform) => platform.id === rolePlatform.groupPlatformId
              )
              type = PlatformType[groupPlatform?.platformId]
            } else if (rolePlatform.groupPlatform) {
              groupPlatform = rolePlatform.groupPlatform
              type = groupPlatform.platformName
            } else {
              groupPlatform = groupPlatforms.find(
                (platform) => platform.id === rolePlatform.id
              )
              type = PlatformType[groupPlatform?.platformId]

            }
            const { cardPropsHook: useCardProps, cardSettingsComponent } =
              platforms[type]

            let PlatformCardSettings = cardSettingsComponent
            // only show Google access level settings and Discord role settings for new platforms
            if (!rolePlatform.isNew) PlatformCardSettings = null

            return (
              <RolePlatformProvider
                key={rolePlatform.roleId}
                rolePlatform={{
                  ...rolePlatform,
                  roleId,
                  groupPlatform,
                  index,
                }}
              >
                <PlatformCard
                  usePlatformProps={useCardProps}
                  groupPlatform={groupPlatform}
                  cornerButton={
                    !rolePlatform.isNew ? (
                      <RemovePlatformButton removeButtonColor={removeButtonColor} />
                    ) : (
                      <CloseButton
                        size="sm"
                        color={removeButtonColor}
                        rounded="full"
                        aria-label="Remove platform"
                        zIndex="1"
                        onClick={() => remove(index)}
                      />
                    )
                  }
                  actionRow={PlatformCardSettings && <PlatformCardSettings />}
                />
              </RolePlatformProvider>
            )
          })
        )}
      </SimpleGrid>
      <AddRewardModal {...{ isOpen, onClose }} />
    </Section>
  )
}

export default RolePlatforms
