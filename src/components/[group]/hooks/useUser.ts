import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useKeyPair from "hooks/useKeyPair"
import useSWR from "swr"
import useSWRImmutable from "swr/immutable"
import { PlatformAccountDetails, User, UserMPC, UserSSO } from "types"
import { isMainnet } from "utils/ckb"
import { fetcherParse2, useFetcherWithSign } from "utils/fetcher"

const initUser: User = {
    ssoID: "",
    publicKey: "",
    addresses: [],
    cotaAddresses: [],
    platformUsers: [],
    id: 0
}
export const useUserParse = (ssoID?: string): {
    data: User; isLoading: boolean; mutate: any
} => {
    const { isValidating: isLoading, data, mutate } = useSWRImmutable(
        ssoID?.length > 0 ? [`/parse/functions/queryUser`, {
            method: "POST", body: { ssoID: ssoID }, redirect: 'follow'
        }] : null,
        fetcherParse2
    )

    return { data: data ?? initUser, isLoading, mutate }
}
//合并gate及mpc用户信息并在parse server 创建用户，返回创建后用户信息
export const createUserToParse = (gate: UserSSO, mpc: UserMPC) => {
    //console.info("requestCreateUser:", gate, mpc)
    if (!gate?.name || !mpc?.name) {
        console.error("requestCreateUser: gate or mpc user info is null")
        return Promise.reject("requestCreateUser: gate or mpc user info is null")
    }

    const platformUser1: PlatformAccountDetails = {
        platformId: 23,
        platformName: "GATESSO",
        platformUserId: gate.sub,
        username: gate.name,
        avatar: gate.data.avatar,
        platformUserData: { key: "gateUser", value: gate },
    }

    const user: User = {
        ssoID: gate.sub,
        addresses: [mpc.ethAddr],
        publicKey: mpc.publicKey,
        cotaAddresses: [isMainnet ? mpc.mainnetAddr : mpc.testnetAddr],
        platformUsers: [platformUser1],
    }

    return fetcherParse2(`/parse/classes/Users`, { body: user })
}
export const useUser = () => {
    const { account,ssoID } = useWeb25React()
    const { keyPair, ready, isValid } = useKeyPair()
    const fetcherWithSign = useFetcherWithSign()
    const { isValidating, data, mutate } = useSWR<User>(
        account && ready && keyPair && isValid
            ? [`/parse/functions/queryUser`, { method: "POST", body: { address: account } }]
            : null,
        fetcherParse2
        //fetcherWithSign
    )
    ////console.info("useUser:1", account,ssoID)
    ////console.info("useUser:", account, ready, keyPair, isValid, data, isValidating)
    return {
        //id:data?.ssoID,
        isLoading: !data && isValidating,
        ...data,
        mutate,
    }
}

export default useUser
