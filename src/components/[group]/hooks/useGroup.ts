
import useIsSuperAdmin from "hooks/useIsSuperAdmin"
import useKeyPair from "hooks/useKeyPair"
import { useRouter } from "next/router"
import useSWRImmutable from "swr/immutable"
import useSWR from "swr"
import { Group } from "types"
import fetcher, { fetcherParse2, useFetcherWithSign } from "utils/fetcher"
import useUser from "./useUser"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"

const useGroup = (groupId?: number) => {
  const router = useRouter()
  const { account, chainId, provider } = useWeb25React()
  const { addresses } = useUser()
  const isSuperAdmin = useIsSuperAdmin()

  //const id = groupId ?? router.query.group
  const urlName = router.query.group

  const { ready, keyPair } = useKeyPair()
  const fetcherWithSign = useFetcherWithSign()

  const { data, mutate, isValidating } = useSWRImmutable<Group>(
    (groupId || router.query.group) && account ? [`/group/${urlName}`,
    { method: "POST", body: { id: groupId, urlName, ready }, }] : null,
    fetcherWithSign
  )
  const isAdmin = !!data?.admins?.some(
    (admin) => admin.address?.toLowerCase() === addresses?.[0].toLowerCase()
  )

  const { data: dataDetails, mutate: mutateDetails } = useSWRImmutable<Group>(
    urlName && ready && keyPair && (isAdmin || isSuperAdmin)
      ? [`/group/details/${urlName}`,
      {
        method: "POST",
        body: { urlName, id: groupId }
      }]
      : null,
    fetcherWithSign
  )
  return {
    ...(dataDetails ?? data),
    isDetailed: !!dataDetails,
    isLoading: !data && isValidating,
    mutateGroup: data ? mutateDetails : mutate,
  }
}

export default useGroup
