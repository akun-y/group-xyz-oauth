import { useWeb25React } from 'components/_app/Web25ReactProvider/Web25React'
import useGroup from "components/[group]/hooks/useGroup"
import useIsSuperAdmin from "hooks/useIsSuperAdmin"

import useUser from "./useUser"

const useGroupPermission = () => {
    const { ssoID } = useWeb25React()
    const group = useGroup()
    const isSuperAdmin = useIsSuperAdmin()
    
    if (!Array.isArray(group?.admins) || typeof ssoID !== "string")
        return { isAdmin: false, isOwner: false }

    const admin = group.admins.find((a) => a?.ssoID === ssoID)
    //console.info("useGroupPermission:",group.admins, admin,ssoID,group.admins)
    return {
        isAdmin: !!admin || isSuperAdmin,
        isOwner: !!admin?.isOwner,
    }
}

export default useGroupPermission
