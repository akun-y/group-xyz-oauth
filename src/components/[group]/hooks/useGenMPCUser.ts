import useSWRImmutable from "swr/immutable"
import { UserMPC } from "types"
import { fetcherMPC } from "utils/fetcher"

const initUser: UserMPC = {
    _id: "",
    ethAddr: "",
    name: "",
    privateKey: "",
    publicKey: "",
    mainnetAddr: "",
    testnetAddr: "",
    blake160: "",
    email: "",
    id: "",
    lockHash: "",
    username: ""
}
//通过sso access token 获取mpc系统的user access token 即 mpc access token
export const useMPCAccessToken = (sso_access_token: string, username: string) => {
    const { isValidating, data, mutate } = useSWRImmutable(
        sso_access_token && username ? [`/auth/login/oauth`, {
            method: "POST", body: {
                access_token: sso_access_token,
                username: username
            },
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + sso_access_token
            }
        }] : null,
        fetcherMPC
    )
    return {
        isLoading: !data && isValidating,
        ...data,
        mutate,
    }
}
// 获取mpc user 信息，如果没有则创建
export const useGenMPCUser = (mpc_access_token): {
    data: UserMPC; isLoading: boolean; mutate: any
    error: any
} => {
    const { isValidating, data, mutate } = useSWRImmutable(mpc_access_token ?
        [`/ckb/gen_address`, {
            method: "POST", body: {},
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + mpc_access_token
            }
        }] : null, fetcherMPC
    )
    return {
        data: data?.statusCode === 0 ? data.data : initUser,
        isLoading: !data && isValidating,
        mutate,
        error: data?.errorMsg
    }
}

export default useGenMPCUser
