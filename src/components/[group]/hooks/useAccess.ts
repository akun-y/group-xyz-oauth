import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useGroup from "components/[group]/hooks/useGroup"
import useSWR from "swr"
import fetcher, { fetcherParse2 } from "utils/fetcher"

const useAccess = (roleId?: number) => {
    const { account } = useWeb25React()
    const { id } = useGroup()

    const shouldFetch = account && id
//console.info("useAccess:", shouldFetch,id)
    //   请求网址: https://api.group.xyz/v1/group/access/1985/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
    //   请求方法: GET
    //   [{"roleId":1904,"access":true,"requirements":[{"requirementId":5363,"access":true,"amount":2}]},{"roleId":13256,"access":true,"requirements":[{"requirementId":121765,"access":true,"amount":1}]},{"roleId":1899,"access":false,"requirements":[{"requirementId":121851,"access":false,"amount":0}]},{"roleId":1900,"access":false,"requirements":[{"requirementId":121798,"access":false,"amount":0}]},{"roleId":1937,"access":false,"requirements":[{"requirementId":121790,"access":false,"amount":0},{"requirementId":121792,"access":false,"amount":0},{"requirementId":121789,"access":false,"amount":0},{"requirementId":121783,"access":false,"amount":0},{"requirementId":121784,"access":false,"amount":0},{"requirementId":121785,"access":false,"amount":0},{"requirementId":121787,"access":false,"amount":0},{"requirementId":121786,"access":false,"amount":0},{"requirementId":121797,"access":false,"amount":0},{"requirementId":121794,"access":false,"amount":0},{"requirementId":121782,"access":false,"amount":0},{"requirementId":121795,"access":false,"amount":0},{"requirementId":121796,"access":false,"amount":0},{"requirementId":121793,"access":false,"amount":0},{"requirementId":121791,"access":false,"amount":0},{"requirementId":121788,"access":false,"amount":0}]},{"roleId":13257,"access":false,"requirements":[{"requirementId":121737,"access":false,"amount":0},{"requirementId":121739,"access":true,"amount":2},{"requirementId":121738,"access":false,"amount":0}]},{"roleId":22842,"access":false,"requirements":[{"requirementId":121803,"access":false,"amount":0},{"requirementId":121804,"access":false,"amount":0}]},{"roleId":23182,"access":false,"requirements":[{"requirementId":121304,"access":true,"amount":0},{"requirementId":121305,"access":false,"amount":0},{"requirementId":121306,"access":false,"amount":0}]}]
    // const { data, isValidating, error, mutate } = useSWR(
    //     shouldFetch ? [`/parse/functions/groupAccess`, { method: "POST", body: { id, account } }] : null, fetcherParse2,
    //     { shouldRetryOnError: false }
    // )
    const { data, isValidating, error, mutate } = useSWR(
      shouldFetch ? `/group/access/${id}/${account}` : null,
      { shouldRetryOnError: false }
    )

    const hasAccess = roleId
        ? (data ?? error)?.find?.((role) => role.roleId === roleId)?.access
        : (data ?? error)?.some?.(({ access }) => access)

    return {
        data,
        hasAccess,
        error,
        isLoading: isValidating,
        mutate,
    }
}

export default useAccess
