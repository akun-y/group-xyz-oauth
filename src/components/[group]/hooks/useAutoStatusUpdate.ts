import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useMemberships from "components/explorer/hooks/useMemberships"
import useDatadog from "components/_app/Datadog/useDatadog"
import { useEffect } from "react"
import { mutate as swrMutate } from "swr"
import fetcher, { fetcherParse2 } from "utils/fetcher"
import useAccess from "./useAccess"
import useGroup from "./useGroup"

const useAutoStatusUpdate = () => {
    const { addDatadogAction, addDatadogError } = useDatadog()
    const { account } = useWeb25React()
    const { id } = useGroup()

    const { data: roleAccesses, error } = useAccess()
    const memberships = useMemberships()

    const roleMemberships = memberships?.find(
        (membership) => membership.groupId === id
    )?.roleIds

    useEffect(() => {
        try {
            const accesses = roleAccesses ?? error

            if (!account || !Array.isArray(accesses) || !Array.isArray(roleMemberships))
                return

            const roleMembershipsSet = new Set(roleMemberships)

            const accessedRoleIds = accesses
                .filter(({ access }) => !!access)
                .map(({ roleId }) => roleId)

            const unaccessedRoleIdsSet = new Set(
                accesses.filter(({ access }) => access === false).map(({ roleId }) => roleId)
            )

            const shouldSendStatusUpdate =
                !error &&
                (accessedRoleIds.some(
                    (accessedRoleId) => !roleMembershipsSet.has(accessedRoleId)
                ) ||
                    roleMemberships.some((roleId) => unaccessedRoleIdsSet.has(roleId)))

            if (shouldSendStatusUpdate) {
                addDatadogAction("Automatic statusUpdate")
                fetcherParse2(`/parse/functions/userStatusUpdate`, {
                    body: { account },
                    redirect: 'follow'
                }).then(() =>
                    Promise.all([
                        //TODO:
                        swrMutate(`/group/access/${id}/${account}`),
                        swrMutate(`/user/membership/${account}`),
                    ])
                )

                // fetcher(`/user/${account}/statusUpdate/${id}`).then(() =>
                //   Promise.all([
                //     swrMutate(`/group/access/${id}/${account}`),
                //     swrMutate(`/user/membership/${account}`),
                //   ])
                // )
            }
        } catch (err) {
            addDatadogError("Automatic statusUpdate error", { error: err })
        }
    }, [roleAccesses, roleMemberships, account, id, error])
}

export default useAutoStatusUpdate
