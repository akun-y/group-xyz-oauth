import useMemberships from "components/explorer/hooks/useMemberships"
import useGroup from "components/[group]/hooks/useGroup"

const useIsMember = (): boolean => {
    const memberships = useMemberships()
    const { id } = useGroup()
  
    if (id === undefined || memberships === undefined) return undefined

    //return memberships.some((_) =>_.groupId === id && _.roleIds?.length)
    return memberships.some((_) =>_.groupId === id)
}

export default useIsMember
