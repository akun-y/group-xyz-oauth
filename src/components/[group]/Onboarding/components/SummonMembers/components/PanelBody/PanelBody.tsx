import {
  Box,
  Center,
  Grid,
  HStack,
  Text,
  useColorModeValue,
  VStack,
} from "@chakra-ui/react"
import useGroup from "components/[group]/hooks/useGroup"
import Image from "next/image"
import PanelDescription from "./components/PanelDescription"
import PanelTitle from "./components/PanelTitle"

const GROUP_CASTLE_SIZE = "70px"
const GROUP_LOGO_DC_URL =
  "https://cdn.discordapp.com/attachments/950682012866465833/951448318976884826/dc-message.png"

const PanelBody = () => {
  const bg = useColorModeValue("gray.100", "#2F3136")

  const { imageUrl, name } = useGroup()

  const shouldShowGroupImage = imageUrl.includes("http")
  const groupImageDimension = shouldShowGroupImage ? 30 : 15

  return (
    <Box
      bg={bg}
      borderRadius={"4px"}
      p="4"
      borderLeft={"4px solid var(--chakra-colors-DISCORD-500)"}
    >
      <Grid templateColumns={`1fr ${GROUP_CASTLE_SIZE}`} gap={3}>
        <VStack alignItems="left">
          <HStack spacing={2}>
            <Center h="26px" w="26px" borderRadius="full" overflow={"hidden"}>
              <Image
                width={groupImageDimension}
                height={groupImageDimension}
                src={(shouldShowGroupImage && imageUrl) || GROUP_LOGO_DC_URL}
                alt="Group Icon"
              />
            </Center>

            <Text fontSize={"sm"} fontWeight="bold">
              {name}
            </Text>
          </HStack>
          <PanelTitle />
          <PanelDescription />
        </VStack>

        <Box m={1}>
          <Image
            src={GROUP_LOGO_DC_URL}
            alt="Group Logo"
            width={GROUP_CASTLE_SIZE}
            height={GROUP_CASTLE_SIZE}
          />
        </Box>
      </Grid>

      <Text mt={2} fontSize="xs">
        Do not share your private keys. We will never ask for your seed phrase.
      </Text>
    </Box>
  )
}

export default PanelBody
