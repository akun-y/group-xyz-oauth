import { Text, useDisclosure, Wrap } from "@chakra-ui/react"
import Button from "components/common/Button"
import useEditGroup from "components/[group]/EditGroup/hooks/useEditGroup"
import useGroup from "components/[group]/hooks/useGroup"
import useDatadog from "components/_app/Datadog/useDatadog"
import { Check, DiscordLogo, TwitterLogo } from "phosphor-react"
import { PlatformType } from "types"
import PaginationButtons from "../PaginationButtons"
import SendDiscordJoinButtonModal from "./components/SendDiscordJoinButtonModal"

type Props = {
  activeStep: number
  prevStep: () => void
  nextStep: () => void
}

export type SummonMembersForm = {
  channelId: string
  serverId: string
  title: string
  description: string
  button: string
}

const SummonMembers = ({ activeStep, prevStep, nextStep: _ }: Props) => {
  const { addDatadogAction } = useDatadog()

  const { isOpen, onOpen, onClose } = useDisclosure()
  const { groupPlatforms, urlName } = useGroup()

  const discordPlatform = groupPlatforms?.find(
    (p) => p.platformId === PlatformType.DISCORD
  )
  const hasJoinButton = discordPlatform?.platformGroupData?.joinButton !== false

  const { onSubmit, isLoading, response } = useEditGroup()
  //console.info("useEditGroup:1",isLoading, response)
  const handleFinish = () => {
    onSubmit({ onboardingComplete: true })
  }

  return (
    <>
      <Text mb="2">
        If you're satisfied with everything, it's time to invite your community to
        join!
      </Text>
      <Wrap>
        {discordPlatform &&
          (hasJoinButton ? (
            <Button h="10" isDisabled colorScheme="DISCORD" leftIcon={<Check />}>
              Join button sent to Discord
            </Button>
          ) : (
            <Button
              h="10"
              onClick={onOpen}
              colorScheme="DISCORD"
              leftIcon={<DiscordLogo />}
            >
              Send Discord join button
            </Button>
          ))}
        <Button
          as="a"
          h="10"
          href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(
            `Just summoned my group on @groupxyz! Join me on my noble quest: group.xyz/${urlName}`
          )}`}
          target="_blank"
          leftIcon={<TwitterLogo />}
          colorScheme="TWITTER"
          onClick={() => {
            addDatadogAction("click on Share [onboarding]")
          }}
        >
          Share
        </Button>
      </Wrap>
      <PaginationButtons
        activeStep={activeStep}
        prevStep={prevStep}
        nextStep={handleFinish}
        nextLabel="Finish"
        nextLoading={isLoading || response}
      />
      {discordPlatform && (
        <SendDiscordJoinButtonModal
          isOpen={isOpen}
          onClose={onClose}
          serverId={discordPlatform.platformGroupId}
        />
      )}
    </>
  )
}

export default SummonMembers
