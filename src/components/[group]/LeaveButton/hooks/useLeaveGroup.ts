import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useMatchMutate from "hooks/useMatchMutate"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { mutate } from "swr"
import fetcher from "utils/fetcher"

type Data = {
  groupId: number
}
type Response = any

const useLeaveGroup = () => {
  const { account } = useWeb25React()
  const toast = useToast()
  const showErrorToast = useShowErrorToast()
  const matchMutate = useMatchMutate()

  const submit = ({ validation, data }: WithValidation<Data>): Promise<Response> =>
    fetcher(`/user/leaveGroup`, {
      body: data,
      validation,
    })

  return useSubmitWithSign<Data, Response>(submit, {
    onSuccess: () => {
      toast({
        title: "You've successfully left this group",
        status: "success",
      })
      mutate(`/user/membership/${account}`)
      matchMutate(/^\/group\/address\//)
    },
    onError: (error) => showErrorToast(error),
  })
}

export default useLeaveGroup
