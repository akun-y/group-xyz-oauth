import {
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  IconButton,
  Tooltip,
  useDisclosure,
} from "@chakra-ui/react"
import Button from "components/common/Button"
import { Alert } from "components/common/Modal"
import useGroup from "components/[group]/hooks/useGroup"
import { SignOut } from "phosphor-react"
import { useEffect, useRef } from "react"
import useIsMember from "../hooks/useIsMember"
import useLeaveGroup from "./hooks/useLeaveGroup"

const LeaveButton = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = useRef()

  const { id: groupId } = useGroup()
  const isMember = useIsMember()
  const { onSubmit, isLoading, response } = useLeaveGroup()

  useEffect(() => {
    if (response) onClose()
  }, [response])

  if (!isMember) return null

  return (
    <>
      <Tooltip label="Leave group">
        <IconButton
          aria-label="Leave group"
          icon={<SignOut />}
          onClick={onOpen}
          minW={"44px"}
          variant="ghost"
          rounded="full"
        />
      </Tooltip>

      <Alert leastDestructiveRef={cancelRef} onClose={onClose} isOpen={isOpen}>
        <AlertDialogOverlay />

        <AlertDialogContent>
          <AlertDialogHeader>Leave group</AlertDialogHeader>
          <AlertDialogBody>
            Are you sure? You'll be able to join again as long as you satisfy the
            requirements of at least one role in it.
          </AlertDialogBody>
          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Cancel
            </Button>
            <Button
              colorScheme="red"
              ml={3}
              onClick={() => onSubmit({ groupId })}
              isLoading={isLoading}
            >
              Leave group
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </Alert>
    </>
  )
}

export default LeaveButton
