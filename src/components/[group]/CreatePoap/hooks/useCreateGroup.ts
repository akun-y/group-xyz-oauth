import useShowErrorToast from "hooks/useShowErrorToast"
import useSubmit from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { CreateGroupForm, RequestMintLinksForm } from "types"
import { fetcherParse2 } from "utils/fetcher"
import preprocessRequirements from "utils/preprocessRequirements"

const requestCreateGroup = (data: CreateGroupForm) => {
  //console.info("requestCreateGroup:", data)
  
    return fetcherParse2(`/parse/classes/Groups`, { body: data})
}

//fetcher("/api/poap/request-mint-links", { body: data })

const useCreateGroup = (onSuccess) => {
    const toast = useToast()
    const showErrorToast = useShowErrorToast()

    const result = useSubmit<CreateGroupForm, any>(requestCreateGroup, {
        onError: (error) => showErrorToast(error?.error ?? "An error occurred"),
        onSuccess: () => {
            toast({
                title: "Successfuly requested create group!",
                status: "success",
            })
            onSuccess()
        },
    })
    //console.info("useCreateGroup:", result)
    return result
}

export default useCreateGroup
