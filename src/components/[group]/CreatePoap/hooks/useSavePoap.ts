import useJsConfetti from "components/create-group/hooks/useJsConfetti"
import useGroup from "components/[group]/hooks/useGroup"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import fetcher from "utils/fetcher"

type SavePoapType = {
  fancyId: string
  poapId: number
  expiryDate: number
  groupId: number
}

const fetchData = async ({ validation, data }: WithValidation<SavePoapType>) =>
  fetcher("/assets/poap", { validation, body: data })

const useSavePoap = () => {
  const { mutateGroup } = useGroup()
  const toast = useToast()
  const showErrorToast = useShowErrorToast()
  const triggerConfetti = useJsConfetti()

  return useSubmitWithSign<SavePoapType, any>(fetchData, {
    onError: (error) => showErrorToast(error),
    onSuccess: () => {
      // Mutating group data, so the new POAP shows up in the POAPs list
      mutateGroup()
      triggerConfetti()
      toast({
        title: "Successful POAP creation!",
        status: "success",
      })
    },
  })
}

export default useSavePoap
