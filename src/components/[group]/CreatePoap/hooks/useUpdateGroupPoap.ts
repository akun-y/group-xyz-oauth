import useGroup from "components/[group]/hooks/useGroup"
import usePoap from "components/[group]/Requirements/components/PoapRequirementCard/hooks/usePoap"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { GroupPoap } from "types"
import fetcher from "utils/fetcher"
import { useCreatePoapContext } from "../components/CreatePoapContext"
import usePoapEventDetails from "../components/Requirements/components/VoiceParticipation/hooks/usePoapEventDetails"

type UpdatePoapData = { id: number; expiryDate?: number; activate?: boolean }

const updateGroupPoap = async ({
  validation,
  data,
}: WithValidation<UpdatePoapData>) =>
  fetcher(`/assets/poap`, {
    method: "PATCH",
    validation,
    body: data,
  })

const useUpdateGroupPoap = (type: "UPDATE" | "ACTIVATE" = "UPDATE") => {
  const showErrorToast = useShowErrorToast()
  const toast = useToast()

  const { poaps, mutateGroup } = useGroup()
  const { poapData, setPoapData } = useCreatePoapContext()
  const { poap, mutatePoap } = usePoap(poapData?.fancy_id)
  const groupPoap = poaps?.find((p) => p.poapIdentifier === poapData?.id)
  const { mutatePoapEventDetails } = usePoapEventDetails()

  return useSubmitWithSign<UpdatePoapData, GroupPoap>(updateGroupPoap, {
    onError: (error) => showErrorToast(error?.error?.message ?? error?.error),
    onSuccess: async (response) => {
      // Mutating group and POAP data, so the user can see the fresh data in the POAPs list
      const mutatePoapEventDetailsWithResponse = mutatePoapEventDetails({
        ...response,
        contracts: groupPoap?.poapContracts,
      })
      await Promise.all([
        mutateGroup,
        mutatePoap,
        mutatePoapEventDetailsWithResponse,
      ])
      setPoapData(poap)
      toast({
        status: "success",
        title:
          type === "ACTIVATE"
            ? "Successfully activated POAP"
            : "Successfully updated POAP",
      })
    },
  })
}

export default useUpdateGroupPoap
