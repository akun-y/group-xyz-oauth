import { KeyedMutator } from "swr"
import useSWRImmutable from "swr/immutable"

type WithdrawableAmountsResponse = {
  id: number
  chainId: number
  vaultId: number
  tokenSymbol: string
  collected: number
}[]

const useWithdrawableAmounts = (
  groupId: number,
  poapId: number
): {
  withdrawableAmounts: WithdrawableAmountsResponse
  isWithdrawableAmountsLoading: boolean
  mutateWithdrawableAmounts: KeyedMutator<any>
} => {
  const {
    data: withdrawableAmounts,
    isValidating: isWithdrawableAmountsLoading,
    mutate: mutateWithdrawableAmounts,
  } = useSWRImmutable(
    typeof groupId === "number" && typeof poapId === "number"
      ? `/api/poap/get-withdrawable-amount/${groupId}/${poapId}`
      : null
  )

  return {
    withdrawableAmounts,
    isWithdrawableAmountsLoading,
    mutateWithdrawableAmounts,
  }
}

export default useWithdrawableAmounts
