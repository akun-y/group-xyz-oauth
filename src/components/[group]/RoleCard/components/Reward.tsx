import { Circle, HStack, Icon, Img, Text, Tooltip } from "@chakra-ui/react"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import Button from "components/common/Button"
import usePlatformAccessButton from "components/[group]/AccessHub/components/usePlatformAccessButton"
import useAccess from "components/[group]/hooks/useAccess"
import useGroup from "components/[group]/hooks/useGroup"
import useIsMember from "components/[group]/hooks/useIsMember"
import { useOpenJoinModal } from "components/[group]/JoinModal/JoinModalProvider"
import GoogleCardWarning from "components/[group]/RolePlatforms/components/PlatformCard/components/useGoogleCardProps/GoogleCardWarning"
import { ArrowSquareOut, LockSimple } from "phosphor-react"
import { useMemo } from "react"
import { PlatformType, Role, RolePlatform } from "types"
import capitalize from "utils/capitalize"

type Props = {
  role: Role // should change to just roleId when we won't need memberCount anymore
  platform: RolePlatform
}

const getRewardLabel = (platform: RolePlatform) => {
  switch (platform.groupPlatform.platformId) {
    case PlatformType.DISCORD:
      return "Role in: "

    case PlatformType.GOOGLE:
      return `${capitalize(platform.platformRoleData?.role ?? "reader")} access to: `

    default:
      return "Access to: "
  }
}

const Reward = ({ role, platform }: Props) => {
  const isMember = useIsMember()
  const { account } = useWeb25React()
  const openJoinModal = useOpenJoinModal()

  const { hasAccess } = useAccess(role.id)
  const { label, ...accessButtonProps } = usePlatformAccessButton(
    platform.groupPlatform
  )

  const state = useMemo(() => {
    if (isMember && hasAccess)
      return {
        tooltipLabel: label,
        buttonProps: accessButtonProps,
      }
    if (!account || (!isMember && hasAccess))
      return {
        tooltipLabel: (
          <>
            <Icon as={LockSimple} display="inline" mb="-2px" mr="1" />
            Join group to get access
          </>
        ),
        buttonProps: { onClick: openJoinModal },
      }
    return {
      tooltipLabel: "You don't satisfy the requirements to this role",
      buttonProps: { isDisabled: true },
    }
  }, [isMember, hasAccess, account])

  return (
    <HStack pt="3" spacing={0} alignItems={"flex-start"}>
      <Circle size={6} overflow="hidden">
        <Img
          src={`/platforms/${PlatformType[
            platform.groupPlatform?.platformId
          ]?.toLowerCase()}.png`}
          alt={platform.groupPlatform?.platformGroupName}
          boxSize={6}
        />
      </Circle>
      <Text px="2" maxW="calc(100% - var(--chakra-sizes-12))">
        {getRewardLabel(platform)}
        <Tooltip label={state.tooltipLabel} hasArrow>
          <Button
            variant="link"
            rightIcon={<ArrowSquareOut />}
            iconSpacing="1"
            {...state.buttonProps}
            maxW="full"
          >
            {platform.groupPlatform?.platformGroupName ||
              platform.groupPlatform?.platformGroupId}
          </Button>
        </Tooltip>
      </Text>

      {platform.groupPlatform?.platformId === PlatformType.GOOGLE && (
        <GoogleCardWarning
          groupPlatform={platform.groupPlatform}
          roleMemberCount={role.memberCount}
          size="sm"
        />
      )}
    </HStack>
  )
}

const RewardWrapper = ({ role, platform }: Props) => {
  const { groupPlatforms } = useGroup()

  const groupPlatform = groupPlatforms?.find(
    (p) => p.id === platform.groupPlatformId
  )

  if (!groupPlatform) return null

  const platformWithGroupPlatform = { ...platform, groupPlatform }

  return <Reward platform={platformWithGroupPlatform} role={role} />
}

export default RewardWrapper
