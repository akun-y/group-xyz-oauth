import { IconButton, useDisclosure } from "@chakra-ui/react"
import OnboardingMarker from "components/common/OnboardingMarker"
import { GearSix } from "phosphor-react"
import { useOnboardingContext } from "../Onboarding/components/OnboardingProvider"
import EditGroupDrawer from "./EditGroupDrawer"

const EditGroupButton = (): JSX.Element => {
  const {
    isOpen: isEditGroupOpen,
    onOpen: onEditGroupOpen,
    onClose: onEditGroupClose,
  } = useDisclosure()

  const { localStep } = useOnboardingContext()

  return (
    <>
      <OnboardingMarker step={1}>
        <IconButton
          icon={<GearSix />}
          aria-label="Edit group"
          minW={"44px"}
          rounded="full"
          colorScheme="alpha"
          data-dd-action-name={
            localStep === null ? "Edit group" : "Edit group [onboarding]"
          }
          onClick={onEditGroupOpen}
        />
      </OnboardingMarker>

      <EditGroupDrawer
        {...{
          isOpen: isEditGroupOpen,
          onClose: onEditGroupClose,
        }}
      />
    </>
  )
}

export default EditGroupButton
