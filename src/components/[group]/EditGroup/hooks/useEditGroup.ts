import useGroup from "components/[group]/hooks/useGroup"
import useMatchMutate from "hooks/useMatchMutate"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import { useRouter } from "next/router"
import { Group } from "types"
import fetcher, { fetcherParse2 } from "utils/fetcher"
import replacer from "utils/groupJsonReplacer"

type Props = {
  onSuccess?: () => void
  groupId?: number
}

const useEditGroup = ({ onSuccess, groupId }: Props = {}) => {
  const group = useGroup(groupId)

  const matchMutate = useMatchMutate()

  const showErrorToast = useShowErrorToast()
  const router = useRouter()

  const id = groupId ?? group?.id

  const submit = ({ validation, data }: WithValidation<Group>) =>
      fetcher(`/group/${id}`, {
        method: "PATCH",
        validation,
        body: { ...data, id },
      })
    // fetcherParse2(`/parse/functions/updateGroup`, {
    //   validation,
    //   body: { data, id },
    //   redirect: 'follow'
    // })

  const useSubmitResponse = useSubmitWithSign<Group, any>(submit, {
    onSuccess: (newGroup) => {
      if (onSuccess) onSuccess()
      group.mutateGroup()

      matchMutate(/^\/group\/address\//)
      matchMutate(/^\/group\?order/)

      if (newGroup?.urlName && newGroup.urlName !== group?.urlName) {
        router.push(newGroup.urlName)
      }
    },
    onError: (err) => {
      if (typeof err?.error) showErrorToast(err.error)
      else showErrorToast(err)
    }
  })
  //console.info('useEditGroup:', { useSubmitResponse })
  return {
    ...useSubmitResponse,
    onSubmit: (data) =>
      useSubmitResponse.onSubmit(JSON.parse(JSON.stringify(data, replacer))),
  }
}

export default useEditGroup
