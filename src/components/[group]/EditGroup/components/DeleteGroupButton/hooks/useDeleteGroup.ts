import useGroup from "components/[group]/hooks/useGroup"
import useMatchMutate from "hooks/useMatchMutate"
import useShowErrorToast from "hooks/useShowErrorToast"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { useRouter } from "next/router"
import { useFormContext } from "react-hook-form"
import fetcher, { fetcherParse2 } from "utils/fetcher"

type Data = {
  removePlatformAccess?: number
}

const useDeleteGroup = () => {
  const { reset } = useFormContext()
  const matchMutate = useMatchMutate()
  const toast = useToast()
  const showErrorToast = useShowErrorToast()
  const router = useRouter()

  const group = useGroup()

  //${group.id}
  const submit = async ({ validation, data }: WithValidation<Data>) =>
    fetcherParse2(`/parse/functions/deleteGroup`, {
      method: "DELETE",
        body: {
            ...data,
            id:group.id
        },
      validation,
    })

  return useSubmitWithSign<Data, any>(submit, {
    onSuccess: () => {
      toast({
        title: `Group deleted!`,
        description: "You're being redirected to the home page",
        status: "success",
      })

      matchMutate(/^\/group\/address\//)
      matchMutate(/^\/group\?order/)

      reset()

      router.push("/explorer")
    },
    onError: (error) => showErrorToast(error),
    forcePrompt: true,
  })
}

export default useDeleteGroup
