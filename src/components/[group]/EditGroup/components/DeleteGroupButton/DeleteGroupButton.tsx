import {
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  FormLabel,
  useDisclosure,
} from "@chakra-ui/react"
import Button from "components/common/Button"
import { Alert } from "components/common/Modal"
import DeleteButton from "components/[group]/DeleteButton"
import ShouldKeepPlatformAccesses from "components/[group]/ShouldKeepPlatformAccesses"
import { useRef, useState } from "react"
import useDeleteGroup from "./hooks/useDeleteGroup"

const DeleteGroupButton = (): JSX.Element => {
  const [removeAccess, setRemoveAccess] = useState(0)
  const { onSubmit, isLoading, signLoadingText } = useDeleteGroup()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = useRef()

  return (
    <>
      <DeleteButton label="Delete group" onClick={onOpen} />
      <Alert
        leastDestructiveRef={cancelRef}
        {...{ isOpen, onClose }}
        size="xl"
        colorScheme={"dark"}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader>Delete group</AlertDialogHeader>
            <AlertDialogBody>
              <FormLabel mb="3">
                What to do with existing members on the platforms?
              </FormLabel>
              <ShouldKeepPlatformAccesses
                keepAccessDescription="Everything on the platforms will remain as is for existing members, but accesses by this group won’t be managed anymore"
                revokeAccessDescription="Existing members will lose every access granted by this group"
                onChange={(newValue) => setRemoveAccess(+newValue)}
                value={removeAccess}
              />
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                Cancel
              </Button>
              <Button
                colorScheme="red"
                ml={3}
                isLoading={isLoading}
                loadingText={signLoadingText || "Deleting"}
                onClick={() => onSubmit({ removePlatformAccess: removeAccess })}
              >
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </Alert>
    </>
  )
}

export default DeleteGroupButton
