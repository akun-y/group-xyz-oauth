import useOauthPopupWindow from "./useOauthPopupWindow"

const useBBSGateAuth = (
  scope: "repo,read:user" | "repo:invite,read:user" = "repo:invite,read:user"
) =>
  useOauthPopupWindow("http://localhost:8000/login/oauth/authorize", {
    client_id: process.env.NEXT_PUBLIC_BBSGATE_CLIENT_ID,
    scope,
  })

export default useBBSGateAuth
