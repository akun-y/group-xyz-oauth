import { Text, ToastId, useColorModeValue } from "@chakra-ui/react"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import Button from "components/common/Button"
import useGroup from "components/[group]/hooks/useGroup"
import useUser from "components/[group]/hooks/useUser"
import useDatadog from "components/_app/Datadog/useDatadog"
import { manageKeyPairAfterUserMerge } from "hooks/useKeyPair"
import { useSubmitWithSign, WithValidation } from "hooks/useSubmit"
import useToast from "hooks/useToast"
import { useRouter } from "next/router"
import { TwitterLogo } from "phosphor-react"
import { useRef } from "react"
import { mutate } from "swr"
import { PlatformName } from "types"
import fetcher, { fetcherParse2, useFetcherWithSign } from "utils/fetcher"

type PlatformResult = {
  platformId: number
  platformName: PlatformName
} & (
    | { success: true }
    | {
      success: false
      errorMsg: "Unknown Member"
      invite: string
    }
  )

type Response = {
  success: boolean
  platformResults: PlatformResult[]
}

export type JoinData =
  | {
    oauthData: any
  }
  | {
    hash: string
  }

const useJoin = (onSuccess?: () => void) => {
  const { addDatadogAction, addDatadogError } = useDatadog()

  const router = useRouter()
  const { account } = useWeb25React()

  const group = useGroup()
  const user = useUser()
  const fetcherWithSign = useFetcherWithSign()

  const toast = useToast()
  const toastIdRef = useRef<ToastId>()
  const tweetButtonBackground = useColorModeValue("blackAlpha.100", undefined)

  //console.info("useJoin:", { group, user })
  const submit = ({
    data,
    validation,
  }: WithValidation<unknown>): Promise<Response> =>
    //fetcherParse2(`/parse/functions/userJoin`, {
    fetcher(`/user/join`, {
      body: data,
      validation,
    }).then((body) => {
      if (body === "rejected") {
        // eslint-disable-next-line @typescript-eslint/no-throw-literal
        throw "Something went wrong, join request rejected."
      }

      if (typeof body === "string") {
        // eslint-disable-next-line @typescript-eslint/no-throw-literal
        throw body
      }

      return manageKeyPairAfterUserMerge(fetcherWithSign, user, account).then(
        () => body
      )
    })

  const useSubmitResponse = useSubmitWithSign<any, Response>(submit, {
    onSuccess: (response) => {
      // mutate user in case they connected new platforms during the join flow
      user?.mutate?.()

      onSuccess?.()

      if (!response.success) {
        toast({
          status: "error",
          title: "No access",
          description: "Seems like you don't have access to any roles in this group",
        })
        return
      }

      addDatadogAction(`Successfully joined a group`)

      mutate(`/user/membership/${account}`)
      // show user in group's members
      mutate(`/group/${router.query.group}`)

      toastIdRef.current = toast({
        title: `Successfully joined group`,
        duration: 8000,
        description: (
          <>
            <Text>Let others know as well by sharing it on Twitter</Text>
            <Button
              as="a"
              href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(
                `Just joined the ${group.name} group. Continuing my brave quest to explore all corners of web3!
group.xyz/${group.urlName} @groupYunqi`
              )}`}
              target="_blank"
              bg={tweetButtonBackground}
              leftIcon={<TwitterLogo weight="fill" />}
              size="sm"
              onClick={() => toast.close(toastIdRef.current)}
              mt={3}
              mb="1"
              borderRadius="lg"
            >
              Share
            </Button>
          </>
        ),
        status: "success",
      })
    },
    onError: (err) => {
      addDatadogError(`Group join error`, { error: err })
    },
  })

  return {
    ...useSubmitResponse,
    onSubmit: (data) =>
      useSubmitResponse.onSubmit({
        groupId: group?.id,
        platforms: Object.entries(data.platforms)
          .filter(([_, value]) => !!value)
          .map(([key, value]: any) => ({
            name: key,
            ...value,
          })),
      }),
  }
}

export default useJoin
