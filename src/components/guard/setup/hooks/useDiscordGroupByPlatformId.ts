import useSWR from "swr"
import { Group, PlatformName } from "types"

const useGroupByPlatformId = (platform: PlatformName, platformId: string) => {
  const shouldFetch = platformId?.length > 0
  const { data, error, isValidating } = useSWR<Partial<Group>>(
    shouldFetch ? `/group/platform/${platform}/${platformId}` : null,
    { fallbackData: { id: null } }
  )

  return { ...data, isLoading: !data && !error && isValidating }
}

export default useGroupByPlatformId
