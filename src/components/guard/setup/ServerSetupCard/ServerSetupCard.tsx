import { Box, SimpleGrid, Stack } from "@chakra-ui/react"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import Button from "components/common/Button"
import Card from "components/common/Card"
import CardMotionWrapper from "components/common/CardMotionWrapper"
import DiscordRoleVideo from "components/common/DiscordRoleVideo"
import useCreateGroup from "components/create-group/hooks/useCreateMyGroup"
import useSetImageAndNameFromPlatformData from "components/create-group/hooks/useSetImageAndNameFromPlatformData"
import useDatadog from "components/_app/Datadog/useDatadog"
import { Web3Connection } from "components/_app/Web3ConnectionManager"
import { AnimatePresence, motion } from "framer-motion"
import usePinata from "hooks/usePinata"
import useServerData from "hooks/useServerData"
import useSubmitWithUpload from "hooks/useSubmitWithUpload"
import { useRouter } from "next/router"
import { Check } from "phosphor-react"
import { useContext, useEffect, useState } from "react"
import { useFormContext, useWatch } from "react-hook-form"
import { GroupFormType } from "types"
import getRandomInt from "utils/getRandomInt"

const MotionStack = motion(Stack)

const ServerSetupCard = ({ children, onSubmit: onSubmitProp }): JSX.Element => {
  const { addDatadogAction, addDatadogError } = useDatadog()

  const router = useRouter()

  const { account } = useWeb25React()
  const { openWalletSelectorModal } = useContext(Web3Connection)

  const {
    control,
    handleSubmit: formHandleSubmit,
    setValue,
  } = useFormContext<GroupFormType>()

  const selectedServer = useWatch({
    control,
    name: "groupPlatforms.0.platformGroupId",
  })

  const {
    data: { serverIcon, serverName },
  } = useServerData(selectedServer, {
    refreshInterval: 0,
  })

  const [watchedVideo, setWatchedVideo] = useState(
    router.pathname.includes("/create-group")
  )

  const { onSubmit, isLoading, response, isSigning, error, signLoadingText } =
    useCreateGroup()

  useEffect(() => {
    if (error) {
      addDatadogError("Group creation error", { error })
    }
    if (response) {
      addDatadogAction("Successful group creation")
    }
  }, [response, error])

  const { isUploading, onUpload } = usePinata({
    onSuccess: ({ IpfsHash }) => {
      setValue("imageUrl", `${process.env.NEXT_PUBLIC_IPFS_GATEWAY}${IpfsHash}`)
    },
    onError: () => {
      setValue("imageUrl", `/groupLogos/${getRandomInt(286)}.svg`)
    },
  })

  const { handleSubmit, isUploadingShown, uploadLoadingText } = useSubmitWithUpload(
    formHandleSubmit(onSubmit),
    isUploading
  )

  useSetImageAndNameFromPlatformData(serverIcon, serverName, onUpload)

  const loadingText = uploadLoadingText || signLoadingText || "Saving data"

  return (
    <CardMotionWrapper>
      <Card px={{ base: 5, sm: 6 }} py={7}>
        <Stack spacing={8}>
          <AnimatePresence initial={false} exitBeforeEnter>
            <MotionStack
              key={watchedVideo ? "form" : "video"}
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ duration: 0.24 }}
              spacing={8}
            >
              {watchedVideo ? children : <DiscordRoleVideo />}
            </MotionStack>
          </AnimatePresence>

          <SimpleGrid columns={2} gap={4}>
            {watchedVideo ? (
              <>
                <Button
                  colorScheme="gray"
                  disabled={!!account}
                  onClick={openWalletSelectorModal}
                  rightIcon={!!account && <Check />}
                  data-dd-action-name="Connect wallet [dc server setup]"
                >
                  {!account ? "Connect wallet" : "Wallet connected"}
                </Button>

                <Button
                  colorScheme="green"
                  disabled={
                    !account ||
                    response ||
                    isLoading ||
                    isSigning ||
                    isUploadingShown
                  }
                  isLoading={isLoading || isSigning || isUploadingShown}
                  loadingText={loadingText}
                  onClick={handleSubmit}
                  data-dd-action-name="Create group [dc server setup]"
                >
                  Create group
                </Button>
              </>
            ) : (
              <>
                <Box />
                <Button
                  colorScheme="green"
                  onClick={() => {
                    onSubmitProp?.()
                    if (onSubmitProp) return
                    setWatchedVideo(true)
                  }}
                >
                  Got it
                </Button>
              </>
            )}
          </SimpleGrid>
        </Stack>
      </Card>
    </CardMotionWrapper>
  )
}

export default ServerSetupCard
