import { Box, Flex, SimpleGrid, Spinner } from "@chakra-ui/react"
import GroupCard from "components/explorer/GroupCard"
import Link from "next/link"
import { ArrowRight } from "phosphor-react"
import useSWR from "swr"
import LandingButton from "./LandingButton"
import LandingWideSection from "./LandingWideSection"

const ExploreTrendingGroups = (): JSX.Element => {
  const { data: groups, isValidating } = useSWR("/group", {
    refreshInterval: 0,
    revalidateIfStale: false,
    revalidateOnFocus: false,
    revalidateOnReconnect: false,
  })
  const renderedGroups = groups?.slice(0, 12) || []

  return (
    <LandingWideSection
      title="Explore trending Groups"
      position="relative"
      pt="6"
      mb="-8"
    >
      {!groups?.length && isValidating ? (
        <Flex alignItems="center" justifyContent="center">
          <Spinner />
        </Flex>
      ) : (
        <>
          <Box maxH="70vh" overflow="hidden" position="relative">
            <SimpleGrid
              columns={{ base: 1, md: 2, lg: 3 }}
              spacing={{ base: 5, md: 6 }}
            >
              {renderedGroups.map((group) => (
                <GroupCard key={group.urlName} groupData={group} />
              ))}
            </SimpleGrid>

            <Flex
              alignItems="end"
              justifyContent="center"
              position="absolute"
              inset={-1}
              bgGradient="linear-gradient(to top, var(--chakra-colors-gray-800), rgba(39, 39, 42, 0))"
              zIndex="banner"
              pointerEvents="none"
            />
          </Box>

          <Flex alignItems="center" justifyContent="center">
            <Link passHref href="/explorer">
              <LandingButton
                as="a"
                w="max-content"
                position="relative"
                colorScheme="DISCORD"
                mb={8}
                rightIcon={<ArrowRight />}
              >
                See all the groups
              </LandingButton>
            </Link>
          </Flex>
        </>
      )}
    </LandingWideSection>
  )
}

export default ExploreTrendingGroups
