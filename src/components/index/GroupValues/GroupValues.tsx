import { GridItem, SimpleGrid } from "@chakra-ui/react"
import LandingWideSection from "../LandingWideSection"
import ValueCard from "./components/ValueCard"
import ValueText from "./components/ValueText"

const valueCards = [
  {
    link: "https://discord.gg/groupxyz",
    title: "Dedicated support",
    content: (
      <ValueText>
        You can count on us. There are no <br />
        silly questions here. We can help <br />
        you start, secure and scale.
      </ValueText>
    ),
    image: "/landing/ghost.svg",
  },
  {
    link: "https://github.com/agoraxyz/group.xyz",
    title: "Open-source",
    content: (
      <ValueText>
        Our frontend and platform <br />
        connectors are available <br />
        for anyone.
      </ValueText>
    ),
    image: "/landing/fox.svg",
  },
  {
    link: "https://docs.group.xyz/group/group-api-alpha",
    title: "API/SDK",
    content: (
      <ValueText>
        Be creative and build on <br />
        Group's access control <br />
        strategies.
      </ValueText>
    ),
    image: "/landing/group-dude.svg",
  },
  {
    link: "/create-group",
    title: "Accessible",
    content: (
      <ValueText>
        Group is for everyone. It's a no-
        <br />
        code tool with smooth user <br />
        experience.
      </ValueText>
    ),
    image: "/landing/group-guy.svg",
  },
]

const GroupValues = (): JSX.Element => (
  <LandingWideSection title="Group values">
    <SimpleGrid columns={2} gap={{ base: 6, lg: 8 }}>
      {valueCards.map((card) => (
        <GridItem key={card.title} colSpan={{ base: 2, lg: 1 }}>
          <ValueCard
            link={card.link}
            title={card.title}
            content={card.content}
            image={card.image}
          />
        </GridItem>
      ))}
    </SimpleGrid>
  </LandingWideSection>
)

export default GroupValues
