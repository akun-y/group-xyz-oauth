import {
  Alert, AlertDescription, AlertIcon, AlertTitle, Box, FormControl, FormErrorMessage, FormLabel, GridItem, Input, Modal, ModalBody, ModalCloseButton, ModalContent,
  ModalFooter, ModalHeader, ModalOverlay, SimpleGrid, Textarea, useDisclosure, VStack
} from "@chakra-ui/react"
import CardMotionWrapper from "components/common/CardMotionWrapper"
import GroupCard, { GroupSkeletonCard } from "components/create-group/gatesso/GroupCard"

import SearchBar from "components/explorer/SearchBar"
import useCreateGroup from "components/[group]/CreatePoap/hooks/useCreateGroup"
import useUser from "components/[group]/hooks/useUser"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useGateablesWeb2 from "hooks/useGateablesWeb2"
import useToast from "hooks/useToast"
import { Keyboard } from "phosphor-react"
import { useRef, useState } from "react"
import { useForm } from "react-hook-form"
import getRandomInt from "utils/getRandomInt"
import AddCard from "./AddCard"
import Button from "./Button"
import ReconnectAlert from "./ReconnectAlert"
import { PlatformName, PlatformType } from "types"
import useCreateMyGroup from "components/create-group/hooks/useCreateMyGroup"

const GatessoGroupSetup = ({
  onSelection,
}: {
  onSelection?: (platformGroupId: string) => void
  platformGroupId: string
  description?: string
}) => {
  const { ssoID, account, cotaAddr } = useWeb25React()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const toast = useToast()
  const initialRef = useRef(null)
  const finalRef = useRef(null)
  const [search, setSearch] = useState("")
  const user = useUser()

  const { gateables, isLoading, error, mutate } = useGateablesWeb2(
    "GATESSO", ssoID)


  const filteredRepos = gateables?.filter?.((repo) =>
    [repo.objectId, repo.name, repo.description]
      .some((prop) => prop?.toLowerCase()?.includes(search))
  )
  //console.info("filteredRepos:", filteredRepos, gateables)

  if (isLoading) {//骨架图
    return (
      <>
        <Box maxW="lg" mb={8}>
          <SearchBar placeholder="Search group" {...{ search, setSearch }} />
        </Box>

        <SimpleGrid columns={{ base: 1, md: 2 }} spacing={{ base: 4, md: 6 }}>
          {[...Array(9)].map((i) => (
            <GridItem key={i}>
              <GroupSkeletonCard />
            </GridItem>
          ))}
        </SimpleGrid>
      </>
    )
  }

  if (error) {
    return <ReconnectAlert platformName="GATESSO" />
  }

  const defaultValues = {
    name: "",
    ssoID: "",
    urlName: "",
    description: "",
    imageUrl: "https://ipfs.fleek.co/ipfs/bafkreih6axvq3xplvxutsz6fi2npoyluqjayohmklns57a34bfhyehhx7u",
    theme: {
      mode: "DARK" as "DARK",
      color: "#004d4d",
      backgroundImage: "",
      backgroundCss: null
    },
    ownerAddr: account,
    ownerId: ssoID,
    showMembers: true,
    hideFromExplorer: false,
    onboardingComplete: false,
    roles: [
      {
        name: 'Free-1',
        description: 'ewrwer',
        logic: 'AND',
        requirements: [{ type: 'FREE', data: {} }],
        imageUrl: '/groupLogos/112.svg',
        rolePlatforms: [],
        members: [account],
      },
      {
        name: "Member",
        logic: "AND",
        imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
        requirements: [{ type: "FREE" }],
        members: [account],
      },
    ],
    admins: [{
      id: user?.id,
      ssoID,
      address: account?.toLocaleLowerCase(),
      cotaAddress: cotaAddr,
      isOwner: true
    }],
    platforms: ["GATESSO"],
    poaps: [],
    groupPlatforms: [
      {
        platformName: "GATESSO",
        //platformGroupId: encodeURIComponent(platformGroupId),
        platformGroupId: "985740354131218472",
        //"id": 6257,
        platformId: PlatformType.GATESSO,
        platformGroupData: { "invite": "https://discord.gg/theW5CqpMH", "inviteChannel": "1039022942744485888" },
        platformGroupName: "akun's server",
        invite: "https://discord.gg/theW5CqpMH"
      },
    ],
    memberCount: 1,
    isDeleted: false
  }
  const SetupDialog = () => {
    const methods = useForm({
      mode: "all",
      defaultValues
    })
    const {
      onSubmit,
      isLoading,
      isSigning,
      signLoadingText,
    } = useCreateMyGroup()
    // const { onSubmit, isLoading, response } = useCreateGroup(() => {
    //   onClose()
    //   //动态刷新本页面
    //   mutate()
    // })

    const {
      control,
      register,
      setValue,
      formState: { isDirty, errors, touchedFields },
      handleSubmit,
    } = methods

    return (<Modal
      initialFocusRef={initialRef}
      finalFocusRef={finalRef}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create your group</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={6}>
          <FormControl isRequired isInvalid={!!errors?.name}>
            <FormLabel tabIndex={0}>Group Name:</FormLabel>
            <Input ref={initialRef}
              {...register("name", { required: "This field is required." })}
            />
            <FormErrorMessage>{errors?.name?.message}</FormErrorMessage>
          </FormControl>
          <FormControl>
            <FormLabel mt={4}>Description:</FormLabel>
            <Textarea placeholder=''
              {...register("description")}
            />
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme='blue' mr={3}
            onClick={methods.handleSubmit((data) => {
              if (ssoID) {
                //console.info("create group data:", ssoID, data)
                onSubmit(data)
              } else {
                toast({
                  status: "error",
                  title: "No access",
                  description: "Please connect your account first",
                })
              }
            })}
          >
            Save
          </Button>
          <Button onClick={onClose}>Cancel</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>)
  }
  if (gateables?.length > 0) {
    return (
      <>
        <Box maxW="lg" mb={8}>
          <SearchBar placeholder="Search group" {...{ search, setSearch }} />
        </Box><SimpleGrid columns={{ base: 1, md: 2 }} spacing={{ base: 4, md: 6 }}>
          {(filteredRepos ?? gateables)?.map?.((repo) => (
            <CardMotionWrapper key={repo.objectId}>
              <GridItem>
                {/* <GroupCard group={repo} onSelection={onSelection} /> */}
                <GroupCard group={Object.assign(repo, { groupId: repo.objectId })} />
              </GridItem>
            </CardMotionWrapper>
          )).concat(
            <CardMotionWrapper>
              <GridItem>
                <AddCard text="Create group" onClick={() => onOpen()} />
              </GridItem>
            </CardMotionWrapper>
          )}
        </SimpleGrid><SetupDialog />
      </>
    )
  }
  const handleCreateGroup = () => {
    onOpen()
  }

  return (
    <><Alert status="error">
      <AlertIcon />
      <VStack alignItems={"start"}>
        <AlertTitle>No Groups</AlertTitle>
        <AlertDescription>
          It looks like you don't have any group yet. Create one and return
          here to gate access to it!
        </AlertDescription>

        <Button onClick={onOpen} size="sm" rightIcon={<Keyboard />}>
          Create a group
        </Button>
      </VStack>
    </Alert>
      <SetupDialog />
    </>
  )
}

export default GatessoGroupSetup
