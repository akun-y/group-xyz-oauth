import {
  Box, ButtonGroup, Center, Divider,
  HStack, Icon, Img, Text, Tooltip, useDisclosure, VStack
} from "@chakra-ui/react"
import GroupAvatar from "components/common/GroupAvatar"
import { Web25Connection } from "components/_app/Web25ConnectionManager"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { Chains, RPC } from "connectors"
import { LinkBreak, SignIn } from "phosphor-react"
import { useContext, useState } from "react"
import { StorgeItemName } from "types"
import shortenCKB from "utils/shortenCKB"
import AccountButton from "./components/AccountButton"
import AccountModal from "./components/AccountModal"
import NetworkModal from "./components/NetworkModal"
const { SSOID, SSOTOKEN, USERSSO, MPCTOKEN, ACCOUNT, COTAADDRESS, SSOREFRESHTOKEN, SSONAME } = StorgeItemName

const AccountWeb25 = (): JSX.Element => {
  const [value, setValue] = useState("12")
  const { account, chainId, ENSName, ssoID, userParse, cotaAddr, refresh } = useWeb25React()
  const { openWalletSelectorModal, triedEager } = useContext(Web25Connection)
  const {
    isOpen: isAccountModalOpen,
    onOpen: onAccountModalOpen,
    onClose,
  } = useDisclosure()
  const {
    isOpen: isNetworkModalOpen,
    onOpen: onNetworkModalOpen,
    onClose: onNetworkModalClose,
  } = useDisclosure()
  const { addresses } = userParse
  if (!account) {
    return (
      <AccountButton
        style={{ marginRight: "0.5rem" }}
        leftIcon={<SignIn />}
        isLoading={!triedEager}
        onClick={openWalletSelectorModal}
      >
        Login
      </AccountButton>
    )
  }
  const onAccountModalClose = () => {
    onClose()
    
    refresh()
  }
  const linkedAddressesCount = (addresses?.length ?? 1) - 1
  //console.info("AccountWeb25:", ssoID, ENSName, account, linkedAddressesCount)
  return (
    <Box bg="blackAlpha.400" borderRadius={"2xl"}>
      <ButtonGroup isAttached variant="ghost" alignItems="center">
        <AccountButton onClick={onNetworkModalOpen}>
          <Tooltip label={RPC[Chains[chainId]]?.chainName ?? "Unsupported chain"}>
            {RPC[Chains[chainId]]?.iconUrls?.[0] ? (
              <Img src={RPC[Chains[chainId]].iconUrls[0]} boxSize={4} />
            ) : (
              <Center>
                <Icon as={LinkBreak} />
              </Center>
            )}
          </Tooltip>
        </AccountButton>
        <Divider
          orientation="vertical"
          borderColor="whiteAlpha.300"
          /**
           * Space 11 is added to the theme by us and Chakra doesn't recognize it
           * just by "11" for some reason
           */
          h="var(--chakra-space-11)"
        />
        <AccountButton onClick={onAccountModalOpen}>
          <HStack spacing={3}>
            <VStack spacing={0} alignItems="flex-end">
              <Text
                as="span"
                fontSize={linkedAddressesCount ? "sm" : "md"}
                fontWeight={linkedAddressesCount ? "bold" : "semibold"}
              >
                {ENSName || `${shortenCKB(account, 3)}`}
              </Text>
              {linkedAddressesCount && (
                <Text
                  as="span"
                  fontSize="xs"
                  fontWeight="medium"
                  color="whiteAlpha.600"
                >
                  {`+ ${linkedAddressesCount} address${linkedAddressesCount > 1 ? "es" : ""
                    }`}
                </Text>
              )}
            </VStack>
            <GroupAvatar address={account} size={4} />
          </HStack>
        </AccountButton>
      </ButtonGroup>

      <AccountModal isOpen={isAccountModalOpen} onClose={onAccountModalClose} />
      <NetworkModal isOpen={isNetworkModalOpen} onClose={onNetworkModalClose} />
    </Box>
  )
}

export default AccountWeb25
