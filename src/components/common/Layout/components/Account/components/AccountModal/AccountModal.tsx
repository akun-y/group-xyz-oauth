import {
  HStack,
  Icon,
  IconButton,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Tooltip,
  useColorModeValue,
} from "@chakra-ui/react"
import { CoinbaseWallet } from "@web3-react/coinbase-wallet"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { MetaMask } from "@web3-react/metamask"
import { WalletConnect } from "@web3-react/walletconnect"
import CopyableAddress from "components/common/CopyableAddress"
import GroupAvatar from "components/common/GroupAvatar"
import { Modal } from "components/common/Modal"
import useUser from "components/[group]/hooks/useUser"
import { deleteKeyPairFromIdb } from "hooks/useKeyPair"
import { SignOut } from "phosphor-react"
import AccountConnections from "./components/AccountConnections"
import Web25WalletConnect from "components/_app/Web25ReactProvider/Web25Connector"
import { StorgeItemName } from "types"
const {SSOID,SSOTOKEN,USERSSO,MPCTOKEN,ACCOUNT,COTAADDRESS,SSOREFRESHTOKEN,SSONAME}=StorgeItemName

const AccountModal = ({ isOpen, onClose }) => {
  const {ssoID, account, connector,setIsWeb3 } = useWeb25React()
  const { isLoading, platformUsers, addresses } = useUser()
  setIsWeb3(false)
  const modalFooterBg = useColorModeValue("gray.100", "gray.800")

  const connectorName = (c) =>
    c instanceof MetaMask
      ? "MetaMask"
      : c instanceof WalletConnect
        ? "WalletConnect"
        : c instanceof CoinbaseWallet
          ? "Coinbase Wallet"
          : c instanceof Web25WalletConnect
            ? "MPC Wallet"
            : ""

  const handleLogout = () => {
    onClose()
    connector.deactivate()

    const keysToRemove = Object.keys({ ...window.localStorage }).filter((key) =>  /^group_auth_.*$/.test(key)
    )
    
    keysToRemove.forEach((key) => {
      window.localStorage.removeItem(key)
      //console.info("removed key:", key)
    })

    //mpc userid
    deleteKeyPairFromIdb(ssoID).catch(() => { })
    //console.info("deleted keypair from idb",ssoID)
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Account</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack mb={9} direction="row" spacing="4" alignItems="center">
            <GroupAvatar address={account} />
            <CopyableAddress address={account} decimals={5} fontSize="2xl" />
          </Stack>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="space-between"
            mb="-1"
          >
            <Text colorScheme="gray" fontSize="sm" fontWeight="medium">
              {`Connected with ${connectorName(connector)}`}
            </Text>
            <HStack>
              <Tooltip label="Disconnect">
                <IconButton
                  size="sm"
                  variant="outline"
                  onClick={handleLogout}
                  icon={<Icon as={SignOut} p="1px" />}
                  aria-label="Disconnect"
                />
              </Tooltip>
            </HStack>
          </Stack>
        </ModalBody>
        {(isLoading || platformUsers || addresses) && (
          <ModalFooter bg={modalFooterBg} flexDir="column" pt="10">
            <AccountConnections />
          </ModalFooter>
        )}
      </ModalContent>
    </Modal>
  )
}

export default AccountModal
