
import { useWeb3React } from "@web3-react/core"
import { useUserParse } from "components/[group]/hooks/useUser"
import { createContext, PropsWithChildren, useContext, useEffect, useState } from "react"
import { StorgeItemName, User, UserSSO, Web25Session } from "types"
import Web25WalletConnect from "./Web25Connector"
import { createStore, del, get, set } from "idb-keyval"
import useGenMPCUser from "components/[group]/hooks/useGenMPCUser"
import { cotaService, isMainnet } from "utils/ckb"
import isEmpty from "utils/isEmpty"
const initSession = {
  ssoID: "",
  ssoToken: "",
  ssoRefreshToken: "",
  mpcToken: "",
  ENSName: "",
  account: "",
  cotaAddr: "",
}
const { SSOID, SSOTOKEN, USERSSO, MPCTOKEN, ACCOUNT, COTAADDRESS, SSONAME } = StorgeItemName
const getStore = () => createStore("web25", "session")

const getSessionFromIdb = (userId: string) => get<Web25Session>(userId, getStore())
const deleteSessionFromIdb = (userId: string) => del(userId, getStore())
const setSessionToIdb = (userId: string, keys: Web25Session) => set(userId, keys, getStore())


const Web25React = createContext({
  account: "",
  cotaAddr: "",
  connector: null,
  chainId: 1,
  isActive: false,
  usersChainId: null,
  ENSName: "",
  provider: null,
  setIsWeb3: null,

  ssoID: "",
  ssoName: "",
  ssoToken: "",

  mpcToken: "",
  userSSO: null,
  userParse: null,
  mutate: null,
  id: null,
  refresh: null,
})
const useWeb25React = () => useContext(Web25React)
function send(method: string, params: Array<any>): Promise<any> {
  //console.info("send:", method, params)
  return null
}

const provider = {
  getSigner: (address: string) => {
    //console.info("Web25React - getSigner:", address)
    return {
      signMessage: (message: string) => {
        //console.info("Web25React - signMessage:", message)
        return "0x55686a3701ef70e52f7c1eb533369df8328864123107a54dd597b104e33ef2bb66a4d47651a30906056fd921ad7b19f241420d0dc199321d316633967774f4b01c"
      }
    }
  },
  lookupAddress: (address: string) => {
    //console.info("Web25React - lookupAddress:", address)
    return "aabd"
  },
  send
}


type Props = {
  isWeb25?: boolean
  children: React.ReactNode
}
const Web25ReactProvider = ({ children }: PropsWithChildren<Props>): JSX.Element => {
  const { connector: web3connector, account: web3account, isActive: web3isActive, chainId: web3chainId } = useWeb3React()

  const [account, setAccount] = useState<string>("")
  const [cotaAddr, setCotaAddr] = useState<string>("")

  const [userSSO, setUserSSO] = useState<UserSSO>(null)
  const [ssoID, setSsoID] = useState<string>("")
  const [ssoName, setSsoName] = useState<string>("")
  const [ssoToken, setSsoToken] = useState<string>("")

  const [isWeb3, setIsWeb3] = useState(false)
  const [chainId, setChainId] = useState(1)
  const [isActive, setIsActive] = useState(false)
  const [connector, setConnector] = useState(null)
  const [mpcToken, setMpcToken] = useState("")

  const usersChainId = chainId
  const { data: userParse, isLoading, mutate } = useUserParse(ssoID)
  const { data: userMPC } = useGenMPCUser(mpcToken)
  const refresh = () => {
    //console.info("refresh", ssoID, window.localStorage.getItem(SSOID))
    setSsoID(window.localStorage.getItem(SSOID))
    setSsoName(window.localStorage.getItem(SSONAME))
    setSsoToken(window.localStorage.getItem(SSOTOKEN))
    setMpcToken(window.localStorage.getItem(MPCTOKEN))
    setUserSSO(JSON.parse(window.localStorage.getItem(USERSSO)))

    setAccount(window.localStorage.getItem(ACCOUNT))
    setCotaAddr(window.localStorage.getItem(COTAADDRESS))

    if (isWeb3) {
      setChainId(web3chainId)
      setIsActive(web3isActive)
      setAccount(web3account)
      setConnector(web3connector)
    } else {
      setChainId(1)
      setConnector(new Web25WalletConnect())
      if (userMPC._id) {
        !account && userMPC.ethAddr && setAccount(userMPC.ethAddr)
        setCotaAddr(isMainnet ? userMPC.mainnetAddr : userMPC.testnetAddr)
      }
      if (userParse?.addresses?.length > 0) {
        !account && userParse.addresses[0] && setAccount(userParse.addresses[0])
        setIsActive(true)
      }
    }

    if (isEmpty(ssoID)) {
      setAccount("")
      setCotaAddr("")
    }

  }
  useEffect(refresh, [userParse, isWeb3, userMPC, ssoID])
  //console.info("Web25React:", { ssoID, account, cotaAddr, connector, chainId, isActive, })
  return (
    <Web25React.Provider
      value={{
        account,
        cotaAddr,
        ENSName: userParse?.ssoID?.length > 0 ?
          userParse?.platformUsers?.[0]?.username :
          userSSO?.name,

        connector,
        chainId,
        isActive,
        usersChainId,

        provider,
        setIsWeb3,
        mpcToken,
        userSSO,
        ssoID,
        ssoName,
        ssoToken,
        userParse,
        mutate,
        id: userParse?.id,
        refresh
      }}
    >
      {children}
    </Web25React.Provider>
  )
}

export { Web25React, Web25ReactProvider, useWeb25React }
