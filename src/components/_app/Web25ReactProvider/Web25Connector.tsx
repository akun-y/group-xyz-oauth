
class Web25WalletConnect  {
  constructor() {
  }
  activate(): Promise<boolean> {//执行登录操作
    console.log('activate')
    return new Promise(function (resolve, reject) {
      const id = window?.localStorage.getItem('gateId')
      if (id?.length > 0) {
        resolve(true)
      } else {
        reject(false)
      }
    })
  }

  deactivate() {//执行退出操作
    console.log('Web25WalletConnect deactivate', window)
    //window?.localStorage.removeItem('gateId')
    //window?.localStorage.removeItem('userSSO')
  }
}

export default Web25WalletConnect
