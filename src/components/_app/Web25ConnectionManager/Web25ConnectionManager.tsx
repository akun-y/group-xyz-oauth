import { useDisclosure } from "@chakra-ui/react"
import { CoinbaseWallet } from "@web3-react/coinbase-wallet"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { MetaMask } from "@web3-react/metamask"
import { WalletConnect } from "@web3-react/walletconnect"
import NetworkModal from "components/common/Layout/components/Account/components/NetworkModal/NetworkModal"
import { useUserParse } from "components/[group]/hooks/useUser"
import { useRouter } from "next/router"
import { createContext, PropsWithChildren, useEffect, useState } from "react"
import { StorgeItemName, User, UserSSO } from "types"
import useDatadog from "../Datadog/useDatadog"
import SSOLoginModal from "./components/WalletSelectorModal"
import useEagerConnect from "./hooks/useEagerConnect"

const Web25Connection = createContext<{
  isWalletSelectorModalOpen: boolean
  openWalletSelectorModal: () => void
  closeWalletSelectorModal: () => void
  triedEager: boolean
  isNetworkModalOpen: boolean
  openNetworkModal: () => void
  closeNetworkModal: () => void
  userParse: User
}>({
  isWalletSelectorModalOpen: false,
  openWalletSelectorModal: () => { },
  closeWalletSelectorModal: () => { },
  triedEager: false,
  isNetworkModalOpen: false,
  openNetworkModal: () => { },
  closeNetworkModal: () => { },
  userParse: null
})

const Web25ConnectionManager = ({
  children,
}: PropsWithChildren<any>): JSX.Element => {
  const { addDatadogAction } = useDatadog()

  const { connector, isActive, account } = useWeb25React()
  const [userSSO, setUserSSO] = useState<UserSSO>(null)
  const { data: userParse } = useUserParse(userSSO?.sub)

  const {
    isOpen: isWalletSelectorModalOpen,
    onOpen: openWalletSelectorModal,
    onClose: closeWalletSelectorModal,
  } = useDisclosure()
  const {
    isOpen: isNetworkModalOpen,
    onOpen: openNetworkModal,
    onClose: closeNetworkModal,
  } = useDisclosure()
  const router = useRouter()

  ////console.info("Web25ConnectionManager:", { userSSO })
  // try to eagerly connect to an injected provider, if it exists and has granted access already
  const triedEager = useEagerConnect()

  useEffect(() => {
    const json = window.localStorage.getItem(StorgeItemName.USERSSO)
    if (json) {
      const userSSO = JSON.parse(json) as UserSSO
      setUserSSO(userSSO)
    }
  }, [])
  useEffect(() => {
    if (triedEager && !isActive && router.query.redirectUrl) {
      //console.info("Web3ConnectionManager-openWalletSelectorModal",
        //triedEager, isActive, router.query.redirectUrl)
      openWalletSelectorModal()
    }
  }, [triedEager, isActive, router.query])

  useEffect(() => {
    if (!isActive || !triedEager) return
    addDatadogAction("Successfully connected wallet", {
      userAddress: account?.toLowerCase(),
    })
  }, [isActive, triedEager])

  // Sending actions to datadog
  useEffect(() => {
    if (!connector) return
    if (connector instanceof MetaMask) {
      addDatadogAction(`Successfully connected wallet [Metamask]`)
    }
    if (connector instanceof WalletConnect)
      addDatadogAction(`Successfully connected wallet [WalletConnect]`)
    if (connector instanceof CoinbaseWallet)
      addDatadogAction(`Successfully connected wallet [CoinbaseWallet]`)
  }, [connector])

  return (
    <Web25Connection.Provider
      value={{
        isWalletSelectorModalOpen,
        openWalletSelectorModal,
        closeWalletSelectorModal,
        triedEager,
        isNetworkModalOpen,
        openNetworkModal,
        closeNetworkModal,
        userParse
      }}
    >
      {children}
      <SSOLoginModal
        {...{
          isModalOpen: isWalletSelectorModalOpen,
          openModal: openWalletSelectorModal,
          closeModal: closeWalletSelectorModal,
        }}
      />
      <NetworkModal
        {...{
          isOpen: isNetworkModalOpen,
          onClose: closeNetworkModal,
        }}
      />
    </Web25Connection.Provider>
  )
}
export { Web25Connection, Web25ConnectionManager }
