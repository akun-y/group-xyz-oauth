import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { connectors } from "connectors"
import { useEffect, useState } from "react"

const useEagerConnect = (): boolean => {
  const { isActive } = useWeb25React()

  const [tried, setTried] = useState(false)
  const [[metaMask]] = connectors

  useEffect(() => {
    metaMask
      .connectEagerly()
      .catch(() => setTried(true))
      .finally(() => setTried(true))
  }, [metaMask])

  // if the connection worked, wait until we get confirmation of that to flip the flag
  useEffect(() => {
    if (!tried && isActive) {
      setTried(true)
    }
  }, [tried, isActive])

  return tried
}

export default useEagerConnect
