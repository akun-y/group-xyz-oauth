import {
  createContext,
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  useContext,
  useState,
} from "react"

const BATCH_SIZE = 24

const ExplorerContext = createContext<{
  renderedGroupsCount: number
  setRenderedGroupsCount: Dispatch<SetStateAction<number>>
}>(null)

const ExplorerProvider = ({ children }: PropsWithChildren<any>): JSX.Element => {
  const [renderedGroupsCount, setRenderedGroupsCount] = useState(BATCH_SIZE)

  return (
    <ExplorerContext.Provider
      value={{ renderedGroupsCount, setRenderedGroupsCount }}
    >
      {children}
    </ExplorerContext.Provider>
  )
}

const useExplorer = () => useContext(ExplorerContext)

export default ExplorerProvider
export { useExplorer, BATCH_SIZE }
