import { datadogRum } from "@datadog/browser-rum"
import { RumComponentContextProvider } from "@datadog/rum-react-integration"
import { PropsWithChildren, useEffect } from "react"

const Datadog = ({ children }: PropsWithChildren<unknown>): JSX.Element => {
  const url = typeof window !== "undefined" ? window.location.host : ""

  useEffect(() => {
    if (process.env.NODE_ENV !== "production" || url !== "group.xyz") return
    datadogRum.init({
      applicationId: "6439c9a2-55e3-40fd-9a98-6e02d5ed9b85",
      //8b204261677204e2b356c7e9ad94129777260d86
      clientToken: "30d6a379c675b56cc4a67a144881cbd7",
      site: "datadoghq.com",
      service: "group.xyz",
      env: "prod",
      silentMultipleInit: true,
      sampleRate: 100,
      trackInteractions: true,
      version: "1.0.0",
      proxyUrl: "/api/ddrum",
      beforeSend(event, _) {
        if (
          // We can ignore these 2 event types, since we can't really get useful information from them
          event.type === "resource" ||
          event.type === "long_task" ||
          // Don't send 3rd party handled errors (e.g. "MetaMask: received invalid isUnlocked parameter")
          (event.type === "error" &&
            ((event.error.source !== "custom" &&
              event.error.handling === "handled") ||
              // Ignoring this event, because it comes from a Chakra UI dependency
              event.error.type === "IgnoredEventCancel" ||
              event.error.message === "Script error."))
        )
          return false
      },
    })

    datadogRum.startSessionReplayRecording()
  }, [])

  return <RumComponentContextProvider componentName="App" {...{ children }} />
}

export default Datadog
