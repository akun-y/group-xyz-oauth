import * as bbsGate from "../../bbs-gate"
const isBrowser = () => typeof window !== "undefined"
const Casdoor = (): JSX.Element => {
  if (isBrowser) {
    return (
      <iframe id="iframeTask" src={`${bbsGate.getSigninUrl()}&silentSignin=1`} width={600} height={700} frameBorder="no" />
    )
  }
  return (
    <p>Loading...</p>
  )
}
export const SSOLoginIframe = (): JSX.Element => {
  if (isBrowser) {
    return (
      <iframe id="iframeTask" src={`${bbsGate.getSigninUrl()}&silentSignin=1&theme=dark`}
        width="100%" height={600} frameBorder="no" />
    )
  }
  return (
    <p>Loading...</p>
  )
}
export default Casdoor