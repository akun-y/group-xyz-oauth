import { Tag, TagLabel, TagLeftIcon, Tooltip, Wrap } from "@chakra-ui/react"
import DisplayCard from "components/common/DisplayCard"
import Link from "components/common/Link"
import { Users } from "phosphor-react"
import { GroupBase } from "types"
import pluralize from "utils/pluralize"

type Props = {
  groupData: GroupBase
}

const GroupCard = ({ groupData }: Props): JSX.Element => (
  <Link
    href={`/${groupData.urlName}`}
    prefetch={false}
    _hover={{ textDecor: "none" }}
    borderRadius="2xl"
    w="full"
    h="full"
  >
    <DisplayCard image={groupData.imageUrl} title={groupData.name}>
      <Wrap zIndex="1">
        <Tag as="li">
          <TagLeftIcon as={Users} />
          <TagLabel>{groupData.memberCount ?? 0}</TagLabel>
        </Tag>
        <Tooltip label={groupData.roles.join(", ")}>
          <Tag as="li">
            <TagLabel>{pluralize(groupData.roles?.length ?? 0, "role")}</TagLabel>
          </Tag>
        </Tooltip>
      </Wrap>
    </DisplayCard>
  </Link>
)

export default GroupCard
