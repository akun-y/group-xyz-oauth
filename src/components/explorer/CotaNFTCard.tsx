import { Tag, TagLabel, TagLeftIcon, Tooltip, Wrap } from "@chakra-ui/react"
import DisplayCard from "components/common/DisplayCard"
import Link from "components/common/Link"
import { Users } from "phosphor-react"
import { CotaNFTBase } from "types"
import pluralize from "utils/pluralize"

type Props = {
  nftData: CotaNFTBase
  bgColor?: string
}
//卡片形式，占用空间较小
const CotaNFTCard = ({ nftData,bgColor }: Props): JSX.Element => (
  <Link
    href={`https://pudge.explorer.nervos.org/nft-collections/${nftData.cotaId}`}
    prefetch={false}
    _hover={{ textDecor: "none" }}
    borderRadius="2xl"
    target={"_blank"}
    w="full"
    h="full"
  >
    <DisplayCard image={nftData.image} title={nftData.name} bgColor={bgColor}>
      <Wrap zIndex="1">
        <Tag as="li">
          <TagLeftIcon as={Users} />
          <TagLabel>{nftData.configure ?? 0}</TagLabel>
        </Tag>
        <Tooltip label={nftData.cotaId}>
          <Tag as="li">
            <TagLabel>{pluralize(nftData.cotaId?.length ?? 0, "role")}</TagLabel>
          </Tag>
        </Tooltip>
      </Wrap>
    </DisplayCard>
  </Link>
)

export default CotaNFTCard
