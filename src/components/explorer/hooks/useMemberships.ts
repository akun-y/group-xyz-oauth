import { useWeb3React } from "@web3-react/core"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useSWRImmutable from "swr/immutable"

export type Memberships = Array<{
  groupId: number
  roleIds: number[]
}>

const useMemberships = () => {
  const { account } = useWeb25React()

  const shouldFetch = !!account

  const { data } = useSWRImmutable<Memberships>(
    shouldFetch ? `/user/membership/${account}` : null
  )

  return data
}

export default useMemberships
