import { datadogRum } from "@datadog/browser-rum"
import { Web3Provider } from "@ethersproject/providers"
import { useWeb3React } from "@web3-react/core"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useKeyPair from "hooks/useKeyPair"
import { sign } from "hooks/useSubmit"
import { SignProps } from "hooks/useSubmit/useSubmit"
import useTimeInaccuracy from "hooks/useTimeInaccuracy"

const fetcher = async (
    resource: string,
    { body, validation, ...init }: Record<string, any> = {}
) => {
    const isGroupApiCall = !resource.startsWith("http") && !resource.startsWith("/api")
    const isServerless = resource.startsWith("/api")

    const api = isGroupApiCall ? process.env.NEXT_PUBLIC_API : ""

    const payload = body ?? {}

    const options = {
        ...(body
            ? {
                method: "POST",
                body: JSON.stringify(
                    validation
                        ? {
                            payload,
                            ...validation,
                        }
                        : body
                ),
            }
            : {}),
        ...init,
        headers: {
            ...(body ? { "Content-Type": "application/json" } : {}),
            ...init.headers,
        },
    }

    if (isGroupApiCall || isServerless)
        datadogRum?.addAction(`FETCH ${resource}`, {
            url: `${api}${resource}`,
            options,
            userAddress: resource.includes("checkPubKey")
                ? body.address?.toLowerCase()
                : undefined,
        })

    return fetch(`${api}${resource}`, options)
        .catch((err) => {
            datadogRum?.addError("Failed to fetch", {
                url: `${api}${resource}`,
                error: err?.message || err?.toString?.() || err,
            })
            throw err
        })
        .then(async (response: Response) => {
            // //console.info("response", response)
            // //console.info("response.json1", api)
            // //console.info("response.json2", resource)
            const res = await response?.json?.()

            if (!response.ok) {
                if (isGroupApiCall) {
                    const error = res.errors?.[0]
                    const errorMsg = error
                        ? `${error.msg}${error.param ? ` : ${error.param}` : ""}`
                        : res

                    datadogRum?.addError("FETCH ERROR", {
                        url: `${api}${resource}`,
                        response:
                            !error && resource.startsWith("/group/access")
                                ? "Access check error(s)"
                                : errorMsg,
                    })

                    return Promise.reject(errorMsg)
                }

                return Promise.reject(res)
            }

            return res
        })
}
const fetcherGate = async (
    resource: string,
    { body, validation, ...init }: Record<string, any> = {}
) => {
    if (!resource)
        return Promise.reject("No resource provided to fetcherGate")
    const isApiCall = !resource.startsWith("http")
    const isServerless = resource.startsWith("/api")

    const api = isApiCall ? process.env.NEXT_PUBLIC_GATE_API : ""

    const payload = body ?? {}

    const options = {
        ...(body
            ? {
                method: "POST",
                body: JSON.stringify(
                    validation
                        ? {
                            payload,
                            ...validation,
                        }
                        : body
                ), redirect: "follow"
            }
            : {}),
        ...init,
        headers: {
            ...(body ? { "Content-Type": "application/json" } : {}),
            ...init.headers,
        },
        redirect: "follow"
    }

    return fetch(`${api}${resource}`, options)
        .catch((err) => {
            datadogRum?.addError("Failed to fetch", {
                url: `${api}${resource}`,
                error: err?.message || err?.toString?.() || err,
            })
            throw err
        })
        .then(async (response: Response) => {
            const res = await response.json?.()
            if (!response.ok) {
                if (isApiCall) {
                    const error = res.errors?.[0]
                    const errorMsg = error
                        ? `${error.msg}${error.param ? ` : ${error.param}` : ""}`
                        : res

                    datadogRum?.addError("FETCH ERROR", {
                        url: `${api}${resource}`,
                        response:
                            !error && resource.startsWith("/group/access")
                                ? "Access check error(s)"
                                : errorMsg,
                    })

                    return Promise.reject(errorMsg)
                }

                return Promise.reject(res)
            }
            return res
        })
}
const fetcherParse2 = async (
    resource: string,
    { body, validation, ...init }: Record<string, any> = {}
) => {
    const isApiCall = !resource.startsWith("http") && !resource.startsWith("/api")
    const api = isApiCall ? process.env.NEXT_PUBLIC_PARSE_SERVER_URL : ""

    const payload = body ?? {}
    const options = {
        ...(body
            ? {
                method: "POST",
                body: JSON.stringify(
                    validation
                        ? {
                            payload,
                            ...validation,
                        }
                        : body
                ),
            }
            : {}),
        headers: {
            ...({
                "Content-Type": "application/json",
                "X-Parse-Application-Id": process.env.NEXT_PUBLIC_PARSE_APP_ID,
            }),
            ...init.headers,
        },
        redirect: "follow"
    }
    return fetch(`${api}${resource}`, options)
        .catch((err) => {
            datadogRum?.addError("Failed to fetch", {
                url: `${api}${resource}`,
                error: err?.message || err?.toString?.() || err,
            })
            throw err
        })
        .then(async (response: Response) => {
            const res = await response.json?.()
            if (!response.ok) {
                if (isApiCall) {
                    const error = res.errors?.[0]
                    const errorMsg = error
                        ? `${error.msg}${error.param ? ` : ${error.param}` : ""}`
                        : res

                    datadogRum?.addError("FETCH ERROR", {
                        url: `${api}${resource}`,
                        response:
                            !error && resource.startsWith("/group/access")
                                ? "Access check error(s)"
                                : errorMsg,
                    })

                    return Promise.reject(errorMsg)
                }

                return Promise.reject(res)
            }
            return res?.result ?? res?.results ?? res
        })
}
function catchFetchError(error) {

    console.log("fetch !response.ok", error)
    return Promise.reject(error)

}
const fetcherMPC = async (
    resource: string,
    { body, validation, ...init }: Record<string, any> = {}
) => {
    const isApiCall = !resource.startsWith("http")
    const api = isApiCall ? process.env.NEXT_PUBLIC_MPC_API : ""

    const payload = body ?? {}
    const options = {
        ...(body
            ? {
                method: "POST",
                body: JSON.stringify(
                    validation
                        ? {
                            payload,
                            ...validation,
                        }
                        : body
                ),
            }
            : {}),
        headers: {
            ...(body ? {
                "Content-Type": "application/json",
            } : {}),
            ...init.headers,
        },
        redirect: "follow"
    }
    return fetch(`${api}${resource}`, options)
        .catch((err) => {
            datadogRum?.addError("Failed to fetch", {
                url: `${api}${resource}`,
                error: err?.message || err?.toString?.() || err,
            })
            console.error("fethcerMPC Failed to fetch:", err)
            throw err
        })
        .then(async (response: Response) => {
            const res = await response.json?.()
            if (!response.ok) {
                //console.info("fetcherMPC !response.ok", response)

                const error = res.errors?.[0]
                const errorMsg = error
                    ? `${error.msg}${error.param ? ` : ${error.param}` : ""}`
                    : res

                datadogRum?.addError("FETCH ERROR", {
                    url: `${api}${resource}`,
                    response:
                        !error && resource.startsWith("/group/access")
                            ? "Access check error(s)"
                            : errorMsg,
                })
                ////console.info("fetcherMPC !response.ok", errorMsg)
                return {res,error,errorMsg};
            }
            return res
        })
}
const fetcherWithSign = async (
    signProps: Omit<SignProps, "payload" | "forcePrompt"> & {
        forcePrompt?: boolean
    },
    resource: string,
    { body, ...rest }: Record<string, any> = {}
) => {
    const validation = await sign({
        forcePrompt: false,
        ...signProps,
        payload: body,
    })

    return fetcher(resource, { body, validation, ...rest })
}

const useFetcherWithSign = () => {
    const { account, chainId, provider } = useWeb25React()
    const { keyPair } = useKeyPair()
    const timeInaccuracy = useTimeInaccuracy()

    return (resource: string, { signOptions, ...options }: Record<string, any> = {}) =>
        fetcherWithSign(
            {
                address: account,
                chainId: chainId.toString(),
                provider,
                keyPair,
                ts: Date.now() + timeInaccuracy,
                ...signOptions,
            },
            resource,
            options
        )
}

export { fetcherWithSign, useFetcherWithSign, fetcherParse2, fetcherMPC, fetcherGate }
export default fetcher
