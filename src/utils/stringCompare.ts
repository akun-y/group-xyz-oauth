//忽略大小写的字符串比较
export function stringCompare(a: string, b: string) {
    return a.localeCompare(b, undefined, { sensitivity: 'accent' });
}