const isEmpty = (val: any) => val == null || !(Object.keys(val) || val).length || val === "" || val === "undefined" || val === "null" || !val;

// If typescript compiler is yelling because of the type `any`
//const isEmpty = (val: Record<string, unknown> | null | undefined) =>
//  val == null || !(Object.keys(val) || val).length
// const test = () => {
//     isEmpty([]) // true
//     isEmpty({}) // true
//     isEmpty("") // true
//     isEmpty([1, 2]) // false
//     isEmpty({ a: 1, b: 2 }) // false
//     isEmpty("text") // false
//     isEmpty(123) // true - type is not considered a collection
//     isEmpty(true) // true - type is not considered a collection
// }

export default isEmpty