//找出两个数组中相同的元素
export function intersection<T>(a: T[], b: T[]): T[] {
    const s = new Set(b)
    return a.filter(x => s.has(x))
}
//找出数组a中不在数组b中的元素
export function difference<T>(a: T[], b: T[]): T[] {
    const s = new Set(b)
    return a.filter(x => !s.has(x))
}
//找出数组a和数组b中不同的元素
export function symmetricDifference<T>(a: T[], b: T[]): T[] {
    const sa = new Set(a)
    const sb = new Set(b)
    return [...a.filter(x => !sb.has(x)), ...b.filter(x => !sa.has(x))]
}