import Casdoor from 'components/casdoor'
import { useEffect, useState } from 'react'
function HomePage() {
  const [win, setWin] = useState(false)
  useEffect(() => {
    if (typeof window !== 'undefined') {
      setWin(true)
    }
  }, [])

  return (win) ? (<div style={{
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    textAlign: 'center',
  }}>
    < Casdoor />
  </div>) : (
    <div style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100vh",
      textAlign: 'center',
    }}>
      <div>Loading...</div>
    </div>)
}

export default HomePage