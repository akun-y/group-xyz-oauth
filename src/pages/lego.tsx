import { Container, Heading, SimpleGrid, Stack } from "@chakra-ui/react"
import LegoCard from "components/lego/LegoCard"
import Head from "next/head"

const Page = () => (
  <>
    <Head>
      <title>Group Lego</title>
      <meta name="og:title" content="Group Lego" />
    </Head>

    <Container maxW={{ base: "container.sm", lg: "container.lg" }}>
      <Stack pt={16}>
        <Heading
          fontFamily="display"
          fontSize={{ base: "4xl", md: "5xl", lg: "7xl" }}
          textAlign="center"
          mb={{ base: 10, md: 20 }}
        >
          Group Lego Assemblies
        </Heading>
        <SimpleGrid
          columns={{ lg: 2 }}
          spacing={{ base: "6", md: "8", lg: "10" }}
          flexGrow={1}
          pb={{ base: 10, md: 20 }}
        >
          <LegoCard
            href="/lego/BrandenburgGroupAssembly.pdf"
            img="/lego/brandenburg-group.png"
            name="Brandenburg Group"
            pieces={362}
          />
          <LegoCard
            href="/lego/ArcDeGroupAssembly.pdf"
            img="/lego/arc-de-group.png"
            name="Arc de Group"
            pieces={190}
          />
          <LegoCard
            href="/lego/LightGroupEmpireAssembly.pdf"
            img="/lego/group-empire-light.png"
            name="Group Empire"
            pieces={162}
          />
          <LegoCard
            href="/lego/DarkGroupEmpireAssembly.pdf"
            img="/lego/group-empire-dark.png"
            name="Group Empire"
            pieces={162}
          />
          <LegoCard
            href="/lego/GroupCastleAssembly.pdf"
            img="/lego/group-castle.png"
            name="Group Castle"
            pieces={59}
          />
          <LegoCard
            href="/lego/GroupDudeAssembly.pdf"
            img="/lego/group-dude.png"
            name="Group Dude"
            pieces={74}
          />
          <LegoCard
            href="/lego/GroupFoxAssembly.pdf"
            img="/lego/group-fox.png"
            name="Group Fox"
            pieces={58}
          />
          <LegoCard
            href="/lego/GroupGhostAssembly.pdf"
            img="/lego/group-ghost.png"
            name="Group Ghost"
            pieces={64}
          />
        </SimpleGrid>
      </Stack>
    </Container>
  </>
)

export default Page
