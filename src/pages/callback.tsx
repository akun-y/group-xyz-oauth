//仅仅用于casdoord登录
import AuthRedirect from "components/AuthRedirect"
import { useUserSSOByCode } from "hooks/bbsGate/useAccessToken"
import { useRouter } from "next/dist/client/router"
import { useEffect } from "react"
import { StorgeItemName, UserSSO } from "types"
type OAuthResponse = {
  error_description?: string
  error?: string
  state?: string
} & Record<string, any>
const {SSOID,SSOTOKEN,USERSSO,MPCTOKEN,ACCOUNT,COTAADDRESS,SSOREFRESHTOKEN,SSONAME}=StorgeItemName
const OAuth = () => {
  const router = useRouter()
  useEffect(() => {
    const interval = setInterval(() => {
      try {
        const shouldClose = JSON.parse(
          window.localStorage.getItem("oauth_window_should_close")
        )
        if (shouldClose) {
          window.localStorage.removeItem("oauth_window_should_close")
          window.close()
        }
      } catch { }
    }, 800)

    return () => clearInterval(interval)
  }, [])

  useEffect(() => {
    if (!router.isReady || typeof window === "undefined") return

    // We navigate to the index page if the oauth page is used incorrectly
    // For example if someone just manually goes to /oauth

    let params: OAuthResponse = {}

    if (typeof router.query?.state !== "string") {
      if (!window.location.hash) router.push("/")
      const fragment = new URLSearchParams(window.location.hash.slice(1))
      params = Object.fromEntries(fragment.entries())
    } else {
      params = router.query
    }

    if (Object.keys(params).length <= 0) router.push("/")

    if (params.error) {
      const { error, errorDescription } = params
      window.localStorage.setItem(
        "oauth_popup_data",
        JSON.stringify({
          type: "OAUTH_ERROR",
          data: { error, errorDescription },
        })
      )
      return
    }
    //console.info("params------------:", params)
    const [clientId, csrfToken] = params.state?.split(";") ?? [undefined, undefined]
    //console.info("clientId:", clientId)
    //console.info("csrfToken:", csrfToken)

    if (params.code && params.state) {
      window.localStorage.setItem(
        "oauth_popup_data",
        JSON.stringify({
          type: "OAUTH_SUCCESS",
          data: { code: params.code, state: params.state },
        })
      )
      useUserSSOByCode(params.code)
      .then((res) => {
        //console.info("useUserSSOByCode:", res)
        if (res?.data) {
          const u: UserSSO = res.data
          //console.info("useUserSSOByCode:", u)
          window.localStorage.setItem(SSOID, u.sub)
          window.localStorage.setItem(SSONAME, u.name)
          window.localStorage.setItem(USERSSO, JSON.stringify(u))
          window.localStorage.setItem(SSOTOKEN, res.access_token)
          window.localStorage.setItem(SSOREFRESHTOKEN, res.refresh_token)
          window.top.location.href = "/web2-home"
          //router.push("/web2-home?code=" + params.code + "&state=" + params.state)
        } else {
          console.error("useUserSSOByCode:", res)
          window.top.location.href = "/web2-login"
        }
      }).catch((e) => {
        console.error("useUserSSOByCode",e)
      })
      //window.top.location.href = "/web2-home?code=" + params.code + "&state=" + params.state
    } else {
      window.localStorage.setItem(
        "oauth_popup_data",
        JSON.stringify({
          type: "OAUTH_ERROR",
          data: { error: "invalid_state", errorDescription: "Invalid state" },
        })
      )
    }

  }, [router])

  return <AuthRedirect />
}
export default OAuth
