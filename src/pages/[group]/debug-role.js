// 请求网址: https://api.group.xyz/v1/group/access/14797/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 206 
const res5=[
    {
      "roleId": 22940,
      "access": true,
      "requirements": [
        {
          "requirementId": 118356,
          "access": true,
          "amount": null
        }
      ]
    },
    {
      "roleId": 24469,
      "access": null,
      "requirements": [
        {
          "requirementId": 127963,
          "access": null,
          "amount": null
        }
      ],
      "errors": [
        {
          "requirementId": 127963,
          "msg": "contract call failed",
          "errorType": "REQUIREMENT_INVALID",
          "statusCode": 470
        }
      ]
    }
  ]
//   请求网址: https://api.group.xyz/v1/role/22940
//   请求方法: PATCH
//   状态代码: 200 
const payload ={
    "payload": {
      "roleId": 22940,
      "name": "Member",
      "description": "3434",
      "imageUrl": "/guildLogos/223.svg",
      "logic": "AND",
      "requirements": [
        {
          "type": "FREE",
          "data": {}
        }
      ],
      "rolePlatforms": [
        {
          "id": 11027,
          "platformRoleId": "1042423850987700224",
          "guildPlatformId": 6640
        }
      ]
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "3OE8RK7/qp0ek9ngSTrWpl/etIDkNS/ipx28b9L9+Sk=",
      "ts": "1670389557626",
      "hash": "0x1b01a98eecdb00c2f1e3e63065f8992a1a10b3629d2bb9bed3c05a228c617031",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "0a00fca882377112da0fb37e823c87ca984cd0dca5e546079129b5667e1b603618f620dfb3b1c3ef14261f95cf92380302f2ddec4e85f76f3d2d9093a982bf54"
}
  const res = {
    "id": 22940,
    "name": "Member",
    "description": "3434",
    "imageUrl": "/guildLogos/223.svg",
    "createdAt": "2022-11-16T13:00:12.927Z",
    "hidden": false,
    "logic": "AND",
    "guildId": 14797,
    "requirements": [
      {
        "id": 128107,
        "type": "FREE",
        "address": null,
        "data": {},
        "symbol": "-",
        "name": "-",
        "chain": "ETHEREUM",
        "roleId": 22940
      }
    ]
  }

// 请求网址: https://api.group.xyz/v1/role/22133
// 请求方法: GET
// 状态代码: 200 
const res0= {
    "id": 22133,
    "name": "Member",
    "description": "123456",
    "imageUrl": "/guildLogos/88.svg",
    "createdAt": "2022-11-07T03:45:36.829Z",
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 115292,
        "type": "FREE",
        "chain": "ETHEREUM",
        "address": null,
        "data": null
      }
    ],
    "rolePlatforms": [
      {
        "guildPlatformId": 6257,
        "platformRoleId": "1039022788838707211",
        "guildPlatform": {
          "id": 6257,
          "platformGroupId": "985740354131218472",
          "platformGroupData": {
            "invite": "https://discord.gg/theW5CqpMH",
            "inviteChannel": "1039022942744485888"
          },
          "platformGroupName": "akun's server",
          "invite": "https://discord.gg/theW5CqpMH",
          "platform": {
            "id": 1,
            "name": "DISCORD"
          }
        }
      }
    ],
    "members": [
      "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
    ]
  }
// 请求网址: https://api.group.xyz/v1/role/22133
// 请求方法: PATCH
// 状态代码: 200 
const payload = 
{
    "payload": {
      "roleId": 22133,
      "name": "Member",
      "description": "123456",
      "imageUrl": "/guildLogos/88.svg",
      "logic": "AND",
      "requirements": [
        {
          "id": 115292,
          "type": "FREE",
          "chain": "ETHEREUM"
        }
      ],
      "rolePlatforms": [
        {
          "id": 10285,
          "platformRoleId": "1039022788838707211",
          "guildPlatformId": 6257
        }
      ]
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "TTFVG6Vj2W3gGbpMuz1tgrmmhlijx7yBDvm6PeH8qe0=",
      "ts": "1670385856461",
      "hash": "0xecf693d7ca250f6d234c398a883eb4ce5191a03e1758cb6b1e56324ecab1c2b7",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "a131633a03991a8023f85dbff62d5e89cd6fa694af33a8d785e06098bae38ae7ec322715ca494403226b7864133f4405ab3e77267b283c1648115699cdaab1a4"
}
  const res = 
{
    "id": 22133,
    "name": "Member",
    "description": "123456",
    "imageUrl": "/guildLogos/88.svg",
    "createdAt": "2022-11-07T03:45:36.829Z",
    "hidden": false,
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 115292,
        "type": "FREE",
        "address": null,
        "data": null,
        "symbol": "-",
        "name": "-",
        "chain": "ETHEREUM",
        "roleId": 22133
      }
    ]
  }

//   请求网址: https://api.group.xyz/v1/group/access/14434/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
const res2=[
    {
      "roleId": 22133,
      "access": true,
      "requirements": [
        {
          "requirementId": 115292,
          "access": true,
          "amount": null
        }
      ]
    },
    {
      "roleId": 24256,
      "access": true,
      "requirements": [
        {
          "requirementId": 126861,
          "access": true,
          "amount": null
        }
      ]
    },
    {
      "roleId": 24465,
      "access": true,
      "requirements": [
        {
          "requirementId": 127950,
          "access": true,
          "amount": null
        }
      ]
    }
]
  
// 请求网址: https://group.xyz/api/ddrum?ddforward=https%3A%2F%2Frum.browser-intake-datadoghq.eu%2Fapi%2Fv2%2Frum%3Fddsource%3Dbrowser%26ddtags%3Dsdk_version%253A4.25.0%252Cenv%253Aprod%252Cservice%253Aguild.xyz%252Cversion%253A1.0.0%26dd-api-key%3Dpub7cf22f3b79a010363cf58c859cfa8ad8%26dd-evp-origin-version%3D4.25.0%26dd-evp-origin%3Dbrowser%26dd-request-id%3D26136878-ff54-4689-8e3b-843d9955a705%26batch_time%3D1670386165744
// 请求方法: POST
// 状态代码: 202 
const payload= {
    
        "_dd": {
          "format_version": 2,
          "drift": -1,
          "session": {
            "plan": 2
          },
          "document_version": 9,
          "replay_stats": {
            "records_count": 841,
            "segments_count": 4,
            "segments_total_raw_size": 586462
          }
        },
        "application": {
          "id": "996b7a2a-d610-4235-a5b4-65391973ea76"
        },
        "date": 1670386043772,
        "service": "group.xyz",
        "version": "1.0.0",
        "source": "browser",
        "session": {
          "id": "14088e0a-ffec-4dce-850e-f13977b91598",
          "type": "user",
          "has_replay": true
        },
        "view": {
          "id": "825c7b4b-9e23-4f3b-bb42-ace2b3135935",
          "url": "https://group.xyz/akuns-server",
          "referrer": "",
          "action": {
            "count": 24
          },
          "frustration": {
            "count": 0
          },
          "cumulative_layout_shift": 0.2313,
          "first_byte": 166500000,
          "dom_complete": 4718500000,
          "dom_content_loaded": 1841700000,
          "dom_interactive": 1695900000,
          "error": {
            "count": 0
          },
          "first_contentful_paint": 1863900000,
          "first_input_delay": 7100000,
          "first_input_time": 82827400000,
          "is_active": true,
          "largest_contentful_paint": 2065200000,
          "load_event": 4719300000,
          "loading_time": 5343000000,
          "loading_type": "initial_load",
          "long_task": {
            "count": 0
          },
          "resource": {
            "count": 0
          },
          "time_spent": 122366000000,
          "in_foreground_periods": [
            {
              "start": 1968400000,
              "duration": 4003500000
            },
            {
              "start": 82833700000,
              "duration": 35210900000
            },
            {
              "start": 119337200000,
              "duration": 2177200000
            }
          ]
        },
        "type": "view"
      }
  

const res3={
    "request_id": "26136878-ff54-4689-8e3b-843d9955a705"
}
  
// 请求网址: https://api.group.xyz/v1/role
// 请求方法: POST
// 状态代码: 201
const payload2=
{
    "payload": {
      "guildId": 14434,
      "name": "role-test",
      "description": "tttttttt",
      "logic": "AND",
      "requirements": [
        {
          "type": "ALLOWLIST",
          "data": {
            "hideAllowlist": true,
            "addresses": [
              "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
              "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa2"
            ]
          }
        }
      ],
      "imageUrl": "/guildLogos/237.svg",
      "rolePlatforms": [
        {
          "guildPlatformId": 6257,
          "platformRoleData": {},
          "isNew": true
        }
      ]
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "t8d5KtsIZGT71dxk3EUbEEW/TpkMe7lWbRntbsXahJA=",
      "ts": "1670386371079",
      "hash": "0x68907dadafd1c8f6d19d2a9a482ccafc10bdcfbb08ad323373d32d954caa53d7",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "2f63e3eaa7703579ef245f3529b1603020ae01fa0e3bb22f9b9f4f26a4a1bae0d9817f9f39069e88a90c903b3754bc9a512c99816db25930d2c2b5c0a58d919c"
}
  const res4= {
    "id": 24534,
    "name": "role-test",
    "description": "tttttttt",
    "imageUrl": "/guildLogos/237.svg",
    "createdAt": "2022-12-07T04:12:52.043Z",
    "hidden": false,
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 128096,
        "type": "ALLOWLIST",
        "address": null,
        "data": {
          "hideAllowlist": true,
          "addresses": [
            "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
            "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa2"
          ]
        },
        "symbol": "-",
        "name": "-",
        "chain": "ETHEREUM",
        "roleId": 24534
      }
    ]
  }