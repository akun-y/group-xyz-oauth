// 请求网址: https://api.group.xyz/v1/group/listGateables
// 请求方法: POST
// 状态代码: 201 
// 远程地址: [2606:4700:20::681a:773]:443

const payload ={
  "payload": {
    "platformName": "DISCORD"
  },
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "I+i1lD5kR2mfZI0KFuzJzt0dlrvoFIH1NQIoOTKBMV0=",
    "ts": "1670418495773",
    "hash": "0x4df7d39a82523ee14f9a35712ae2c8286a0c873bda3ddbd22921345f5d9f8324",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "5639b9dafcc2e40b309589f641f40feb07508787d33c4ed2a45c307157537f06f15ea51d996aca931bbd0b7a9884240768f8db0b205857acd6371a6838afaa5b"
}
const res =[
  {
    "img": "https://cdn.discordapp.com/icons/883317833734430731/a7aca168282d42d432c6edc00c06ce84.png",
    "id": "883317833734430731",
    "name": "Rebase Community",
    "owner": false
  },
  {
    "img": "/default_discord_icon.png",
    "id": "972848476792688691",
    "name": "aaa",
    "owner": true
  },
  {
    "img": "/default_discord_icon.png",
    "id": "983976647868948530",
    "name": "akun‘s server 2",
    "owner": true
  },
  {
    "img": "/default_discord_icon.png",
    "id": "985740354131218472",
    "name": "akun's server",
    "owner": true
  }
]

// 请求网址: https://api.group.xyz/v1/discord/server/972848476792688691
// 请求方法: POST
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade

const res2=
{
  "serverIcon": "",
  "serverName": "",
  "serverId": "972848476792688691",
  "channels": [],
  "roles": [],
  "isAdmin": null,
  "membersWithoutRole": null
}
//创建
// 请求网址: https://api.group.xyz/v1/group
// 请求方法: POST
// 状态代码: 201 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade
const payload3= {
  "payload": {
    "name": "aaa",
    "description": "",
    "imageUrl": "/guildLogos/71.svg",
    "guildPlatforms": [
      {
        "platformName": "DISCORD",
        "platformGroupId": "972848476792688691",
        "platformGroupData": {
          "inviteChannel": ""
        }
      }
    ],
    "roles": [
      {
        "name": "Member",
        "logic": "AND",
        "imageUrl": "/guildLogos/258.svg",
        "requirements": [
          {
            "type": "FREE"
          }
        ],
        "rolePlatforms": [
          {
            "guildPlatformIndex": 0
          }
        ]
      }
    ]
  },
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "VOIgg8fOPrs+26X+VGf4Cr2bBbjAJ3/f48XcBZu10+M=",
    "ts": "1670418998720",
    "hash": "0xbe7308564dcf6b9f06889bed8ca2b0fbe0d751c06255f276771499f876d9aad7",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "6d2dbf1f746b13fee21a7b25fde92c9b66ad7606b618708ada62277519eac2bc26a6c1156e02d7d343980f8154038e08706c66f564b1b436ced241c390db615b"
}

const res5={
  "id": 15496,
  "name": "aaa",
  "urlName": "aaa-f9439e",
  "description": "",
  "imageUrl": "/guildLogos/71.svg",
  "showMembers": true,
  "hideFromExplorer": false,
  "createdAt": "2022-12-07T13:16:40.691Z",
  "onboardingComplete": false,
  "admins": [
    {
      "id": 241503,
      "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "isOwner": true
    }
  ],
  "theme": {
    "mode": "DARK",
    "color": null,
    "backgroundImage": null,
    "backgroundCss": null
  },
  "poaps": [],
  "guildPlatforms": [
    {
      "id": 7390,
      "platformId": 1,
      "platformGroupId": "972848476792688691",
      "platformGroupData": {
        "joinButton": false,
        "inviteChannel": "972848477312798762"
      },
      "platformGroupName": "aaa",
      "invite": "https://discord.gg/D4KqTx5ZbQ"
    }
  ],
  "roles": [
    {
      "id": 24601,
      "name": "Member",
      "logic": "AND",
      "imageUrl": "/guildLogos/258.svg",
      "description": null,
      "requirements": [
        {
          "id": 128209,
          "data": null,
          "name": "-",
          "type": "FREE",
          "chain": "ETHEREUM",
          "roleId": 24601,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 12684,
          "platformRoleId": "1050038143199481856",
          "guildPlatformId": 7390,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    }
  ]
}

// 请求网址: https://group.xyz/api/event
// 请求方法: POST
// 状态代码: 202 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade

const payload4= {
  "n": "pageview",
  "u": "https://group.xyz/aaa-f9439e",
  "d": "group.xyz",
  "r": null,
  "w": 988
}
const res6 = "ok"
//create group 之后
// 请求网址: https://api.group.xyz/v1/user/membership/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade
const res7=[
  {
    "guildId": 1985,
    "roleIds": [
      1904
    ],
    "isAdmin": false
  },
  {
    "guildId": 3474,
    "roleIds": [
      3883
    ],
    "isAdmin": false
  },
  {
    "guildId": 3718,
    "roleIds": [
      4220
    ],
    "isAdmin": false
  },
  {
    "guildId": 13468,
    "roleIds": [
      19924
    ],
    "isAdmin": false
  },
  {
    "guildId": 14434,
    "roleIds": [
      24465,
      24534,
      22133,
      24256
    ],
    "isAdmin": true
  },
  {
    "guildId": 14797,
    "roleIds": [
      22940
    ],
    "isAdmin": true
  },
  {
    "guildId": 15496,
    "roleIds": [
      24601
    ],
    "isAdmin": true
  }
]

// 请求网址: https://api.group.xyz/v1/group/access/15496/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade
const res8=[
  {
    "roleId": 24601,
    "access": true,
    "requirements": [
      {
        "requirementId": 128209,
        "access": true,
        "amount": null
      }
    ]
  }
]

// 请求网址: https://api.group.xyz/v1/group/aaa-f9439e
// 请求方法: GET
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade
const res9={
  "id": 15496,
  "name": "aaa",
  "urlName": "aaa-f9439e",
  "description": "",
  "imageUrl": "/guildLogos/71.svg",
  "showMembers": true,
  "hideFromExplorer": false,
  "createdAt": "2022-12-07T13:16:40.691Z",
  "onboardingComplete": false,
  "admins": [
    {
      "id": 241503,
      "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "isOwner": true
    }
  ],
  "theme": {
    "mode": "DARK",
    "color": null,
    "backgroundImage": null,
    "backgroundCss": null
  },
  "poaps": [],
  "guildPlatforms": [
    {
      "id": 7390,
      "platformId": 1,
      "platformGroupId": "972848476792688691",
      "platformGroupData": {
        "joinButton": false,
        "inviteChannel": "972848477312798762"
      },
      "platformGroupName": "aaa",
      "invite": "https://discord.gg/D4KqTx5ZbQ"
    }
  ],
  "roles": [
    {
      "id": 24601,
      "name": "Member",
      "logic": "AND",
      "imageUrl": "/guildLogos/258.svg",
      "description": null,
      "requirements": [
        {
          "id": 128209,
          "data": null,
          "name": "-",
          "type": "FREE",
          "chain": "ETHEREUM",
          "roleId": 24601,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 12684,
          "platformRoleId": "1050038143199481856",
          "guildPlatformId": 7390,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    }
  ]
}

// 请求网址: https://api.group.xyz/v1/group/details/aaa-f9439e
// 请求方法: POST
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade

const payload10={
  "payload": {},
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "+iFkzFGx8SPo6Q2bYMIEIUIza/spVxJNzJLTG6f7NkA=",
    "ts": "1670419006803",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "781761d773fd6e765ebf96d6957173274e1cbfb1f486c2a066f838357afcad5ea2c29393ae1e4dfaeffdea4f2f397597bf6f6d83330549ee66013b4bd7703c32"
}
const res10={
  "id": 15496,
  "name": "aaa",
  "urlName": "aaa-f9439e",
  "description": "",
  "imageUrl": "/guildLogos/71.svg",
  "showMembers": true,
  "hideFromExplorer": false,
  "createdAt": "2022-12-07T13:16:40.691Z",
  "onboardingComplete": false,
  "admins": [
    {
      "id": 241503,
      "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "isOwner": true
    }
  ],
  "theme": {
    "mode": "DARK",
    "color": null,
    "backgroundImage": null,
    "backgroundCss": null
  },
  "poaps": [],
  "guildPlatforms": [
    {
      "id": 7390,
      "platformId": 1,
      "platformGroupId": "972848476792688691",
      "platformGroupData": {
        "joinButton": false,
        "inviteChannel": "972848477312798762"
      },
      "platformGroupName": "aaa",
      "invite": "https://discord.gg/D4KqTx5ZbQ"
    }
  ],
  "roles": [
    {
      "id": 24601,
      "name": "Member",
      "logic": "AND",
      "imageUrl": "/guildLogos/258.svg",
      "description": null,
      "requirements": [
        {
          "id": 128209,
          "data": null,
          "name": "-",
          "type": "FREE",
          "chain": "ETHEREUM",
          "roleId": 24601,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 12684,
          "platformRoleId": "1050038143199481856",
          "guildPlatformId": 7390,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    }
  ]
}

// 请求网址: https://api.group.xyz/v1/role/24601
// 请求方法: GET
// 状态代码: 200 
// 远程地址: 0.0.0.0:1087
// 引荐来源网址政策: no-referrer-when-downgrade
const res11={
  "id": 24601,
  "name": "Member",
  "description": null,
  "imageUrl": "/guildLogos/258.svg",
  "createdAt": "2022-12-07T13:16:41.491Z",
  "logic": "AND",
  "guildId": 15496,
  "requirements": [
    {
      "id": 128209,
      "type": "FREE",
      "chain": "ETHEREUM",
      "address": null,
      "data": null
    }
  ],
  "rolePlatforms": [
    {
      "guildPlatformId": 7390,
      "platformRoleId": "1050038143199481856",
      "guildPlatform": {
        "id": 7390,
        "platformGroupId": "972848476792688691",
        "platformGroupData": {
          "joinButton": false,
          "inviteChannel": "972848477312798762"
        },
        "platformGroupName": "aaa",
        "invite": "https://discord.gg/D4KqTx5ZbQ",
        "platform": {
          "id": 1,
          "name": "DISCORD"
        }
      }
    }
  ],
  "members": [
    "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
  ]
}