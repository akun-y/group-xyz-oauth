import {
  Box, Button, Center, Collapse,
  Heading, HStack, Spinner, Text
} from "@chakra-ui/react"
import { WithRumComponentContext } from "@datadog/rum-react-integration"
import GroupLogo from "components/common/GroupLogo"
import Layout from "components/common/Layout"
import LinkPreviewHead from "components/common/LinkPreviewHead"
import Section from "components/common/Section"
import AccessHub from "components/[group]/AccessHub"
import useAccess from "components/[group]/hooks/useAccess"
import useAutoStatusUpdate from "components/[group]/hooks/useAutoStatusUpdate"
import useGroup from "components/[group]/hooks/useGroup"
import useGroupPermission from "components/[group]/hooks/useGroupPermission"
import useIsMember from "components/[group]/hooks/useIsMember"
import JoinButton from "components/[group]/JoinButton"
import JoinModalProvider from "components/[group]/JoinModal/JoinModalProvider"
import LeaveButton from "components/[group]/LeaveButton"
import Members from "components/[group]/Members"
import OnboardingProvider from "components/[group]/Onboarding/components/OnboardingProvider"
import RoleCard from "components/[group]/RoleCard/RoleCard"
import Tabs from "components/[group]/Tabs/Tabs"
import { ThemeProvider, useThemeContext } from "components/[group]/ThemeContext"
import useKeyPair from "hooks/useKeyPair"
import useUniqueMembers from "hooks/useUniqueMembers"
import { GetStaticPaths, GetStaticProps } from "next"
import dynamic from "next/dynamic"
import Head from "next/head"
import ErrorPage from "pages/_error"
import React, { useEffect, useMemo, useState } from "react"
import { SWRConfig } from "swr"
import { Group, Group } from "types"
import { fetcherParse2 } from "utils/fetcher"



const GroupPage = (): JSX.Element => {
  const { name, description:description, imageUrl, showMembers, roles, isLoading, onboardingComplete,
  } = useGroup()

  useAutoStatusUpdate()

  const { data: roleAccesses } = useAccess()
  const sortedRoles = useMemo(() => {
    const byMembers = roles?.sort(
      (role1, role2) => role2.memberCount - role1.memberCount
    )
    if (!roleAccesses) return byMembers

    // prettier-ignore
    const accessedRoles = [], otherRoles = []
    byMembers?.forEach((role) =>
      (roleAccesses?.find(({ roleId }) => roleId === role.id)?.access
        ? accessedRoles
        : otherRoles
      ).push(role)
    )
    return accessedRoles.concat(otherRoles)
  }, [roles, roleAccesses])

  const [DynamicEditGroupButton, setDynamicEditGroupButton] = useState(null)
  const [DynamicAddRoleButton, setDynamicAddRoleButton] = useState(null)
  const [DynamicAddRewardButton, setDynamicAddRewardButton] = useState(null)
  const [DynamicMembersExporter, setDynamicMembersExporter] = useState(null)
  const [DynamicOnboarding, setDynamicOnboarding] = useState(null)

  const { isAdmin } = useGroupPermission()
  const isMember = useIsMember()

  const members = useUniqueMembers(roles)
  const { textColor, localThemeColor, localBackgroundImage } = useThemeContext()
  const { ready, set, keyPair } = useKeyPair()

  //console.info("GroupPage",members, isMember, isAdmin, onboardingComplete)

  useEffect(() => {
    if (isAdmin) {
      const EditGroupButton = dynamic(() => import("components/[group]/EditGroup"))
      const AddRoleButton = dynamic(() => import("components/[group]/AddRoleButton"))
      const AddRewardButton = dynamic(
        () => import("components/[group]/AddRewardButton")
      )
      const MembersExporter = dynamic(
        () => import("components/[group]/Members/components/MembersExporter")
      )
      setDynamicEditGroupButton(EditGroupButton)
      setDynamicAddRoleButton(AddRoleButton)
      setDynamicAddRewardButton(AddRewardButton)
      setDynamicMembersExporter(MembersExporter)

      if (!onboardingComplete) {
        const Onboarding = dynamic(() => import("components/[group]/Onboarding"))
        setDynamicOnboarding(Onboarding)
      }
    } else {
      setDynamicEditGroupButton(null)
      setDynamicAddRoleButton(null)
    }
  }, [isAdmin])

  // not importing it dinamically because that way the whole page flashes once when it loads
  const DynamicOnboardingProvider = DynamicOnboarding
    ? OnboardingProvider
    : React.Fragment

  const showOnboarding = DynamicOnboarding && !onboardingComplete
  const showAccessHub = (isMember || isAdmin) && !showOnboarding

  return (
    <DynamicOnboardingProvider>
      <Layout
        title={name}
        textColor={textColor}
        description={description}
        showLayoutDescription
        image={
          <GroupLogo
            imageUrl={imageUrl}
            size={{ base: "56px", lg: "72px" }}
            mt={{ base: 1, lg: 2 }}
            bgColor={textColor === "primary.800" ? "primary.800" : "transparent"}
          />
        }
        background={localThemeColor}
        backgroundImage={localBackgroundImage}
        action={DynamicEditGroupButton && <DynamicEditGroupButton />}
      >
        {DynamicOnboarding && <DynamicOnboarding />}

        {!showOnboarding && (
          <Tabs tabTitle={showAccessHub ? "Home" : "Roles"}>
            {isMember ? (
              <HStack>
                {DynamicAddRewardButton && <DynamicAddRewardButton />}
                <LeaveButton />
              </HStack>
            ) : (
              <HStack>
                {DynamicAddRewardButton && <DynamicAddRewardButton />}
                <JoinButton />
              </HStack>
            )}
          </Tabs>
        )}

        <Collapse in={showAccessHub} unmountOnExit>
          <AccessHub />
        </Collapse>

        <Section
          title={(showAccessHub || showOnboarding) && "Roles"}
          titleRightElement={
            (showAccessHub || showOnboarding) &&
            DynamicAddRoleButton && (
              <Box my="-2 !important" ml="auto !important">
                <DynamicAddRoleButton />
              </Box>
            )
          }
          spacing={4}
          mb="12"
        >
          {sortedRoles?.map((role) => (
            <RoleCard key={role.id} role={role} />
          ))}
        </Section>

        {(showMembers || isAdmin) && (
          <Section
            title="Members"
            titleRightElement={
              <HStack justifyContent="end" w="full">
                {/* <HStack justifyContent="space-between" w="full"> */}
                {/* <Tag size="sm" maxH={6} pt={1}>
                  {isLoading ? (
                    <Spinner size="xs" />
                  ) : (
                    members?.filter((address) => !!address)?.length ?? 0
                  )}
                </Tag> */}
                {DynamicMembersExporter && <DynamicMembersExporter />}
              </HStack>
            }
          >
            {showMembers ? (
              <Members members={members} />
            ) : (
              <Text>Members are hidden</Text>
            )}
          </Section>
        )}
      </Layout>
    </DynamicOnboardingProvider>
  )
}

type Props = {
  fallback: { string: Group }
}

const GroupPageWrapper = ({ fallback }: Props): JSX.Element => {
  const group = useGroup()

  if (!fallback) {
    if (group.isLoading)
      return (
        <Center h="100vh" w="screen">
          <Spinner />
          <Heading fontFamily={"display"} size="md" ml="4" mb="1">
            Loading group...
          </Heading>
        </Center>
      )

    if (!group.id) return <ErrorPage statusCode={404} />
  }

  return (
    <>
      <LinkPreviewHead
        path={fallback ? Object.values(fallback)[0].urlName : group.urlName}
      />
      <Head>
        <title>{fallback ? Object.values(fallback)[0].name : group.name}</title>
        <meta
          property="og:title"
          content={fallback ? Object.values(fallback)[0].name : group.name}
        />
      </Head>
      <SWRConfig value={fallback && { fallback }}>
        <ThemeProvider>
          <JoinModalProvider>
            <GroupPage />
          </JoinModalProvider>
        </ThemeProvider>
      </SWRConfig>
    </>
  )
}

const getStaticProps: GetStaticProps = async ({ params }) => {
  //params.group=sdfgsdfg
  const endpoint = "/parse/functions/queryGroup"
  //const endpoint = `/parse/classes/Groups/hhCh68cCyP`
  const data = await fetcherParse2(endpoint, { method: "POST", body: { urlName: params.group } }
  )
    .catch((err) => console.error(err))
  //const endpoint = `/group/${params.group?.toString()}`
  //const data = await fetcher(endpoint).catch((_) => ({}))
  //console.info("data:", endpoint, data)
  if (!data?.objectId)
    return {
      props: {},
      revalidate: 10,
    }
  // Removing the members list, and then we refetch them on client side. This way the members won't be included in the SSG source code.
  const filteredData = { ...data }
  filteredData.roles?.forEach((role) => (role.members = []))

  // Fetching requirements client-side in this case
  if (filteredData.roles?.some((role) => role.requirements?.length > 10)) {
    filteredData.roles?.forEach((role) => (role.requirements = []))
  }

  return {
    props: {
      fallback: {
        [endpoint]: filteredData,
      },
    },
    revalidate: 10,
  }
}

const SSG_PAGES_COUNT = 24
const getStaticPaths: GetStaticPaths = async () => {
  const mapToPaths = (_: Group[]) =>
    Array.isArray(_)
      ? _.slice(0, SSG_PAGES_COUNT).map(({ urlName: group }) => ({
        params: { group },
      }))
      : []

  const paths = await fetcherParse2(`/parse/classes/Groups`)
    .then(res => {
      //console.info("res:", res)
      //const { results } = res
      return mapToPaths(res)
    })
  //const paths = await fetcher(`/group`).then(mapToPaths)
  //console.info(`SSG paths: ${paths.length}`, paths)
  return {
    paths,
    fallback: "blocking",
  }
}

export { getStaticPaths, getStaticProps }

export default WithRumComponentContext("Group page", GroupPageWrapper)
