// 请求网址: https://api.group.xyz/v1/user/upvotyAuth/0x2f6c33Fba01f042C831790FA6FcF036a5f5C1958
// 请求方法: GET
// 状态代码: 204 

// access-control-allow-origin: http://localhost:5001
// alt-svc: h3=":443"; ma=86400, h3-29=":443"; ma=86400
// cf-cache-status: DYNAMIC
// cf-ray: 7759db583f167e0e-LAX
// date: Wed, 07 Dec 2022 02:33:30 GMT
// nel: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
// report-to: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v3?s=YzgWAnm9Llivy3BfwIvXHV8dAhJxh2xd8LcPIW%2BkdzbbqRm78QSjltCNsz8jnfbmAosScGAc4j9LPiw149vLDxL9bZHiT%2FOLzklhL8HRIyIQwhI1%2FLbDlwnsKO38rZdHfgvWLtKVS0KJ3OA%3D"}],"group":"cf-nel","max_age":604800}
// server: cloudflare
// vary: Origin
// x-content-type-options: nosniff
// x-frame-options: DENY
// x-xss-protection: 1; mode=block

// 请求网址: https://api.group.xyz/v1/user/leaveGroup
// 请求方法: POST
// 状态代码: 200 
const payload={
  "payload": {
    "guildId": 9267
  },
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "uAI/u8dJdVvUS2jpZ33KcryNn28lLEP7U0ltoCT0ygY=",
    "ts": "1670586613190",
    "hash": "0xc5634b4d75b6de5cb17e9e7198f4865b996792a62a827714a7b1cba1c254fb74",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "345b50d4a099022556e9a45fd3e51453202d6b6c27de6e2fbb419b2bf4b68282602d5b104afd37d1de0346eaec48424959e269bbdd7de0035ea35020224d10e9"
}
const res= true

// 请求网址: https://api.group.xyz/v1/user/membership/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
const res =[
  {
    "guildId": 1985,
    "roleIds": [
      1904
    ],
    "isAdmin": false
  },
  {
    "guildId": 3474,
    "roleIds": [
      3883
    ],
    "isAdmin": false
  },
  {
    "guildId": 3718,
    "roleIds": [
      4220
    ],
    "isAdmin": false
  },
  {
    "guildId": 13468,
    "roleIds": [
      19924
    ],
    "isAdmin": false
  },
  {
    "guildId": 14434,
    "roleIds": [
      24465,
      24534,
      22133,
      24256
    ],
    "isAdmin": true
  },
  {
    "guildId": 14797,
    "roleIds": [
      22940
    ],
    "isAdmin": true
  },
  {
    "guildId": 15496,
    "roleIds": [
      24601
    ],
    "isAdmin": true
  }
]

// 请求网址: https://api.group.xyz/v1/group/access/9267/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
res = [
  {
    "roleId": 12043,
    "access": true,
    "requirements": [
      {
        "requirementId": 87815,
        "access": true,
        "amount": null
      }
    ]
  }
]

// 请求网址: https://group.xyz/api/ddrum?ddforward=https%3A%2F%2Frum.browser-intake-datadoghq.eu%2Fapi%2Fv2%2Frum%3Fddsource%3Dbrowser%26ddtags%3Dsdk_version%253A4.26.0%252Capi%253Afetch%252Cenv%253Aprod%252Cservice%253Aguild.xyz%252Cversion%253A1.0.0%26dd-api-key%3Dpub7cf22f3b79a010363cf58c859cfa8ad8%26dd-evp-origin-version%3D4.26.0%26dd-evp-origin%3Dbrowser%26dd-request-id%3D0800879f-c004-4bad-ad5e-e08b04725835%26batch_time%3D1670586620998
// 请求方法: POST
// 状态代码: 202 
const payload = {
  "_dd": {
    "format_version": 2,
    "drift": 1397520,
    "session": {
      "plan": 2
    },
    "document_version": 3,
    "replay_stats": {
      "records_count": 129,
      "segments_count": 1,
      "segments_total_raw_size": 256793
    }
  },
  "application": {
    "id": "996b7a2a-d610-4235-a5b4-65391973ea76"
  },
  "date": 1670586604492,
  "service": "group.xyz",
  "version": "1.0.0",
  "source": "browser",
  "session": {
    "id": "3247f776-13af-496b-8ee5-7b56e920b041",
    "type": "user",
    "has_replay": true
  },
  "view": {
    "id": "808664ab-bf74-4183-9f9e-8e8c9b126ec5",
    "url": "https://group.xyz/layer3",
    "referrer": "https://group.xyz/layer3",
    "action": {
      "count": 6
    },
    "frustration": {
      "count": 0
    },
    "cumulative_layout_shift": 0.1243,
    "error": {
      "count": 0
    },
    "is_active": true,
    "loading_type": "route_change",
    "long_task": {
      "count": 0
    },
    "resource": {
      "count": 0
    },
    "time_spent": 15191000000,
    "in_foreground_periods": [
      {
        "start": 6138800000,
        "duration": 7180200000
      }
    ]
  },
  "type": "view"
}

const res = {
  "request_id": "0800879f-c004-4bad-ad5e-e08b04725835"
}

// 请求网址: https://api.group.xyz/v1/user/join
// 请求方法: POST
// 状态代码: 200 
const payload = {
  "payload": {
    "guildId": 9267,
    "platforms": []
  },
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "6SzDNvKjmyjZqpTlLDgy05O/SYKcjqKGi/Zoqx2+P4U=",
    "ts": "1670586921397",
    "hash": "0x670adeb8abf2f76875fd5feeb65b6ab382d51cb019f87cc2bc562be0c7577c51",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "c73dfefd404b3cf774b124044ce5a4d630418d0db17da166e8ecfc54d70f64b4f2a3e9a89294a7a1f734c42020121069d89478405210a35bd43a3ca59ccfccd9"
}
cosnt res = {
  "success": true,
  "platformResults": [
    {
      "success": false,
      "platformId": 1,
      "platformName": "DISCORD",
      "errorMsg": "Unknown Member",
      "invite": "https://discord.gg/32vZmccH3f"
    }
  ]
}

// 请求网址: https://api.group.xyz/v1/group/details/akuns-server
// 请求方法: POST
// 状态代码: 200 
const payload = {
  "payload": {},
  "params": {
    "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
    "nonce": "TPwAFaEweeR/JTxM+aooAHlLcf7nfvhjZu/yzI3Yuuc=",
    "ts": "1670587353154",
    "method": 2,
    "msg": "Please sign this message"
  },
  "sig": "fbbc00d35b1d177d4b7f1b6dd613cfc2b8eed2c86c83a216cb697d096cdff600feaf75d445a93e766fa818f006d9ee95454b4e0cca2d3668c77d4ad66736ea79"
}
const res = {
  "id": 14434,
  "name": "akun's server",
  "urlName": "akuns-server",
  "description": "",
  "imageUrl": "/guildLogos/145.svg",
  "showMembers": true,
  "hideFromExplorer": false,
  "createdAt": "2022-11-07T03:45:36.802Z",
  "onboardingComplete": true,
  "admins": [
    {
      "id": 241503,
      "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "isOwner": true
    },
    {
      "id": 718135,
      "address": "0xeef29c005557a8aaa8bdf155c81d9f8dc736f5ea",
      "isOwner": false
    }
  ],
  "theme": {
    "mode": "LIGHT",
    "color": null,
    "backgroundImage": "https://group-xyz.mypinata.cloud/ipfs/QmbrDbWSsMavWHkaRPxcYyPRWXYhbz6bqPLydpcPYhjXad",
    "backgroundCss": null
  },
  "poaps": [
    {
      "id": 482,
      "fancyId": "test-2022-0305-2022",
      "activated": false,
      "expiryDate": 1667952000,
      "poapContracts": null,
      "poapIdentifier": 81741
    },
    {
      "id": 561,
      "fancyId": "23234234-2022",
      "activated": false,
      "expiryDate": 1696896000,
      "poapContracts": null,
      "poapIdentifier": 90460
    }
  ],
  "guildPlatforms": [
    {
      "id": 6257,
      "platformId": 1,
      "platformGroupId": "985740354131218472",
      "platformGroupData": {
        "invite": "https://discord.gg/theW5CqpMH",
        "inviteChannel": "1039022942744485888"
      },
      "platformGroupName": "akun's server",
      "invite": "https://discord.gg/theW5CqpMH"
    }
  ],
  "roles": [
    {
      "id": 22133,
      "name": "Member",
      "logic": "AND",
      "imageUrl": "/guildLogos/88.svg",
      "description": "123456",
      "requirements": [
        {
          "id": 115292,
          "data": null,
          "name": "-",
          "type": "FREE",
          "chain": "ETHEREUM",
          "roleId": 22133,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 10285,
          "platformRoleId": "1039022788838707211",
          "guildPlatformId": 6257,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    },
    {
      "id": 24256,
      "name": "fff",
      "logic": "AND",
      "imageUrl": "https://group-xyz.mypinata.cloud/ipfs/QmT7ZyELmMHyHF76CYitbUof6yEWqseEoPqz1rKadaS5PK",
      "description": "sdfsdf",
      "requirements": [
        {
          "id": 126861,
          "data": {},
          "name": "-",
          "type": "FREE",
          "chain": "ETHEREUM",
          "roleId": 24256,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 12346,
          "platformRoleId": "1048590676524273685",
          "guildPlatformId": 6257,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    },
    {
      "id": 24465,
      "name": "23234234",
      "logic": "AND",
      "imageUrl": "/guildLogos/25.svg",
      "description": "567567567",
      "requirements": [
        {
          "id": 127950,
          "data": {
            "addresses": [
              "0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1",
              "0xB7d484D6Fc1c81597E4990838792bd42DDD182c9",
              "0x13E607774298D92f58F223e84Dbcf6897bbF91A3"
            ],
            "hideAllowlist": true
          },
          "name": "-",
          "type": "ALLOWLIST",
          "chain": "ETHEREUM",
          "roleId": 24465,
          "symbol": "-",
          "address": null
        }
      ],
      "rolePlatforms": [
        {
          "id": 12553,
          "platformRoleId": "1049705205773652099",
          "guildPlatformId": 6257,
          "platformRoleData": null
        }
      ],
      "memberCount": 1
    }
  ],
  "memberCount": 1
}

// 请求网址: https://api.group.xyz/v1/discord/server/884514862737281024
// 请求方法: POST
// 状态代码: 200 
const res ={
  "serverIcon": "https://cdn.discordapp.com/icons/884514862737281024/a_b4cb6297711ecd22281ca5880ce4b0b4.png",
  "serverName": "Layer3",
  "serverId": "884514862737281024",
  "categories": [
    {
      "id": "-",
      "name": "-",
      "channels": [
        {
          "id": "1041900093193846804",
          "name": "ticket-2809-⭐",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049399981485854812",
          "name": "ticket-3661-⭐",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1009733119663931422",
          "name": "important-ticket-log",
          "roles": []
        },
        {
          "id": "1050150790347624488",
          "name": "ticket-3841",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050529515891331072",
          "name": "ticket-3900",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049912322270965830",
          "name": "ticket-3784",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049961536241811487",
          "name": "ticket-3792",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1038091592571043910",
          "name": "collabland-bot",
          "roles": []
        },
        {
          "id": "1030126483810811934",
          "name": "ticket-1716-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1036835766845853696",
          "name": "ticket-2269-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050518670809501809",
          "name": "ticket-3895",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049043593983164527",
          "name": "ticket-3583-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050393009302274149",
          "name": "closed-3870",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049781044821364746",
          "name": "ticket-3768-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050448437340033024",
          "name": "ticket-3886",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049747257660035202",
          "name": "ticket-3764",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048950034961543169",
          "name": "ticket-3557",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050615648725766204",
          "name": "closed-3906",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "884514863425142836",
          "name": "👋┃newcomers",
          "roles": []
        },
        {
          "id": "1050437643344822282",
          "name": "ticket-3880",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050439795425431702",
          "name": "ticket-3881",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "999998548097634304",
          "name": "boomerang-setup",
          "roles": []
        },
        {
          "id": "1049285299936702504",
          "name": "ticket-3612",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049562673114787860",
          "name": "ticket-3704",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049362291851796500",
          "name": "ticket-3630",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049119617865498684",
          "name": "ticket-3592",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050080717008289863",
          "name": "ticket-3813",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050118116086132767",
          "name": "ticket-3839",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "974848422433656862",
          "name": "dune-analytics-treaqura",
          "roles": []
        },
        {
          "id": "1050527244600229928",
          "name": "ticket-3899",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050257111814250546",
          "name": "ticket-3854",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049724784142995548",
          "name": "ticket-3753",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050096185790177332",
          "name": "ticket-3831",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050504452550959144",
          "name": "ticket-3894",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049980434953551893",
          "name": "ticket-3795",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050520563770195998",
          "name": "ticket-3896",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050655941139374090",
          "name": "ticket-3912",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049596718762696764",
          "name": "ticket-3717",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049754249866776606",
          "name": "ticket-3765",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1045321471813226546",
          "name": "ticket-3132-🔴",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1039794940982591519",
          "name": "ticket-2596-⭐",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050632294899130420",
          "name": "ticket-3909-🔴",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1045673182918627378",
          "name": "ticket-3171-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049999476338724937",
          "name": "ticket-3797-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1043738589080584222",
          "name": "ticket-2973-🟡",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049288213455450152",
          "name": "ticket-3614",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050179964126904392",
          "name": "ticket-3844",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050592676589879376",
          "name": "ticket-3903",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048509819952308244",
          "name": "ticket-3506-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048960866562875474",
          "name": "ticket-3559",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1042179397324648519",
          "name": "ticket-2828-🟡",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050252979988607006",
          "name": "ticket-3853-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050322822297309224",
          "name": "ticket-3857",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049993923331899392",
          "name": "ticket-3796",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1007427671040458833",
          "name": "layer3-bat_wayne",
          "roles": []
        },
        {
          "id": "983483938376912948",
          "name": "layer3-felwintrr",
          "roles": []
        },
        {
          "id": "1048120577904353331",
          "name": "ticket-3438-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050223290863591514",
          "name": "ticket-3851-⭐",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050360229205913620",
          "name": "closed-3862",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1044174043030487080",
          "name": "ticket-3014-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048859961058410567",
          "name": "ticket-3543-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1047749143830994944",
          "name": "ticket-3376-🟢",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "983032539470000149",
          "name": "layer3-shini",
          "roles": []
        },
        {
          "id": "1050467666583896114",
          "name": "ticket-3890",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049661872862920744",
          "name": "ticket-3737",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1047452165364523038",
          "name": "ticket-3340",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050007408082178079",
          "name": "ticket-3801-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048756868052430918",
          "name": "ticket-3535-🟡",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050434794821009479",
          "name": "ticket-3877",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "999998550077349908",
          "name": "boomerang-join",
          "roles": []
        },
        {
          "id": "1044602368135798876",
          "name": "ticket-3064-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050620015847669761",
          "name": "closed-3907",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049500494722244679",
          "name": "ticket-3695",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049044104438358016",
          "name": "ticket-3584-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050129984544571493",
          "name": "ticket-3840",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "997840231967510608",
          "name": "dune-analytics-bat_wayne",
          "roles": []
        },
        {
          "id": "1049766696249995334",
          "name": "ticket-3767",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049387047246377041",
          "name": "ticket-3644-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049954118619373588",
          "name": "ticket-3789-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1045526511073103962",
          "name": "ticket-3160-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1023522208053723186",
          "name": "ticket-1281-🟢",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048322932700610560",
          "name": "ticket-3464-🔵",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1038394054569906186",
          "name": "ticket-2484-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049413056649764914",
          "name": "ticket-3667",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1048966062697361449",
          "name": "ticket-3561",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1049851305033732118",
          "name": "ticket-3777-🔴",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050109162954162186",
          "name": "ticket-3835",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050444192427544607",
          "name": "ticket-3883",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050221717538209843",
          "name": "ticket-3850-⭐",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "971400836863393793",
          "name": "layer3-a37",
          "roles": []
        },
        {
          "id": "1048400866132111430",
          "name": "ticket-3479-🟠",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050605497193078854",
          "name": "ticket-3905",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050730524613816330",
          "name": "ticket-3915",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050737798128353300",
          "name": "ticket-3916",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050738355060625458",
          "name": "ticket-3917",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050738605603164181",
          "name": "ticket-3918",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050738787208147004",
          "name": "ticket-3919",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050739031547322418",
          "name": "ticket-3920",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050740212218736710",
          "name": "ticket-3921",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050740306041122829",
          "name": "ticket-3922",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1050741303287234700",
          "name": "ticket-3923",
          "roles": [
            "884528932702011402"
          ]
        }
      ]
    },
    {
      "id": "948863328661409852",
      "name": "Represent 🌍",
      "channels": [
        {
          "id": "967113969775050844",
          "name": "🇷🇺┃russia",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1009811260436643903",
            "1019260162525122591"
          ]
        },
        {
          "id": "986639589152292944",
          "name": "🇯🇵┃日本語",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1009811260436643903",
            "1019260457468571728"
          ]
        },
        {
          "id": "1015748341319081985",
          "name": "🇵🇱┃polish",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1009811260436643903",
            "1019260544890454087"
          ]
        },
        {
          "id": "1017406970543755336",
          "name": "🇻🇳┃việt",
          "roles": [
            "954674771830927371",
            "1019260668018442290",
            "887289781707501618",
            "1009811260436643903"
          ]
        },
        {
          "id": "1020337149083652236",
          "name": "🇫🇷┃français",
          "roles": [
            "1020337320324513942",
            "1009811260436643903"
          ]
        },
        {
          "id": "1017123960900550767",
          "name": "🇺🇦┃ukrainian",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1019260577555677224",
            "1009811260436643903"
          ]
        },
        {
          "id": "972849465725698108",
          "name": "🇮🇷┃farsi",
          "roles": [
            "954674771830927371",
            "1019260215952150658",
            "887289781707501618",
            "1009811260436643903"
          ]
        },
        {
          "id": "954196611364442143",
          "name": "🇪🇸┃español",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1009811260436643903",
            "1019260334848086076"
          ]
        },
        {
          "id": "1017436655415730236",
          "name": "🇹🇷┃türkçe",
          "roles": [
            "954674771830927371",
            "1019260711014244425",
            "887289781707501618",
            "1009811260436643903"
          ]
        },
        {
          "id": "948864301425377290",
          "name": "🇮🇳┃india",
          "roles": [
            "954674771830927371",
            "887289781707501618",
            "1019260302518386759",
            "1009811260436643903"
          ]
        },
        {
          "id": "1028010105347772497",
          "name": "🇵🇹┃portuguese",
          "roles": [
            "1028010553324617858",
            "1009811260436643903"
          ]
        },
        {
          "id": "948863503823945728",
          "name": "🇨🇳┃中文",
          "roles": [
            "954674771830927371",
            "1019260188395581491",
            "887289781707501618",
            "1009811260436643903"
          ]
        },
        {
          "id": "1017435892547342397",
          "name": "🇰🇷┃한국어",
          "roles": [
            "954674771830927371",
            "1019261391489749056",
            "887289781707501618",
            "1009811260436643903"
          ]
        },
        {
          "id": "1004754738375569418",
          "name": "🇹🇭┃thai",
          "roles": [
            "954674771830927371",
            "1019260497712926770",
            "887289781707501618",
            "1009811260436643903"
          ]
        }
      ]
    },
    {
      "id": "884527726801858601",
      "name": "Mission Control",
      "channels": [
        {
          "id": "935409971909263370",
          "name": "💸┃bounty-board",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "1019292232022495304",
          "name": "🗞┃publications",
          "roles": []
        },
        {
          "id": "886742656447819837",
          "name": "📬┃announcements",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "991690470898020402",
          "name": "🧭┃navigation",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "943104379597250590",
          "name": "🐦┃tweets",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "1013897718374092820",
          "name": "📝┃bounty-instructions",
          "roles": [
            "884514862737281024"
          ]
        }
      ]
    },
    {
      "id": "918855241754112000",
      "name": "Community wizards",
      "channels": [
        {
          "id": "970548551321022504",
          "name": "bug-report",
          "roles": []
        },
        {
          "id": "916308855745544192",
          "name": "mod-log",
          "roles": []
        },
        {
          "id": "964536206299902032",
          "name": "🥂┃builders-lounge",
          "roles": [
            "968231253734207518"
          ]
        },
        {
          "id": "967432434721370172",
          "name": "🙇┃mods",
          "roles": [
            "954674771830927371"
          ]
        },
        {
          "id": "884532912156999681",
          "name": "moderator-only",
          "roles": []
        },
        {
          "id": "991702036951351376",
          "name": "support-ticket-log",
          "roles": []
        }
      ]
    },
    {
      "id": "910187982986833950",
      "name": "Archive",
      "channels": [
        {
          "id": "969279092107141241",
          "name": "layer3-talktothelocals",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "971104976447365220",
          "name": "🎩┃dune-wizards",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "906318021205844028",
          "name": "🐋phuture-finance",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "905231658389749760",
          "name": "🌾beanstalk",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "957966269628424212",
          "name": "🕵┃bounties",
          "roles": []
        },
        {
          "id": "918205006400880641",
          "name": "🦸┃superteam",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "968786506414161950",
          "name": "layer3-t4t5",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "912420914090606623",
          "name": "🏦┃notional",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "962225559612588082",
          "name": "bancor-dariya-•-l3-🚀",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "921400558542274650",
          "name": "🔧┃workshop",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "909153122285391912",
          "name": "🟣enter-dao",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "884529942732374040",
          "name": "🚦start-here",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "904441539487481857",
          "name": "🎥glass",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "917077161863680090",
          "name": "🎨┃art-and-design",
          "roles": [
            "953244409628069921",
            "892433425447931905"
          ]
        },
        {
          "id": "893512635994234910",
          "name": "💲alphr",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "1018902176204062911",
          "name": "🔎┃proof-of-discovery",
          "roles": []
        },
        {
          "id": "955783369990483978",
          "name": "🤓┃new-to-web3",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "905228551035453491",
          "name": "🕹decentral-games",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "969605393141608478",
          "name": "layer3-thecontributoooor",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "950400418356277282",
          "name": "feedback-private",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "887291156273184778",
          "name": "📜rules",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "910194950782746684",
          "name": "🗿de-gods",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "884527864148529182",
          "name": "🎯┃suggest-quests",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "895088026777169920",
          "name": "🙋┃open-roles",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "887112249427259402",
          "name": "🏦bancor",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "969295010476458045",
          "name": "🛠┃project-winners",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "904368271728476170",
          "name": "🌐┃gcr",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "902277914819694714",
          "name": "🐶┃doge-nft",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "902576539986710568",
          "name": "🏛┃olympus",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "962218553598173214",
          "name": "bancor-dariya-•-l3-🚀",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "995066815300505731",
          "name": "crm-test",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "928296592807723009",
          "name": "🏆┃contest-winners",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "963124009686433832",
          "name": "layer3-yahya--l3",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "893511526026211398",
          "name": "🏎rari-capital",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "905434138012696587",
          "name": "📅┃events",
          "roles": []
        },
        {
          "id": "893520305211965441",
          "name": "🦉┃index-coop",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "903006932204404827",
          "name": "🚜harvest-finance",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "901923909551673344",
          "name": "🦊shapeshift",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "897808148960804884",
          "name": "🌊pool-together",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "970321960179494943",
          "name": "📅┃5-days-of-l3",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "915652169074630676",
          "name": "✈┃harmony-one",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "917077314137882684",
          "name": "💻┃engineering",
          "roles": [
            "892433425447931905"
          ]
        },
        {
          "id": "968212884721123409",
          "name": "layer3-brandon--l3",
          "roles": [
            "884528932702011402"
          ]
        },
        {
          "id": "917076990119538688",
          "name": "📊┃analytics",
          "roles": [
            "892433425447931905"
          ]
        },
        {
          "id": "963177740390002728",
          "name": "layer3-thecontributoooor",
          "roles": [
            "884528932702011402"
          ]
        }
      ]
    },
    {
      "id": "1019293436295270440",
      "name": "Help and support",
      "channels": [
        {
          "id": "887675810318790657",
          "name": "♻┃feedback",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "928652998941487105",
          "name": "🚨┃scam-alert",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "884531986759303199",
          "name": "🆘┃help",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "991708515154526358",
          "name": "📩┃support-ticket",
          "roles": [
            "887289781707501618"
          ]
        }
      ]
    },
    {
      "id": "903573188573814806",
      "name": "Contributor Cohorts",
      "channels": [
        {
          "id": "905429450227867728",
          "name": "🚧┃work-submissions",
          "roles": []
        },
        {
          "id": "967808910696267837",
          "name": "🎓┃cohort-1",
          "roles": []
        },
        {
          "id": "903574996411097100",
          "name": "🔭┃chat",
          "roles": []
        }
      ]
    },
    {
      "id": "955784514062385162",
      "name": "Community",
      "channels": [
        {
          "id": "908731585468186695",
          "name": "🤝┃partnerships",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "1017848084723671040",
          "name": "explore-pass-testers",
          "roles": []
        },
        {
          "id": "887306949690015754",
          "name": "🤙┃general",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "893431988340523008",
          "name": "☕┃gm",
          "roles": [
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "917077792032698369",
          "name": "✍┃content",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "1019250571804414054",
          "name": "🐕┃pet-photos",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "1009818434625875978",
          "name": "🚀┃ambassadors",
          "roles": [
            "1009811260436643903"
          ]
        },
        {
          "id": "884530100442374174",
          "name": "🐸┃memes",
          "roles": [
            "953244409628069921",
            "884528932702011402",
            "887289781707501618"
          ]
        },
        {
          "id": "921280424800555059",
          "name": "🤖┃xp-bot",
          "roles": [
            "887289781707501618"
          ]
        },
        {
          "id": "956905931201978439",
          "name": "🤩┃ama",
          "roles": [
            "887289781707501618"
          ]
        }
      ]
    },
    {
      "id": "927682083801948211",
      "name": "Start here👇",
      "channels": [
        {
          "id": "1038091596920524830",
          "name": "2┃get-roles-🙋",
          "roles": []
        },
        {
          "id": "913706133649260625",
          "name": "5┃intros👋",
          "roles": [
            "887289781707501618",
            "884514862737281024",
            "892433425447931905"
          ]
        },
        {
          "id": "991662011614244905",
          "name": "4┃faq❓",
          "roles": []
        },
        {
          "id": "927682248428367942",
          "name": "3┃about-us💡",
          "roles": [
            "884514862737281024"
          ]
        },
        {
          "id": "927682600724738098",
          "name": "1┃verify-here-✅",
          "roles": [
            "884514862737281024"
          ]
        }
      ]
    }
  ],
  "roles": [
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": "🌐",
      "id": "954674771830927371",
      "name": "Moderator",
      "color": 1752220,
      "hoist": true,
      "rawPosition": 53,
      "permissions": "2027227827265",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1647682602604
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019261391489749056",
      "name": "KR",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 8,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081252692
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "995061129627840516",
      "name": "Appy Pie Connect",
      "color": 0,
      "hoist": false,
      "rawPosition": 28,
      "permissions": "2146958591",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "631032608641384461"
      },
      "createdTimestamp": 1657311460645
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "958730830560235523",
      "name": "World Time",
      "color": 0,
      "hoist": false,
      "rawPosition": 39,
      "permissions": "16384",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "447266583459528715"
      },
      "createdTimestamp": 1648649642363
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "984099844698308672",
      "name": "Apollo",
      "color": 0,
      "hoist": false,
      "rawPosition": 30,
      "permissions": "36776045648",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "475744554910351370"
      },
      "createdTimestamp": 1654698086667
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "991701415024144458",
      "name": "Ticket Tool",
      "color": 0,
      "hoist": false,
      "rawPosition": 29,
      "permissions": "2416045072",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "557628352828014614"
      },
      "createdTimestamp": 1656510442263
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260668018442290",
      "name": "VN",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 10,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081080203
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1049753062870372354",
      "name": "new role",
      "color": 0,
      "hoist": false,
      "rawPosition": 1,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670351033657
    },
    {
      "group": "884514862737281024",
      "icon": "d826f396f8b330d68784075fae68cdbb",
      "unicodeEmoji": null,
      "id": "1038089665670037567",
      "name": "NFT Collector",
      "color": 13236354,
      "hoist": true,
      "rawPosition": 48,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1667570263069
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260215952150658",
      "name": "IR",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 17,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663080972422
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1028010553324617858",
      "name": "PT",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 5,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1665167215425
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1036684997739958294",
      "name": "Translator",
      "color": 0,
      "hoist": false,
      "rawPosition": 4,
      "permissions": "535263833152",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "360081866461806595"
      },
      "createdTimestamp": 1667235364137
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": "🛠️",
      "id": "968231253734207518",
      "name": "Builders",
      "color": 12228095,
      "hoist": false,
      "rawPosition": 54,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1650914719757
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "953244409628069921",
      "name": "tip.cc",
      "color": 0,
      "hoist": false,
      "rawPosition": 40,
      "permissions": "344128",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "617037497574359050"
      },
      "createdTimestamp": 1647341577680
    },
    {
      "group": "884514862737281024",
      "icon": "7bb395166020077463a52dc9e4c3a332",
      "unicodeEmoji": null,
      "id": "884528932702011402",
      "name": "Layer3 Team",
      "color": 15844367,
      "hoist": true,
      "rawPosition": 60,
      "permissions": "541031657215",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1630958531309
    },
    {
      "group": "884514862737281024",
      "icon": "261c605300b7f63a2b24905875462571",
      "unicodeEmoji": null,
      "id": "1049752387830685706",
      "name": "Level 20+",
      "color": 2998461,
      "hoist": false,
      "rawPosition": 51,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670350872715
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1009209655068741635",
      "name": "Invite Tracker",
      "color": 0,
      "hoist": false,
      "rawPosition": 25,
      "permissions": "268822624",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "720351927581278219"
      },
      "createdTimestamp": 1660684731977
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "999998544083689566",
      "name": "Boomerang",
      "color": 0,
      "hoist": false,
      "rawPosition": 26,
      "permissions": "1644909034609",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "912606780922667039"
      },
      "createdTimestamp": 1658488631984
    },
    {
      "group": "884514862737281024",
      "icon": "0eb8818ed0df42634046cff36a1ad4fc",
      "unicodeEmoji": null,
      "id": "1038090629462364210",
      "name": "Turbocharged",
      "color": 4954497,
      "hoist": true,
      "rawPosition": 46,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1667570492855
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "998915995857076285",
      "name": "Group Member",
      "color": 0,
      "hoist": true,
      "rawPosition": 27,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1658230532374
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "921063270020964373",
      "name": "MEE6",
      "color": 0,
      "hoist": false,
      "rawPosition": 61,
      "permissions": "1945496671",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "159985870458322944"
      },
      "createdTimestamp": 1639668996101
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019678184289091709",
      "name": "Sapphire",
      "color": 0,
      "hoist": false,
      "rawPosition": 7,
      "permissions": "1945627735",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "678344927997853742"
      },
      "createdTimestamp": 1663180623839
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260497712926770",
      "name": "TH",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 13,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081039599
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "975091951630188568",
      "name": "Zapier",
      "color": 0,
      "hoist": false,
      "rawPosition": 31,
      "permissions": "2146958591",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "368105370532577280"
      },
      "createdTimestamp": 1652550437601
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260711014244425",
      "name": "TR",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 9,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081090454
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1009589701663002657",
      "name": "MemberList",
      "color": 0,
      "hoist": false,
      "rawPosition": 24,
      "permissions": "52228",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "674885857458651161"
      },
      "createdTimestamp": 1660775342146
    },
    {
      "group": "884514862737281024",
      "icon": "1960f209913f63ab802b9110a9e18cb1",
      "unicodeEmoji": null,
      "id": "1049752817411309689",
      "name": "Level 30+",
      "color": 14516007,
      "hoist": false,
      "rawPosition": 52,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670350975135
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "930760428169101313",
      "name": "Bounty Bot",
      "color": 0,
      "hoist": false,
      "rawPosition": 59,
      "permissions": "1945496671",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "930753406438604810"
      },
      "createdTimestamp": 1641980978768
    },
    {
      "group": "884514862737281024",
      "icon": "31b940e49c21ecd23d9b8e44abe6ed67",
      "unicodeEmoji": null,
      "id": "1049754869558411285",
      "name": "Level 5+",
      "color": 12475350,
      "hoist": false,
      "rawPosition": 49,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670351464405
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1020337320324513942",
      "name": "FR",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 6,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663337774116
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "998915931231223813",
      "name": "Group.xyz bot",
      "color": 0,
      "hoist": false,
      "rawPosition": 56,
      "permissions": "8",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "868172385000509460"
      },
      "createdTimestamp": 1658230516966
    },
    {
      "group": "884514862737281024",
      "icon": "0b65362acc89fec013216b56bfe61138",
      "unicodeEmoji": null,
      "id": "1018927070870519838",
      "name": "Group Supporter",
      "color": 6323595,
      "hoist": true,
      "rawPosition": 21,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663001544445
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260188395581491",
      "name": "CN",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 18,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663080965852
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "887289781707501618",
      "name": "Bounty Hunters",
      "color": 3066993,
      "hoist": true,
      "rawPosition": 42,
      "permissions": "831180098625",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1631616769006
    },
    {
      "group": "884514862737281024",
      "icon": "9015ef3f5c1813e1e15b81bcb0aad2fc",
      "unicodeEmoji": null,
      "id": "1038090070323904552",
      "name": "Refuel",
      "color": 9276664,
      "hoist": true,
      "rawPosition": 45,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1667570359546
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "887299311950503958",
      "name": "Dyno",
      "color": 0,
      "hoist": false,
      "rawPosition": 62,
      "permissions": "268774416",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "155149108183695360"
      },
      "createdTimestamp": 1631619041193
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "936556498694340659",
      "name": "Monika",
      "color": 0,
      "hoist": false,
      "rawPosition": 32,
      "permissions": "8",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "340476335279570945"
      },
      "createdTimestamp": 1643362869667
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260302518386759",
      "name": "IN",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 16,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663080993061
    },
    {
      "group": "884514862737281024",
      "icon": "7fcaf99fe0c3ef56f66c57e625209aca",
      "unicodeEmoji": null,
      "id": "1038091022649008210",
      "name": "Powered by ETH",
      "color": 14635248,
      "hoist": true,
      "rawPosition": 47,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1667570586598
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "928861413169070103",
      "name": "DiscordServers.com",
      "color": 0,
      "hoist": false,
      "rawPosition": 34,
      "permissions": "1",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "115385224119975941"
      },
      "createdTimestamp": 1641528218310
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "900837360298758227",
      "name": "Server Booster",
      "color": 65531,
      "hoist": false,
      "rawPosition": 43,
      "permissions": "549755813889",
      "managed": true,
      "mentionable": false,
      "tags": {
        "premiumSubscriberRole": true
      },
      "createdTimestamp": 1634846763444
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260577555677224",
      "name": "UA",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 11,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081058635
    },
    {
      "group": "884514862737281024",
      "icon": "e7283dbb401e26ed900b6b644f5b8e76",
      "unicodeEmoji": null,
      "id": "1009811260436643903",
      "name": "Ambassador",
      "color": 4150783,
      "hoist": true,
      "rawPosition": 55,
      "permissions": "2187282869317",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1660828165874
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260457468571728",
      "name": "JP",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 14,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081030004
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1011315908213473343",
      "name": "GarticBOT",
      "color": 0,
      "hoist": false,
      "rawPosition": 22,
      "permissions": "256064",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "487328045275938828"
      },
      "createdTimestamp": 1661186901859
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260334848086076",
      "name": "ES",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 15,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081000769
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019252618129833984",
      "name": "Community Wizard",
      "color": 15158332,
      "hoist": false,
      "rawPosition": 20,
      "permissions": "549755888728",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663079160960
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "918976254286376994",
      "name": "Discobot",
      "color": 0,
      "hoist": false,
      "rawPosition": 35,
      "permissions": "805694528",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "633565743103082527"
      },
      "createdTimestamp": 1639171412775
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "930846363732820030",
      "name": "sesh",
      "color": 0,
      "hoist": false,
      "rawPosition": 33,
      "permissions": "397620538432",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "616754792965865495"
      },
      "createdTimestamp": 1642001467403
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1045386726367043648",
      "name": "Layer3 Role Bot",
      "color": 0,
      "hoist": false,
      "rawPosition": 58,
      "permissions": "268435456",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "1044886122590912522"
      },
      "createdTimestamp": 1669310017912
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "974023890886070356",
      "name": "carl-bot",
      "color": 0,
      "hoist": false,
      "rawPosition": 38,
      "permissions": "66321471",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "235148962103951360"
      },
      "createdTimestamp": 1652295792076
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "898691930903441490",
      "name": "gmBOT",
      "color": 0,
      "hoist": false,
      "rawPosition": 36,
      "permissions": "380104821760",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "898671533730459698"
      },
      "createdTimestamp": 1634335253216
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260162525122591",
      "name": "RU",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 19,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663080959684
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1049750773292093541",
      "name": "new role",
      "color": 0,
      "hoist": false,
      "rawPosition": 2,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670350487779
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1019260544890454087",
      "name": "PL",
      "color": 6323594,
      "hoist": false,
      "rawPosition": 12,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1663081050847
    },
    {
      "group": "884514862737281024",
      "icon": "1eec2b97799d319d91322dde12317616",
      "unicodeEmoji": null,
      "id": "1049750786764193812",
      "name": "Level 10+",
      "color": 12082711,
      "hoist": false,
      "rawPosition": 50,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1670350490991
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "892433425447931905",
      "name": "TweetShift",
      "color": 0,
      "hoist": false,
      "rawPosition": 37,
      "permissions": "537160768",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "713026372142104687"
      },
      "createdTimestamp": 1632843109238
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "959149631797874761",
      "name": "Layer3",
      "color": 0,
      "hoist": false,
      "rawPosition": 41,
      "permissions": "268446744",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "942680509056692245"
      },
      "createdTimestamp": 1648749492359
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": null,
      "id": "1011226305368440924",
      "name": "ServerStats",
      "color": 0,
      "hoist": false,
      "rawPosition": 23,
      "permissions": "1100816",
      "managed": true,
      "mentionable": false,
      "tags": {
        "botId": "458276816071950337"
      },
      "createdTimestamp": 1661165538876
    },
    {
      "group": "884514862737281024",
      "icon": null,
      "unicodeEmoji": "💫",
      "id": "973402961424314449",
      "name": "Superstar",
      "color": 16711680,
      "hoist": false,
      "rawPosition": 44,
      "permissions": "549755880512",
      "managed": false,
      "mentionable": false,
      "tags": {},
      "createdTimestamp": 1652147750956
    }
  ],
  "isAdmin": true,
  "membersWithoutRole": 0,
  "channels": [
    {
      "id": "1019292232022495304",
      "name": "🗞┃publications"
    },
    {
      "id": "884514863425142836",
      "name": "👋┃newcomers"
    },
    {
      "id": "1038091596920524830",
      "name": "2┃get-roles-🙋"
    },
    {
      "id": "913706133649260625",
      "name": "5┃intros👋"
    },
    {
      "id": "1018902176204062911",
      "name": "🔎┃proof-of-discovery"
    },
    {
      "id": "991662011614244905",
      "name": "4┃faq❓"
    },
    {
      "id": "1011226511237447690",
      "name": "Bounty Hunters: 63757"
    },
    {
      "id": "927682248428367942",
      "name": "3┃about-us💡"
    },
    {
      "id": "927682600724738098",
      "name": "1┃verify-here-✅"
    },
    {
      "id": "1013897718374092820",
      "name": "📝┃bounty-instructions"
    }
  ]
}