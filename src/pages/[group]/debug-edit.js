// 请求网址: http://localhost:1337/parse/functions/groupAccess
// 请求方法: POST
// 状态代码: 200 OK
export const a = () => {
    const rest =
    {
        "id": 220,
        "account": "0x4fE489AdB65E27F04CeDd3Fd0b84707Bc4D93b75"
    }

    { "result": [{ "roleId": 1904, "access": true, "requirements": [{ "requirementId": 5363, "access": true, "amount": 2 }] }, { "roleId": 13256, "access": true, "requirements": [{ "requirementId": 121765, "access": true, "amount": 1 }] }, { "roleId": 1899, "access": false, "requirements": [{ "requirementId": 121851, "access": false, "amount": 0 }] }, { "roleId": 1900, "access": false, "requirements": [{ "requirementId": 121798, "access": false, "amount": 0 }] }, { "roleId": 1937, "access": false, "requirements": [{ "requirementId": 121790, "access": false, "amount": 0 }, { "requirementId": 121792, "access": false, "amount": 0 }, { "requirementId": 121789, "access": false, "amount": 0 }, { "requirementId": 121783, "access": false, "amount": 0 }, { "requirementId": 121784, "access": false, "amount": 0 }, { "requirementId": 121785, "access": false, "amount": 0 }, { "requirementId": 121787, "access": false, "amount": 0 }, { "requirementId": 121786, "access": false, "amount": 0 }, { "requirementId": 121797, "access": false, "amount": 0 }, { "requirementId": 121794, "access": false, "amount": 0 }, { "requirementId": 121782, "access": false, "amount": 0 }, { "requirementId": 121795, "access": false, "amount": 0 }, { "requirementId": 121796, "access": false, "amount": 0 }, { "requirementId": 121793, "access": false, "amount": 0 }, { "requirementId": 121791, "access": false, "amount": 0 }, { "requirementId": 121788, "access": false, "amount": 0 }] }, { "roleId": 13257, "access": false, "requirements": [{ "requirementId": 121737, "access": false, "amount": 0 }, { "requirementId": 121739, "access": true, "amount": 2 }, { "requirementId": 121738, "access": false, "amount": 0 }] }, { "roleId": 22842, "access": false, "requirements": [{ "requirementId": 121803, "access": false, "amount": 0 }, { "requirementId": 121804, "access": false, "amount": 0 }] }, { "roleId": 23182, "access": false, "requirements": [{ "requirementId": 121304, "access": true, "amount": 0 }, { "requirementId": 121305, "access": false, "amount": 0 }, { "requirementId": 121306, "access": false, "amount": 0 }] }] }

    // 请求网址: https://api.group.xyz/v1/group/220
    // 请求方法: PATCH
    // 状态代码: 401 
    const boddy =
    {
        "payload": {
            "name": "test 2022-03054545",
            "imageUrl": "https://ipfs.fleek.co/ipfs/bafkreih6axvq3xplvxutsz6fi2npoyluqjayohmklns57a34bfhyehhx7u",
            "description": "345345",
            "theme": {
                "color": "#9d0d0d",
                "mode": "LIGHT"
            },
            "showMembers": true,
            "admins": [
                "0x4fE489AdB65E27F04CeDd3Fd0b84707Bc4D93b75"
            ],
            "urlName": "d0298d2a545-01d9-49c5-b14c-084ec6612256",
            "hideFromExplorer": false,
            "guildPlatforms": [
                {
                    "platformName": "GITHUB",
                    "platformGroupId": "985740354131218472",
                    "id": 6257,
                    "platformId": 1,
                    "platformGroupData": {
                        "invite": "https://discord.gg/theW5CqpMH",
                        "inviteChannel": "1039022942744485888"
                    },
                    "platformGroupName": "akun's server",
                    "invite": "https://discord.gg/theW5CqpMH"
                }
            ]
        },
        "params": {
            "addr": "0x4fe489adb65e27f04cedd3fd0b84707bc4d93b75",
            "nonce": "nRTiskZFOHlbnD6RMRt3QJr/DurDgA3J0DCdvEXn7y4=",
            "ts": "1670034271787",
            "hash": "0x5fe52143c19edbd808eafdfd178667d2d5b404350f919b8f5df138f9af8b57a1",
            "method": 2,
            "msg": "Please sign this message"
        },
        "sig": "a56297c3a17cf5c857d5324e8bfcecf81223551265ce1ad0acd988e0504818979decae30f0488dc5586d1bac88feb4c14b9b067e728e0473c3c7656e1ac0cae4"
    }

    { "message": "Hash is not valid!" }
}

//=== beforSave Groups
const request = {
    "triggerName": "beforeSave",
    "object": {
        "name": "test 2022-0305", "imageUrl": "https://ipfs.fleek.co/ipfs/bafkreih6axvq3xplvxutsz6fi2npoyluqjayohmklns57a34bfhyehhx7u", "description": "23234234234234",
        "theme": { "color": "#3f0d0d", "mode": "LIGHT" },
        "showMembers": true,
        "admins": ["0x4fE489AdB65E27F04CeDd3Fd0b84707Bc4D93b75"],
        "urlName": "d0298d2a-01d9-49c5-b14c-084ec6612256",
        "hideFromExplorer": false,
        "guildPlatforms": [{
            "platformName": "GITHUB",
            "platformGroupId": "985740354131218472",
            "id": 6257, "platformId": 1,
            "platformGroupData": {
                "invite": "https://discord.gg/theW5CqpMH",
                "inviteChannel": "1039022942744485888"
            },
            "platformGroupName": "akun's server",
            "invite": "https://discord.gg/theW5CqpMH"
        }]
    },
    "master": false,
    "log": {
        "options": { "jsonLogs": false, "logsFolder": "./logs", "verbose": false }, "appId": "nft-web2"
    },
    "headers": {
        "host": "localhost:1337",
        "connection": "keep-alive",
        "content-length": "635",
        "sec-ch-ua": "\"Google Chrome\";v=\"107\", \"Chromium\";v=\"107\", \"Not=A?Brand\";v=\"24\"",
        "content-type": "application/json",
        "dnt": "1",
        "x-parse-application-id": "nft-web2", "sec-ch-ua-mobile": "?0", "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
        "sec-ch-ua-platform": "\"macOS\"", "accept": "*/*",
        "origin": "http://localhost:5001",
        "sec-fetch-site": "same-site",
        "sec-fetch-mode": "cors", "sec-fetch-dest": "empty",
        "referer": "http://localhost:5001/",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "zh-CN,zh;q=0.9"
    },
    "ip": "172.19.0.1", "context": {}
}
//error: beforeSave failed for Groups for user undefined:

const aa = {
    "name": "test 2022-0305",
    "imageUrl": "https://ipfs.fleek.co/ipfs/bafkreih6axvq3xplvxutsz6fi2npoyluqjayohmklns57a34bfhyehhx7u", "description": "",
    "theme": { "color": "#004d4d" },
    "showMembers": false,
    "admins": ["0x4fE489AdB65E27F04CeDd3Fd0b84707Bc4D93b75"], "urlName": "d0298d2a-01d9-49c5-b14c-084ec6612256", "hideFromExplorer": false,
    "guildPlatforms": [{
        "platformName": "GITHUB", "platformGroupId": "985740354131218472", "id": 6257, "platformId": 1,
        "platformGroupData": {
            "invite": "https://discord.gg/theW5CqpMH", "inviteChannel": "1039022942744485888"
        },
        "platformGroupName": "akun's server",
        "invite": "https://discord.gg/theW5CqpMH"
    }]
}

// 请求网址: https://api.group.xyz/v1/user/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 

const res2 =
{
    "id": 241503,
    "addresses": 1,
    "createdAt": "2022-09-13T08:29:13.450Z",
    "platformUsers": [
        {
            "platformId": 1,
            "platformName": "DISCORD"
        },
        {
            "platformId": 3,
            "platformName": "GITHUB"
        }
    ],
    "signingKey": "041a1adc970db6e025c035a1115fa16ccb808d7b8be976fb8af89eb17a09c11527eebe2bf3ff429f3530747d91197ec9876f3275284c34c694ad29c4b950fdea76"
}
// 请求网址: https://api.group.xyz/v1/user/membership/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
const res3 =
    [
        {
            "guildId": 1985,
            "roleIds": [
                1904
            ],
            "isAdmin": false
        },
        {
            "guildId": 3474,
            "roleIds": [
                3883
            ],
            "isAdmin": false
        },
        {
            "guildId": 3718,
            "roleIds": [
                4220
            ],
            "isAdmin": false
        },
        {
            "guildId": 13468,
            "roleIds": [
                19924
            ],
            "isAdmin": false
        },
        {
            "guildId": 14434,
            "roleIds": [
                22133
            ],
            "isAdmin": true
        },
        {
            "guildId": 14797,
            "roleIds": [
                22940
            ],
            "isAdmin": true
        }
    ]

//     请求网址: https://api.group.xyz/v1/user/details/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: POST
// 状态代码: 200 
const payload2 = {
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "lILp3gwA5Xx4T5Sjadwr7GzNaGzZCVW+hif0Jxcpv9o=",
      "ts": "1670072981276",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "e72b11c1b3ca25e40f30f4f740b4f53463002a3e802c4244eaf7b9446e369d6a21d7c1d1fd11e344d48bee558d96f5e01c57ab21b16712146a1c562036e3caa9"
}
  const res4={
    "id": 241503,
    "addresses": [
      "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
    ],
    "createdAt": "2022-09-13T08:29:13.450Z",
    "signingKey": "041a1adc970db6e025c035a1115fa16ccb808d7b8be976fb8af89eb17a09c11527eebe2bf3ff429f3530747d91197ec9876f3275284c34c694ad29c4b950fdea76",
    "isSuperAdmin": false,
    "platformUsers": [
      {
        "platformId": 1,
        "platformName": "DISCORD",
        "platformUserId": "825368282814742559",
        "platformUserData": {
          "scope": "guilds guilds.members.read identify",
          "expiresIn": 1670564218079,
          "accessToken": "JRFAC1gTjiGsMZvCpgf_jXQduo6u7eRu1eylCqoEHuJisfYlc33i6YU3bLchKBhduPNFLeOJISdInMW3AS6mA2t6ZmVnRnc5WExwMWFuUmVBcmk2QlNPTUNuR1k1NA",
          "refreshToken": "3bgMjokyA-1pXw7Hv5DcxnGjOtEuRluCflBxNwWaZK3h2GWwBQFBWjKmhaDDZozSN_nYfxectXHoHa9soNN-CVRhMUdTdWNiTkhqM3JlUEdxTHlmeXplSXpKczU1Yg"
        },
        "username": "akun",
        "avatar": "https://cdn.discordapp.com/avatars/825368282814742559/adfb8381146dac73af6922104b9ebd13.png"
      },
      {
        "platformId": 3,
        "platformName": "GITHUB",
        "platformUserId": "112370890",
        "platformUserData": {
          "readonly": false,
          "reauthable": false,
          "accessToken": "9kMSKhTRopQW7eK6FUFkqyL1PwbYSZpnMviWRuiDMehSfNofTCbsU-4SVA6qHrxGCpwEUO8uTbT5K3w3N_EqCGdob19jSTdrOGExODBCTDVyVEhvMzgyVTJwa0RVNWdSdGYxMEhpaDU"
        },
        "username": "kamran-hz",
        "avatar": "https://avatars.githubusercontent.com/u/112370890?v=4"
      }
    ]
  }
//   请求网址: https://api.group.xyz/v1/role/22133
// 请求方法: GET
// 状态代码: 200 
const res5={
    "id": 22133,
    "name": "Member",
    "description": null,
    "imageUrl": "/guildLogos/88.svg",
    "createdAt": "2022-11-07T03:45:36.829Z",
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 115292,
        "type": "FREE",
        "chain": "ETHEREUM",
        "address": null,
        "data": null
      }
    ],
    "rolePlatforms": [
      {
        "guildPlatformId": 6257,
        "platformRoleId": "1039022788838707211",
        "guildPlatform": {
          "id": 6257,
          "platformGroupId": "985740354131218472",
          "platformGroupData": {
            "invite": "https://discord.gg/theW5CqpMH",
            "inviteChannel": "1039022942744485888"
          },
          "platformGroupName": "akun's server",
          "invite": "https://discord.gg/theW5CqpMH",
          "platform": {
            "id": 1,
            "name": "DISCORD"
          }
        }
      }
    ],
    "members": [
      "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
    ]
}
  
// 请求网址: https://group.xyz/api/pinata-key
// 请求方法: POST
// 状态代码: 200 
const payload3={
    "key": "35754f52936dfaca8acb"
}
//200
const res6 = { null} 

// 请求网址: https://api.group.xyz/v1/group/details/akuns-server
// 请求方法: POST
// 状态代码: 200 
const payload4={
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "6SV6iS6bZXVD0CLCMw/86pXwAwhpJwIVwdFbMQbIWpg=",
      "ts": "1670073451868",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "1f49cd6c0a638a4e11803b0634a20fcec3dc7010a81ced9916ba3ace276cac944c3301e7d39c38ebf8e9d052a2a733b786490895042c33f17c95bfe150012e86"
}
  const res7={
    "id": 14434,
    "name": "akun's server",
    "urlName": "akuns-server",
    "description": "",
    "imageUrl": "/guildLogos/145.svg",
    "showMembers": true,
    "hideFromExplorer": false,
    "createdAt": "2022-11-07T03:45:36.802+00:00",
    "onboardingComplete": true,
    "admins": [
      {
        "id": 241503,
        "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
        "isOwner": true
      }
    ],
    "theme": {
      "mode": "DARK",
      "color": null,
      "backgroundImage": "https://group-xyz.mypinata.cloud/ipfs/QmbrDbWSsMavWHkaRPxcYyPRWXYhbz6bqPLydpcPYhjXad",
      "backgroundCss": null
    },
    "poaps": [
      {
        "id": 482,
        "fancyId": "test-2022-0305-2022",
        "activated": false,
        "expiryDate": 1667952000,
        "poapContracts": null,
        "poapIdentifier": 81741
      }
    ],
    "guildPlatforms": [
      {
        "id": 6257,
        "platformId": 1,
        "platformGroupId": "985740354131218472",
        "platformGroupData": {
          "invite": "https://discord.gg/theW5CqpMH",
          "inviteChannel": "1039022942744485888"
        },
        "platformGroupName": "akun's server",
        "invite": "https://discord.gg/theW5CqpMH"
      }
    ],
    "roles": [
      {
        "id": 22133,
        "name": "Member",
        "logic": "AND",
        "imageUrl": "/guildLogos/88.svg",
        "description": null,
        "requirements": [
          {
            "id": 115292,
            "data": null,
            "name": "-",
            "type": "FREE",
            "chain": "ETHEREUM",
            "roleId": 22133,
            "symbol": "-",
            "address": null
          }
        ],
        "rolePlatforms": [
          {
            "id": 10285,
            "platformRoleId": "1039022788838707211",
            "guildPlatformId": 6257,
            "platformRoleData": null
          }
        ],
        "memberCount": 1
      }
    ],
    "memberCount": 1
  }

//   请求网址: https://api.group.xyz/v1/user/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1/statusUpdate/14434
// 请求方法: GET
// 状态代码: 200 
const res8=[
    {
      "guildId": 14434,
      "roleIds": [
        22133,
        24256
      ],
      "platformResults": [
        {
          "platformId": 1,
          "platformName": "DISCORD",
          "success": true
        }
      ]
    }
]
// 请求网址: https://api.group.xyz/v1/user/pubKey
// 请求方法: POST
// 状态代码: 200
const payload5={
    "payload": {
      "pubKey": "04e0a1d658288056a24d286d687048027991aed22c1cfeec91b21c1feea26387fa422a11d01f3bb9573c636664f56346c3839c04f95026b32ca4d8f2f759a726c4"
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "Gd13xuPH/T+EPTbZUjJhLQxsEGLmySgyHTCG7hGywbo=",
      "ts": "1670074052786",
      "hash": "0x60bab0bb9537b00f6edb6365e280615934515e11d35b0ff2e7d6a090ce25246a",
      "method": 1,
      "msg": "Please sign this message, so we can generate, and assign you a signing key pair. This is needed so you don't have to sign every Group interaction."
    },
    "sig": "0xa21d63a4bb84b704edb6f7b3d0b3ac06029013cb3cb868233ea6209fed68e01907b57cc865b5c60450435d03da689f8ff2b4e3951ee27df28c77982f83e8056e1c"
}
const res9 = { "pubKey": "04e0a1d658288056a24d286d687048027991aed22c1cfeec91b21c1feea26387fa422a11d01f3bb9573c636664f56346c3839c04f95026b32ca4d8f2f759a726c4", "userId": 241503 }
// 请求网址: https://api.group.xyz/v1/user/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
const res10 = {
    "id": 241503,
    "addresses": 1,
    "createdAt": "2022-09-13T08:29:13.450Z",
    "platformUsers": [
      {
        "platformId": 1,
        "platformName": "DISCORD"
      },
      {
        "platformId": 3,
        "platformName": "GITHUB"
      }
    ],
    "signingKey": "04e0a1d658288056a24d286d687048027991aed22c1cfeec91b21c1feea26387fa422a11d01f3bb9573c636664f56346c3839c04f95026b32ca4d8f2f759a726c4"
  }