// 请求网址: https://api.guild.xyz/v1/util/symbol/0x1b6c5864375b34af3ff5bd2e5f40bc425b4a8d79/BSC
// 请求方法: GET
// 状态代码: 200 
// 远程地址: [2606:4700:20::ac43:4753]:443
// 引荐来源网址政策: strict-origin-when-cross-origin
res = {
    "name": "-",
    "symbol": "-",
    "errorMsg": "call revert exception [ See: https://links.ethers.org/v5-errors-CALL_EXCEPTION ] (method=\"name()\", data=\"0x\", errorArgs=null, errorName=null, errorSignature=null, reason=null, code=CALL_EXCEPTION, version=abi/5.6.4)"
}
  
// 请求网址: https://api.guild.xyz/v1/util/symbol/0x1b6c5864375b34af3ff5bd2e5f40bc425b4a8d79/ETHEREUM
// 请求方法: GET
// 状态代码: 200 
res =
{
    "name": "TopChainCoin",
    "symbol": "TOPC",
    "decimals": {
      "type": "BigNumber",
      "hex": "0x06"
    }
}
// 请求网址: https://api.guild.xyz/v1/util/symbol/0x1b6c5864375b34af3ff5bd2e5f40bc425b4a8d79/POLYGON
// 请求方法: GET
// 状态代码: 200 
res = {
    "name": "-",
    "symbol": "-",
    "errorMsg": "call revert exception [ See: https://links.ethers.org/v5-errors-CALL_EXCEPTION ] (method=\"name()\", data=\"0x\", errorArgs=null, errorName=null, errorSignature=null, reason=null, code=CALL_EXCEPTION, version=abi/5.6.4)"
}
  
// 请求网址: https://api.guild.xyz/v1/util/symbol/0x1b6c5864375b34af3ff5bd2e5f40bc425b4a8d79/FANTOM
// 请求方法: GET
// 状态代码: 200 
res = {
    "name": "-",
    "symbol": "-",
    "errorMsg": "call revert exception [ See: https://links.ethers.org/v5-errors-CALL_EXCEPTION ] (method=\"name()\", data=\"0x\", errorArgs=null, errorName=null, errorSignature=null, reason=null, code=CALL_EXCEPTION, version=abi/5.6.4)"
  }