// 请求网址: https://api.guild.xyz/v1/role/24534
// 请求方法: DELETE
// 状态代码: 200 
// delete guild

const payload={
    "payload": {
      "removePlatformAccess": "0"
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "/5RRc36AAZyw5ygkTXlbWrvb8JyTzN2LNDEAkH5j+wM=",
      "ts": "1670587344931",
      "hash": "0x941b5113ce7b9250f22142b33dc8cc1c2a1c52e097c8fbaced6ef41e48c4187d",
      "method": 1,
      "msg": "Please sign this message"
    },
    "sig": "0x13446e04ef718824e155f25c3423b71040c861de87ea9f35f73c504d5863141940c8bc59e988e69f89c3a476c2f664a0fd83d9be02d4a9e1b5a44923e55f429a1b"
}
  const res={
    "success": true
  }

//   请求网址: https://api.guild.xyz/v1/role/24465
// 请求方法: DELETE
// 状态代码: 200 
const payload={
    "payload": {
      "removePlatformAccess": "0"
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "Ju7roOR2bSbxsZrSWeoP9cwba9knGPe85ZI5kyhT+xg=",
      "ts": "1670587685513",
      "hash": "0x941b5113ce7b9250f22142b33dc8cc1c2a1c52e097c8fbaced6ef41e48c4187d",
      "method": 1,
      "msg": "Please sign this message"
    },
    "sig": "0x49637b790318bc53ee9b72461486cec2aeb2d6f5d0e015f6a2be16e45728550b713621f6ffefff9ef773c44c02bdd72e8e4adba9ccae5bca6da39bf85e3d08eb1c"
}
  const res={
    "success": true
  }

//   请求网址: https://api.guild.xyz/v1/guild/details/akuns-server
// 请求方法: POST
// 状态代码: 200 
const payload=
{
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "GVC7ax92Cbp0fCD5cP/ooD1eP/ogeabPPZ86TjXfgbY=",
      "ts": "1670587694588",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "00fea7b8e6e750e77edba2720855e1cb512c310645a2d807eb0f9fdb4c32e41ecfa2cc55bb5545d951a291f2647e1e5b9598904771a63e982587a5da51557d9f"
}
  const res={
    "id": 14434,
    "name": "akun's server",
    "urlName": "akuns-server",
    "description": "",
    "imageUrl": "/guildLogos/145.svg",
    "showMembers": true,
    "hideFromExplorer": false,
    "createdAt": "2022-11-07T03:45:36.802Z",
    "onboardingComplete": true,
    "admins": [
      {
        "id": 241503,
        "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
        "isOwner": true
      },
      {
        "id": 718135,
        "address": "0xeef29c005557a8aaa8bdf155c81d9f8dc736f5ea",
        "isOwner": false
      }
    ],
    "theme": {
      "mode": "LIGHT",
      "color": null,
      "backgroundImage": "https://guild-xyz.mypinata.cloud/ipfs/QmbrDbWSsMavWHkaRPxcYyPRWXYhbz6bqPLydpcPYhjXad",
      "backgroundCss": null
    },
    "poaps": [
      {
        "id": 482,
        "fancyId": "test-2022-0305-2022",
        "activated": false,
        "expiryDate": 1667952000,
        "poapContracts": null,
        "poapIdentifier": 81741
      },
      {
        "id": 561,
        "fancyId": "23234234-2022",
        "activated": false,
        "expiryDate": 1696896000,
        "poapContracts": null,
        "poapIdentifier": 90460
      }
    ],
    "guildPlatforms": [
      {
        "id": 6257,
        "platformId": 1,
        "platformGuildId": "985740354131218472",
        "platformGuildData": {
          "invite": "https://discord.gg/theW5CqpMH",
          "inviteChannel": "1039022942744485888"
        },
        "platformGuildName": "akun's server",
        "invite": "https://discord.gg/theW5CqpMH"
      }
    ],
    "roles": [
      {
        "id": 22133,
        "name": "Member",
        "logic": "AND",
        "imageUrl": "/guildLogos/88.svg",
        "description": "123456",
        "requirements": [
          {
            "id": 115292,
            "data": null,
            "name": "-",
            "type": "FREE",
            "chain": "ETHEREUM",
            "roleId": 22133,
            "symbol": "-",
            "address": null
          }
        ],
        "rolePlatforms": [
          {
            "id": 10285,
            "platformRoleId": "1039022788838707211",
            "guildPlatformId": 6257,
            "platformRoleData": null
          }
        ],
        "memberCount": 1
      },
      {
        "id": 24256,
        "name": "fff",
        "logic": "AND",
        "imageUrl": "https://guild-xyz.mypinata.cloud/ipfs/QmT7ZyELmMHyHF76CYitbUof6yEWqseEoPqz1rKadaS5PK",
        "description": "sdfsdf",
        "requirements": [
          {
            "id": 126861,
            "data": {},
            "name": "-",
            "type": "FREE",
            "chain": "ETHEREUM",
            "roleId": 24256,
            "symbol": "-",
            "address": null
          }
        ],
        "rolePlatforms": [
          {
            "id": 12346,
            "platformRoleId": "1048590676524273685",
            "guildPlatformId": 6257,
            "platformRoleData": null
          }
        ],
        "memberCount": 1
      }
    ],
    "memberCount": 1
  }

//   请求网址: https://cloudflare-eth.com/
// 请求方法: POST
// 状态代码: 200 
const payload=
{
    "method": "eth_chainId",
    "params": [],
    "id": 43,
    "jsonrpc": "2.0"
}
  const res={
    "jsonrpc": "2.0",
    "result": "0x1",
    "id": 43
  }
//   请求网址: https://api.guild.xyz/v1/role/22133
//   请求方法: PATCH
//   状态代码: 200 
const payload={
    "payload": {
      "roleId": 22133,
      "name": "Member",
      "description": "123456",
      "imageUrl": "/guildLogos/88.svg",
      "logic": "AND",
      "requirements": [
        {
          "type": "COIN",
          "chain": "ETHEREUM",
          "address": "0x0000000000000000000000000000000000000000",
          "data": {
            "minAmount": 1,
            "maxAmount": 13
          }
        }
      ],
      "rolePlatforms": [
        {
          "id": 10285,
          "platformRoleId": "1039022788838707211",
          "guildPlatformId": 6257
        }
      ]
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "M5evJV6zk2vcwiB9kRflMHhesoVu7xMLpDN4RbpxNeA=",
      "ts": "1670588021608",
      "hash": "0xf657c9f64a2d73ff0953cb61270b69109cc2179791d9c0b96dd7c9da17fdf1c1",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "730eb970f13017c5cf7f88b52f7f2f4196a0245beba4fd558d14520ec804df8c37034d9d27ee23edc48e5af9a71a1cabbcdbdfb19e87391088ce366faf8e94d1"
}
  const res={
    "id": 22133,
    "name": "Member",
    "description": "123456",
    "imageUrl": "/guildLogos/88.svg",
    "createdAt": "2022-11-07T03:45:36.829Z",
    "hidden": false,
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 129054,
        "type": "COIN",
        "address": "0x0000000000000000000000000000000000000000",
        "data": {
          "minAmount": 1,
          "maxAmount": 13
        },
        "symbol": "ETH",
        "name": "Ether",
        "chain": "ETHEREUM",
        "roleId": 22133
      }
    ]
  }

  //添加机器到到群组，-1001624312779 是群组id
//   请求网址: https://api.guild.xyz/v1/telegram/group/-1001624312779
// 请求方法: GET
// 状态代码: 201 

//未添加机器人
res={
{
    "ok": false,
    "message": "It seems like our Bot hasn't got the right permissions."
}
//已添加机器人
res2=
{
    "ok": true,
    "groupName": "akun & q",
    "groupIcon": ""
  }
  //添加机器人  reward

// 请求网址: https://guild.xyz/api/ddrum?ddforward=https%3A%2F%2Frum.browser-intake-datadoghq.eu%2Fapi%2Fv2%2Frum%3Fddsource%3Dbrowser%26ddtags%3Dsdk_version%253A4.26.0%252Capi%253Afetch%252Cenv%253Aprod%252Cservice%253Aguild.xyz%252Cversion%253A1.0.0%26dd-api-key%3Dpub7cf22f3b79a010363cf58c859cfa8ad8%26dd-evp-origin-version%3D4.26.0%26dd-evp-origin%3Dbrowser%26dd-request-id%3De5ff534d-3617-4d58-9295-3bb96d682433%26batch_time%3D1670589825643
// 请求方法: POST
// 状态代码: 202 

const payload = { 很长的一段数据 }
const res ={
    "request_id": "e5ff534d-3617-4d58-9295-3bb96d682433"
}
  
//add reward
// 请求网址: https://api.guild.xyz/v1/guild/14434/platform
// 请求方法: POST
// 状态代码: 200 
const payload={
    "payload": {
      "platformName": "TELEGRAM",
      "platformGuildId": "-1001624312779",
      "roleIds": [
        "22133"
      ]
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "qD/b9j+0CwsBhLcKNp7d/NQC3KE+bX8AExqI4Qx2y7c=",
      "ts": "1670590056645",
      "hash": "0x9395e1ffc6805e401b3751b8285f8494c1296b669ed41f96d6190a9f8f769807",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "683292e3438e85c3fbf6a222c731624f77f375aaae3fff9e292e78d6757f4dc3792b1d9561489e59d0391e5ce3306a342d96194f749c0a323fe11b7774fb5f31"
}
  
const res={
    "id": 7520,
    "guildId": 14434,
    "platformId": 2,
    "platformGuildId": "-1001624312779",
    "data": {}
}
  
//add reward 之后
// 请求网址: https://api.guild.xyz/v1/guild/details/akuns-server
// 请求方法: POST
// 状态代码: 200 
payload=
{
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "hIAEMWs02roBcN7nIhenphfmR/xFiWz4Vy6DMj/YtJs=",
      "ts": "1670590059402",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "1ddd6bfd5d1f4cb3f46e33ee138220afb8bb0e8be6b728542674797ff7bed02ab0021a44ac616e85d3f043b325c4018a949c405371f9fea73059178a5e8e2c7a"
}
  res={
    "id": 14434,
    "name": "akun's server",
    "urlName": "akuns-server",
    "description": "",
    "imageUrl": "/guildLogos/145.svg",
    "showMembers": true,
    "hideFromExplorer": false,
    "createdAt": "2022-11-07T03:45:36.802Z",
    "onboardingComplete": true,
    "admins": [
      {
        "id": 241503,
        "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
        "isOwner": true
      },
      {
        "id": 718135,
        "address": "0xeef29c005557a8aaa8bdf155c81d9f8dc736f5ea",
        "isOwner": false
      }
    ],
    "theme": {
      "mode": "LIGHT",
      "color": null,
      "backgroundImage": "https://guild-xyz.mypinata.cloud/ipfs/QmbrDbWSsMavWHkaRPxcYyPRWXYhbz6bqPLydpcPYhjXad",
      "backgroundCss": null
    },
    "poaps": [
      {
        "id": 482,
        "fancyId": "test-2022-0305-2022",
        "activated": false,
        "expiryDate": 1667952000,
        "poapContracts": null,
        "poapIdentifier": 81741
      },
      {
        "id": 561,
        "fancyId": "23234234-2022",
        "activated": false,
        "expiryDate": 1696896000,
        "poapContracts": null,
        "poapIdentifier": 90460
      }
    ],
    "guildPlatforms": [
      {
        "id": 6257,
        "platformId": 1,
        "platformGuildId": "985740354131218472",
        "platformGuildData": {
          "invite": "https://discord.gg/theW5CqpMH",
          "inviteChannel": "1039022942744485888"
        },
        "platformGuildName": "akun's server",
        "invite": "https://discord.gg/theW5CqpMH"
      },
      {
        "id": 7520,
        "platformId": 2,
        "platformGuildId": "-1001624312779",
        "platformGuildData": {},
        "platformGuildName": "akun & q"
      }
    ],
    "roles": [
      {
        "id": 22133,
        "name": "Member",
        "logic": "AND",
        "imageUrl": "/guildLogos/88.svg",
        "description": "123456",
        "requirements": [
          {
            "id": 129057,
            "data": {},
            "name": "-",
            "type": "FREE",
            "chain": "ETHEREUM",
            "roleId": 22133,
            "symbol": "-",
            "address": null
          }
        ],
        "rolePlatforms": [
          {
            "id": 10285,
            "platformRoleId": "1039022788838707211",
            "guildPlatformId": 6257,
            "platformRoleData": null
          },
          {
            "id": 12929,
            "platformRoleId": null,
            "guildPlatformId": 7520,
            "platformRoleData": null
          }
        ],
        "memberCount": 1
      }
    ],
    "memberCount": 1
  }

  //add reward 之后
//   请求网址: https://api.guild.xyz/v1/guild/access/14434/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: GET
// 状态代码: 200 
res=[
    {
      "roleId": 22133,
      "access": true,
      "requirements": [
        {
          "requirementId": 129057,
          "access": true,
          "amount": null
        }
      ]
    }
]
  //claim reward 通过oauth连接到telegram,然后发送消息
// 请求网址: https://api.guild.xyz/v1/user/connect
// 请求方法: POST
// 状态代码: 200 
payload={
    "payload": {
      "platformName": "TELEGRAM",
      "authData": {
        "id": 1171935800,
        "first_name": "akun",
        "last_name": "haha",
        "username": "akunX12",
        "photo_url": "https://t.me/i/userpic/320/P-SXlSAVjWLYRi3BadwNKjArzdINdWaZeqzAjkd0k_8.jpg",
        "auth_date": 1670590258,
        "hash": "50181bedf95539c43e7296a3d76d39073d597756518f52e9177a5c7a051f02ce"
      }
    },
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "0TkVuQ1E7UR+PrF9+Uql15GhR393eP6q/U4qpX7csOI=",
      "ts": "1670590259203",
      "hash": "0xb3cf99698dce4bd3459e02710669a1437501ef55fdcc2df9b3bb1a46a7bdc70e",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "4bd956330bd400aa092934791708e2a949f8512a255b04f0cc9a5e5245cf1912389812b83ddc8997e4dc64c6e2e5c9e0e4bb0c5449cc35d62ae16c7ebd5ad1b5"
}
  res={
    "success": true,
    "platformUserId": "1171935800"
  }

    //claim reward 之后
//     请求网址: https://api.guild.xyz/v1/user/details/0x8dAe03Ec76EF4F0e4d885D13F1714A62e4C18Aa1
// 请求方法: POST
// 状态代码: 200 
payload=
{
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "tAZ30KxWGZrCJEtZiXWEJSNzNmri3hZ4GLeMnF5fmzA=",
      "ts": "1670590261729",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "3ee704cfd1ecd0c52083900be3ab2473b79c249ea0be983b5dc0b5d94f39ba05acb6603ce39092ab3f7238bfc92d10b5bcd129eab5500fdaefa8406dc239bddd"
}
  res ={
    "id": 241503,
    "addresses": [
      "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
    ],
    "createdAt": "2022-09-13T08:29:13.450Z",
    "signingKey": "04ff51225541b55874c05804af722fbea8177b144b4ad0f28cfc40a3036f0035ded621b4c7d95afb5ada4514f359f8faa60b97e491d64b9abe77ff9db998dd7662",
    "isSuperAdmin": false,
    "platformUsers": [
      {
        "platformId": 1,
        "platformName": "DISCORD",
        "platformUserId": "825368282814742559",
        "platformUserData": {
          "scope": "identify guilds guilds.members.read",
          "expiresIn": 1671175351431,
          "accessToken": "K4zXur2mn8jqbttqynVaSUP35XXK_tE4UYOXi96lic4P4JXDPm3MGIpkiRnBWDphDDYXnschF2cOui7lm3rMDDQ0ZExJMVRWcWxqRmdOeWpjSTUzU3dITlA3YzBwcw",
          "refreshToken": "EB4U6n6yaO1FUCypEv4oTwS4sric9snmLbwBkMxw5Uf-vDoLdZukDb2rp1k6CumVdli4cHbdBsyvR1MWjUEUDFJuYnU2c0NabVZ4UjQ0UnVxQ0kxZ2d3VFZQSktQMA"
        },
        "username": "akun",
        "avatar": "https://cdn.discordapp.com/avatars/825368282814742559/adfb8381146dac73af6922104b9ebd13.png"
      },
      {
        "platformId": 2,
        "platformName": "TELEGRAM",
        "platformUserId": "1171935800",
        "platformUserData": null,
        "username": "akunX12",
        "avatar": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAICAgICAQICAgIDAgIDAwYEAwMDAwcFBQQGCAcJCAgHCAgJCg0LCQoMCggICw8LDA0ODg8OCQsQERAOEQ0ODg7/2wBDAQIDAwMDAwcEBAcOCQgJDg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg7/wAARCACgAKADASIAAhEBAxEB/8QAHgAAAgIDAQEBAQAAAAAAAAAABgcFCAMECQIBCgD/xABAEAACAQMDAgQDBgQEAwkBAAABAgMEBREAEiEGMQcTQVEiYXEIFDKBkbEjocHRFTNCUhaC4QkYJDREYnKS8KL/xAAcAQADAQADAQEAAAAAAAAAAAACAwQFAAEGBwj/xAAtEQACAgEEAAMGBwEAAAAAAAAAAQIRAwQSITETImEFMkFRcbEUgZGh0eHwwf/aAAwDAQACEQMRAD8ApoaSqkyRFIw9W2EjW9Baa6THl00hJ7ALzq6A6fsNLSZis8D4HeQFzx9Trep4qWKRZIKWGl+HKiOJVx+g18h8fEuk2fQ1iyPtoqzbejeoZolaG3S9u5jbH7aOKPofq8KgCU1KPdnyf0zp6bmlZiJdwHPJ1i8wJVoije7NgLnuTpfjc8RX3GeDxzIW1P4eX+ddtVf/ALupHIgTn+mtyLwotpYNcblW3FsfF5sxx+mmBWV1FZ69aW63GC1Tk/HBV1KRuo9OCc69xXSgqYi9FWLXKMYaGVXAH/KTpjy6iC6r8q/4CsWGXfP5/wBgavh30tRkBLXFKfQyEtqSp+m6CCEfdrfT065yCIgo0TJWEj/I59Cxzr194ZnAQAknlu40PiZZ+9JsPw8cfdSRpW7p6nqK1YpB5jduFwNP/pmzUlDT0lLBAqRrjOFwNCHS9pZJ/vFRzvHAPtpq2xM1KpHj4e3y1uaKFSTZlamVpovJ4dyUSeFVH5EatKhwwjHOfc6OJmrJxEkESxRk5LsOcfTSy8GBG/QVYxAMq1OCfYFQf76cTFEUszBVAzzwANfTHkVp18vseAeBuUrfH9iI8derqPw1+zve+rJzGamJBBQwzNxNUOcINvrj4mI9lOvz0+KPWl56u67q7hcq+e4TyE75ah8lsHI7cAfIcDtrob9uvxftfU3iFRdI2a7w1lmsUbic00odZat/xkEcHYoCfIl9ctbnMJa1XG0c52j0/wCuhhlyZsig+kaMNPj0+C12zDbrXLV3ONIVZ5nbC4HOT2/nr9WHTNvag8PLFS1Cf+Kit8CTn3cRqGJ/POvz+/ZE8NH8S/tg9KW+an8y00FQtwuRIyvkwkOVP/yIVf8Am1+iHWprXsjDGnz2zPxxU5OTR/emPTURerfHX9OVMBQMSuVyAedS+vEn/l35xx66yYSlCaku0MywjLG0/kUl6sthgu1WuzeigquR2+WkhdaUxVcgi/hSFd2B+2rR+IFArS1DqQHD5Yp3Oq5XmjMcvmk/EeMnjXqNT5mpfMy9PPyIqo9TIygADBGOedeHeQqMkqAMADXkjL7F4H+4jW0lMX+Bid36a/Ml0fbEjUjV3cBWOccknUjb1SnuEdWzFvJV5A2MncEJX/8ArGsC08FRWyQKx2xEb2U8E+2dSYVIIFSMDA9CM512sm2SaC2ppnLPxKraut8Tam5R1RuD1TgyTCXzCxz3JPz/AE0wfC/qa8WK4xTU9VGTGPwugIwe4x3I9xrD46eF106D6xqOpLbStJ0bXVBkjljG77lIxyYm44Gc7SfT6aWth6to6CjcuoLkdnXg/nr6A834jAnDlP8A1HmI4vBy1Lijpj0x1ND1PZ1ljpXpqyKPNRAxyPcsp/26k7d1J0xVdQR0Md9oTKo3vEKhScD1765ozeJl7rqJrXaamSkpWz5squVOM8Z0G3e6VNCKWakq3gnSYHzY5MMSeS3/AO99Z0PZ0HPuinJq3GPHJ3fstwtNRY6aW3VsFanlbD5DhgMH5aLbSyCqExk8tA2TngY+evz12nxU6wtvUF5rqG+1dKrUqwsI5yuVDE8D3yTz89FH/eH8Sq2wy0N36ouNTa6qMxCFaoqFAPoAR2+et/D7OUZJqf7GFk1u5e7+53/g+2t4K+C/St4gvd6mvtzNQIoaC0ReaxlUHKs5wiD5k65veOP20uv/ABt8Q5jDXzdIdOUgaOgtFrrXA8tif4srrgyOVwD/AKRyAMd+b1T1PW197enralpYZcBhL3DY+fbUfBdq23VXluWidHIhlJ4xnsPQjXpW1VR4MaPErZYKaX7zveO7yySA7hHLnnOexB9v66wWgVVZf4aJXMs7thcnjHqSfYDk6V9uv81RCQv8Jw58wjj8s/np0eGtLVXm/COjp3krq1hHTADJVc5ZvzI/Qa7wy8N2xk/Pwdvv+zy6MpbJ4ddTdQJGrz1ypCs7D42RGOT9GbJ+ir7a6R6pn9lOOgs/RFtsqpKksNFtxuAijIAznB5Y8k+2rmbkPIcH89U6vnIn3aRLjdJ3xyz+1pXGoWmtckjEA44ydbTSxIMu4A+ulJ1x1NDDXGmSbEaL3U5ydDpsLy5En0uxGoyJY3GL5YEdXVMUiSEyDKg4Oe/11W/qBmZnXaAoJ500Lreaeoo5mOQcDnOc/wD7jSivc6tuCMAxPK609TmiuEJwYqRWRlhgUM4UEnsO51iSNZEaXYVOeA551iaQS15DdkGCTrYSVNrYYE+2vzc+j7OYo08ulKnkFtzEDudeg0hZjGdrD8OffX8SjgLuCnGSPYayowEXJGcZydD9DhmaGlrrBV2y6U8dXQ1URSeCaMSIw+anvqgniv4D2u19RyTWFjaYJ33RSR5eBc/6WT/T9Qfy1fcNlQMgH3OgTrWhpqnoyveuZTTbMMGkIDew+vsdaek1GTDNKL7Js+OM48nK2opqvpe4y2u6Ros4JdZY5PgmU9mU+q/seDoPu12lqKlYFVgVjZ1wcjj/AKatP1J0rbr7RVKSOMR72jScEFPc88447g86SFd0o9olaomgVoVY+R8W8SZBBOR9ew769vp88JO32eZ1GGdUuhfxTiHpWGMU6mpndkBY4PB7/qf5a0a5Wo6imo6hQHiUyuM4B3YwNTlUtNQ3enpapGqPvIEuZABsZ9y4HpjOOdY6Xpy63OPp+phppK162tno4yqk7njAbb/9TnHyOt7G75+BhTVcER50FUkySLIaiNCacg/jA9D8xqbkQ0/R9ruCSxPFUSmOSmY7mjKgfF8gedbydM1jXI1TlktNHKHhqWXCmESlXUn3AycfI6x37put6fpK+CsZBEa9I4dkgO1iAxB/5SNO3K0LSfJPR0v3W6U1IRvSSMTM8ZysmcfED6+gx76uf4FXWwdKzNcbrtqZSQjQB+dnHwYxkc+oOc/TVNrYWSSONiJUpBtEg5LHdhR+q5x8h76Y1D1I9nmRaCCKZyCZZmyy/wDU+mefXGkSnL4dlMIpdn6EfA3qK3dY0/3npW2ssESYI2AbCAC2PUge/rqy1N1DNTRsjuxIHOGOuS/2D/GGppfFqK3VzKaarUxzHjAyOD9fprotd7qILvUCPaF3kDn599bOm1TlCprozdRgW7joatV1M8lubfLncDxnt+ekj1BeaaeeTax3An4mPHz1qSXuT7s6LIPnlu+l7dK+eecxxqFd3wcc/XVktXGMaiTR0/JMVNSZYGy2RjgaXtyqEaX4Wy4GMjtohmkQW1lRiPqdBNaxClhnIJ7awsmfc7NOOOlRyYuf2oOuFkIoujaC0Izf+ukknkP0VdvHzOtWq8aPFm40riS6W6yROMqlLQASHj3JbbpUw0KwVcc8azVt1D+XUTSDIUnjaoHc4z39tMeogttlt8xp6xAUTMgqYQW3Hngnn0/lryUdLpIJVBff7noZZ9TO/MyCk6s61krI1qurK6rrWc7XhuLYHHbCYGtmO+dfW2zvPWdY1tLQSNuUJVs/I53A+ntjt8tAVVVS/eI1ZRFDKxeSVU+KXnhsDPb8sa/ppILHYZq641BrppsxxQEElW4YOTnuOx99aKjiqlFfov4IXLJ3uf6v+Rs2nx48QunpTSMF6rt2wPTVFYuyX3K7gPi4B7jRz1L44U/WvhTQU9Haai11MlRmrWU71yo4CEfiBznPGNVUo3r7upkqJ4qWBwzRIspTbj2HpohutJSdPeFVkmZqxxVzyzmUE7RyMbfT0Ooc+jwe+opS9P8AUWYdTlb2uVoN7/4hWd6Gnt1TFUxVCgYqFQbSO2Sd2RyNBL1zTyQlpg8Jid1ikXJJHYjjAPbt6aCWrKO8zGlpnZiqEBmQ7G5z+uiWxt59phhCmRguETzMZI4IHP56njjjijwuSpzeRg/U9MT3uYpGXlrKqNlpyPi2ybgyAY9GI9e3I1e/wG8EK64/Zg6e6gvFM9FeLN1VDXTQsmWMM8iws7A+ycn/ANraUPhx0nUVl/eMmOKmnTKKBjyxzgfmW7+m3XW/w6oo7d4XUEjBKuZ6Nae4MBzkD8R9+DnV/wCKccdEEtNunZQ3xa+zdW2fwY67s9nb7zJb61J7XIV+Kop5UhJTj0BLD3+An11Ufxj6LisPVNg6elpSa26yzSsGY7lZMRpn5sVJ/Ma7j3KGNJ6WCZkmZcKjuMrJvBAB/QarV4keEnTvUXiLaL7dKfz6yzUrT5HbdgnH6gHOixaxuXmFT01Lg5D0dDHDb2qV81C4Z/KH4gwbBz9FB49zqbFMJZNsIMUkzbBufiMcZH99PWl6SMt2vcUVtWKakmcoxgJVmBwpA7bQDv8AntHvrHbfDttlRJJSVEqKeJVT4DnuMk85OScZ+ue1kp8WBGHwHj9laikt/XcFa8iQFGHKrkYGMDXVa41RnYMi7ARuLFs7idc6vDK2x2amtVPHD5EbzRrJgYIG4Zx+WuilUrRhYokHlhRsyPTTcWRxg2JywTkD9RUyFiOFQeudDtRH5lcZRWRwlVJCHOT9D76JJ2QU8vmJzg+mguRnZ5JRsK5xjQvM2qsFQSNqU5olXlieOTxqImiAjbdj5DW40waJFXmQjI9jqFrJSkpUfi7dtI3UNaRyM/w5rR0vOsE8zztseTbABE5z3L+49PXS3mSSv6huJvFbLH5ECzU8bSM+7Bxtzj8x++pWqv073OGCWvlEW8hUcGQnPs5z669VQDWKSsZqdJI2yyxtuHzzn8P5cZ1mRk0/qaUknwQFdK9Fa6eqMn3msLs6RiIqGXHIJwPixyDoBrLyl7rlEykxwRgJDI5LHHck85/pr5feoaisuD1dNOzJCQigsMpx6Aenz41HRPJbLQ1XNWRzGRiRSzwneWPBII7d+57608cXFc9kGRpukFNA0b1kFFQy7Z2wagrHxCgPAUntnPr31ZvqjpusvH2duka+jzVUdLUtFLCEVwuTxk8YJHp21T23VE8MZm5iaXjco37vbGOf5a6WeCFgh6r+zClgBCXLzBWQ5cFiUbBwp5UempNfPbi4+ZToo7spRTqdavpHqGmhns7x26T4jJGchMD1x7Z/no08M7HRdSdQotPUCpjjXO0KOfX8vzGrm+J/g7LUdM1FNHTJBPInnK3kgeYAPiU55xj21U7pvp6p8O+ozfLbb1+/qvxRo/wttOSFzwCe3Y4zrHhljlx11L7mvLG8eS+4lr+jukIqKeGqqoXpoYztQMpyeRn6gjv/AF1c7wqqY4Za+GSo+8edGzyK5+FQThcD0GM/y1zmvv2rrVbfDmG5Wjon/H55ZE8wS1DQxxluHxhS2FYY+I5500/Cj7RUPV/UFut01kPS/UNXTEwKZ/MilA5KbsDDfUc4751S8eTw7a/yJvFhOW1MuffpzCir54ZIqgmE5/0jlf0OdRd4qaMxVdTJKrLURKZCfTjGNQVbBcarpeOsmJYswbv240setbrILVBQwSFHkA3DPpjn+ulRi26O5cCx6l6h6a6Ulrb/AH6tprTaIm8pJ5o97SY7KiDJcnsBg9tDXT3jH0bfa230NhirK5K6UBZaiAxs2c849O2qx9Q9N9eeIfXl7FfY57lbErJIaDEgVIoVchQAex4yWHJJydWE8JPCKXpY09zuwjjqIU201NG24RZGCS3qcca2bUI+pBTlK/gWTtEcb3e3xxJwZ1xkjPfV46iVI7bAZGXeIwD8uNU56HoVuHiTbqdWIjjfe5xxxq11eFejdckNjIO7Q73sYiaqQP19wR654uHQd+eRrRakijiJUjySO3Y6H5Y2jq2YSHyc5GV7H662I7iRSmHcW4IBHtpS4OGtWCOMFUlKBTkFWwc6hJqh5MkHbj+etqrlMsz/ABbgcZyNRJmVWwzcZ9tdNtnDjZa5Ybl1ElNFXRRKOAqL5hXjvyMqx7D9xrS60vP+F26WniqPLzH5bZcBiMd9o/P1Op+iuMdFYJmijqPLlk+N/K8tWI5ILf6u3fI+mkj1XdhcLlUU0S5hLgo8hw3HoBnQ4Y78voivLLZj9WC9BBNVXPyi+PXJON3sM41NR0krX9/vcDVCAZI3lyRjtz6j+mvlot71ld93EU0kQHwtGfXHBOeO/ro9pOk7ktnqKyGglwsPwzSHYu715Hf3GtOU0jOjG0CFUKanroGpohECgLJJHlicdxjV9/ALp25/4NYev+nPE+ySUcQSS69OMGNVRTtK0SwupwoMqRq4I3Dkeo1SKPpu/C8rLPRtUMWX+GiDf8+/YfLnV6/su+A14rvHW19S1NXVWS3wt50hEabZE7lNu0g5zjk8d9YftGMp4FslTXonfp6fU3vZupnp8kkl5ZcM6eVdvsXUXTVsvFRSLNmMMp2Dcvuv0znjS6uXgh4f9RPU1NNTLHI53vHCQoz810/pLbFRWhYaaMCAL8KAcAaU10pqi1Vn3+2ySRSEksVzyfmPX9tZMHXZTJ310Ux64+yRYa3qWqqrdPWdPTTvmcUJURTN/uaNlK7vmMZ1u9K/Zks9gvtvutHPXV16pJUkhqaqYc7TnGFAAHftxq3dB1Ml3qUo7rEyVbnbE6xYDH0Hy1NU8LRV7UkkBhfcBvcgavWXJVWTKMYu6BiZ5P8Ag2tgkpvJaJPKn78NjuD+h+mqkdbz1UHUkq/5ksBw2O3I7j+Wug0tBRVlluyPEpkWMGPB4YBcEn8zqoHW9JSi9yvJGjNja5CjgDgD9NOxSSkMmk42gB6ZtUFJTL5f8RPx/Eecnkn9dF9RIscSxR4EjcY9R89Dlt3wWOnwSZjGMkjsMev5axrWvVXzEI/hoQN3+4++q3yyF2Wo6C6Tis1ipbpMfNqKpNx3YGwj0Hv+ejurqXEZYEnj31B9CXFKvwyFFMWeaNQyfFqRmkLRbffjHrp+RKNbemZ8bbdgVXytJWO247s87j21E+edw2PuIOCQfXW/dRNFXsY4/NjP4wO/11DYCvKWJAZsjOpmwjLPUZg3AfFnnUU1QWO0ja2OfTj31tSKCxcNtP651GzBvN3su4Z7j+2gsJI4936WSC3Gkt6VFZKyiNwQrY47ADP66h+nfDC99SNvpqNy24YcQkhFHcklSMfnpmdJ9D3XrPr2ngp5Yo6WFg0qqNxbt3wf3OujPQ/hJR09hpKRTPDGMb/LbZv9+2mRyRwQ9WOlCWWXoUh8N/s+3OtvheWmnrgGws0yCBTzycZzj241ffo37OnT4lo6i62uWWSMKPglOwY+WAP5ae1j8PrJR0yhqWWQEfEfNK5+uDnTDobVbaCn8umpPKwOB5jHH6nUGTUObsqhiUUI67+Avh/VRuGs8RkcgtMU2SZznuDpndF9H2vpe1iltgaMcZ5zgDUvVFfPLM53E4A9tSdG22MAjI+nGopSbKoJErWSt9xaM85HJ+WlX1HRxy06hRmTBwM/r+WmJWyiRTh9pI+I+2gytnp4FkjCI+e7HvjQINiCvC3ChdqmNY4GjzIpij3OCO3J7alen/FaKvWOn6pjFFWQrhazI2y9u+B8H89SfUtM8+7yjlGJAAGkrdbY8jTgQnaoHGOMaqjQgtHV9W0kFjNbbK2KrjlhZGjjdW79iMfTt/bVNupbtcbn1LWRRxP8T92XaFHzOtOopJ6eT+CzoCfj2sRjWGalmL75HZmPBLN++qI0glkqNUZJqzFBHQ0RLMBh5f3xqZtdOsKIcDOomiov44OO+imBBGqnA9vrp6bZIx4dBVUgp5FyQgTAOmE9RgAEYJ9QeDpfdFKkVvKEfGQD9dGsrd1Kkgc8a7lInceSHuDKXIVgT3IJ0Lzl2mIDZUe/OiCtwY2KcAHIyO2heolKTBWHLHAbvjQ7rB28nqV2B2t3PAOtKRnwSThc8a8ec7MoOcke39tetwCtnv6YOuBqPzM9j8Mbb055NHSQAOoG8J8IY+/GnJY7TT0yj4i8gGCA2duiWWxhql5mjYjPHOM/L56+ZSkj/jyJEB2jT+p1kObl2au1I3Iwkaj4JR88ZGsk048nb5uD9dQ63iFpNoyy+hUk/wAtZzWUMw2vO0LH/fHj99FxXALTR9EHmTq7EMfTnUr5gipcH9tRaCOKYNFMGQ/prTuNwVIjtJOPXjGu9tnSbPlXWCNGzIBn0znS6r7gZK541bgtySeB89bF0vILDaGYDuRwToMmuMLTEuhA9SdEonVklUvDBB5plDlTt25yckHk6DK+RPuokkRfLfO8r7axT14huE9QB5sU2FC57H31A1F3pZVKPMYSIgSrdh3BGmpHRpzQUreYQVIYcc/PUDcoliUEZOMA60LhWz010ikVGlpcEHHprHV3OmFE0skmyEDLMfT/AKacuxb5PsVRFCjFmCgckk6D/wDjpKjraS2QY8qKQAuD+I+o0seu+paybqAW+21WyjWMEvE3+Zn30N2ATRXeOUgli/cn+ejc6B22dDei7kGghJkypXaTpmrUQtFISzFyvwbe2c9zquXRFefuUG3LTjBUE/Dn1J06DUT1MS1MSsqbQhjGOT6n99BKXIto3as7wwJBU9z/AE0O1MCF8ucp64OpxSXpl3AknUfUQndjH5+mutyOUDZ3oSoUFgcKc8kawM1b5gaOEYB4UHudSckR+8PtXH5axZI+AtjkemOddbgqLrVcCiEKQBx9NAN2gmw8dMihvcjONMaWaCWXKbZCO3rjQ/c0WTJclVA/CoxqKKTRS7TFCDNQ3MmYOGJz8BHOiSG4QVFLmRGKgZy4H76j79DBJSlfiiPo2Tn6aBFrRTziN6ghPRXjBP566aoYnuXIV191gijcQoVA5BBwNCFRdYdksjyYcDHxDAXQt1JdpJIGNLOImXLBMfi5/bSxPW0YusluuZWlqzn7vKT8Eo/uOeNNinQDpPgPqysllLzLMzOH2qvsPc6H7jclNPMynKqgPOhafqimpkkd32bVIkUHJyM/EPrpbVXWd2utsiS02qSMSuMtVAqVTnnb8++D76ckAxjUd3E9vfzJAswk2qG9PnqMr6inFYxl2tEUAb8s6XVBbby9HJPX1rTVTysQV+EKPQAenGpdaV/JaSpdpFAxgn00d/IEz1d9pqGx1Us8n8BFyu7kgemNIe+dZ3C8OqUmaWmAw6Kf8wHv+Ws/Wt/FyuaWq3sTTw4MzofxMCeNQVFRgIqYx76FyoOMbMdLTGUBm+IAfDonoImSaLaOM5xjX2KmVEQAYA41K0sPIBGOfbnSHK2HQ++hqhlELNyOM88asJQyYpQwOUI3H56rz0dT7KGP4SSMc6e1pkdolBOT8xom+BDRMpMXYkALxwNfCHaOQgELnk4417NM2NqK3JzleMa3/IZaVwT8JxkjuPbXLoGmD7wkuXBwfrrTqaNWKOOZByRzzokkpXUnK+wORrXmg2QFj3HPHf6aGzlMaXhz1tS9T0Eo85WqUONqnnGmfLEr7g6Hn58apd0WZ+mupYp42yUcb1U8EauPQXanrLfFOh3CRAV9zqHDmUzTy4nEg7skK25tilCMkEjuf66r11TFUxyNMAYgGypPrnuP56sRdagltzoowPhGlD1HQSVTHhdq5ZsknPt9NXOmiRKmIu6zvV0LgcuoHIPb21TnxM6oqKPq6Kwzv/nTCahqDw0T+qH5H0P9tXQuNMsFVJF56h/VS3OqffaI6TWrs1Nd6dTFNA+DIBnHOQeORz66fipPkXNOrCbpao++0Cy1MpkLRqoLHJxpp0VNSQxBsrjtqnnQvXxt9tWjvcbb4xhZo+Q319tMWTxYt0MG2lhqqwj8IC4BP1OilGmcT4LBVclJFC+zAGNxGlD191YlsshtdFIHr6vKq6n/AC0I5P1540DXTxFutwcxW6I0cTKA0knLYH7euoi328VUy1dRKJ37Ak57aBtRQSTbMdpt+2PJAYnuSNEsEBWoKFRk8g4xqQp6RMZRQD7alUomV3cIXIAxgjUkpFaVIj4qZicEfTI1P0VJmRAV7Hk+2t2moN4SU4BYZIz20TW63BqyNVAK59PfSt1s7aGR0vRiO2KdnIAye356a9siZaYMi8hhxj00KWGlEFBEoxnHxc8aP6CKMQEAgE+ueNNc+BO1WTMMTtANx+IHGttYwU2ng63KaEmmTIwwGdbjQK0ZJ5wOToLsBxogmjIJDKDjBB751jeJHUbo2wecH++t+SJjIVjkCsO575/LX0bVqF5Lhj7cDXaBao//2Q=="
      },
      {
        "platformId": 3,
        "platformName": "GITHUB",
        "platformUserId": "112370890",
        "platformUserData": {
          "readonly": false,
          "reauthable": false,
          "accessToken": "9kMSKhTRopQW7eK6FUFkqyL1PwbYSZpnMviWRuiDMehSfNofTCbsU-4SVA6qHrxGCpwEUO8uTbT5K3w3N_EqCGdob19jSTdrOGExODBCTDVyVEhvMzgyVTJwa0RVNWdSdGYxMEhpaDU"
        },
        "username": "kamran-hz",
        "avatar": "https://avatars.githubusercontent.com/u/112370890?v=4"
      }
    ]
  }

//   请求网址: https://api.guild.xyz/v1/guild/details/akuns-server
//   请求方法: POST
//   状态代码: 200 
payload=
  {
    "payload": {},
    "params": {
      "addr": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
      "nonce": "wVc4c1NV3dMyfwHJ+4xnX9ncbBOS+/To0D/N9dXpnYg=",
      "ts": "1670590987717",
      "method": 2,
      "msg": "Please sign this message"
    },
    "sig": "27d8f1cc7d2d82d11816dfa1b9ba2e0c715f884f610085bf0e3b0bb7d8cfceb7ce79060b6b655935e858b1365db33b01876e04855f020a30f58b2b826f65c92e"
}
  res={
    "id": 14434,
    "name": "akun's server",
    "urlName": "akuns-server",
    "description": "",
    "imageUrl": "/guildLogos/145.svg",
    "showMembers": true,
    "hideFromExplorer": false,
    "createdAt": "2022-11-07T03:45:36.802Z",
    "onboardingComplete": true,
    "admins": [
      {
        "id": 241503,
        "address": "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1",
        "isOwner": true
      },
      {
        "id": 718135,
        "address": "0xeef29c005557a8aaa8bdf155c81d9f8dc736f5ea",
        "isOwner": false
      }
    ],
    "theme": {
      "mode": "LIGHT",
      "color": null,
      "backgroundImage": "https://guild-xyz.mypinata.cloud/ipfs/QmbrDbWSsMavWHkaRPxcYyPRWXYhbz6bqPLydpcPYhjXad",
      "backgroundCss": null
    },
    "poaps": [
      {
        "id": 482,
        "fancyId": "test-2022-0305-2022",
        "activated": false,
        "expiryDate": 1667952000,
        "poapContracts": null,
        "poapIdentifier": 81741
      },
      {
        "id": 561,
        "fancyId": "23234234-2022",
        "activated": false,
        "expiryDate": 1696896000,
        "poapContracts": null,
        "poapIdentifier": 90460
      }
    ],
    "guildPlatforms": [
      {
        "id": 6257,
        "platformId": 1,
        "platformGuildId": "985740354131218472",
        "platformGuildData": {
          "invite": "https://discord.gg/theW5CqpMH",
          "inviteChannel": "1050757894578126949"
        },
        "platformGuildName": "akun's server",
        "invite": "https://discord.gg/theW5CqpMH"
      },
      {
        "id": 7520,
        "platformId": 2,
        "platformGuildId": "-1001624312779",
        "platformGuildData": {},
        "platformGuildName": "akun & q"
      }
    ],
    "roles": [
      {
        "id": 22133,
        "name": "Member",
        "logic": "AND",
        "imageUrl": "/guildLogos/88.svg",
        "description": "123456",
        "requirements": [
          {
            "id": 129057,
            "data": {},
            "name": "-",
            "type": "FREE",
            "chain": "ETHEREUM",
            "roleId": 22133,
            "symbol": "-",
            "address": null
          }
        ],
        "rolePlatforms": [
          {
            "id": 10285,
            "platformRoleId": "1039022788838707211",
            "guildPlatformId": 6257,
            "platformRoleData": null
          },
          {
            "id": 12929,
            "platformRoleId": null,
            "guildPlatformId": 7520,
            "platformRoleData": null
          }
        ],
        "memberCount": 1
      }
    ],
    "memberCount": 1
  }

//   请求网址: https://api.guild.xyz/v1/role/22133
// 请求方法: GET
// 状态代码: 200
res={
    "id": 22133,
    "name": "Member",
    "description": "123456",
    "imageUrl": "/guildLogos/88.svg",
    "createdAt": "2022-11-07T03:45:36.829Z",
    "logic": "AND",
    "guildId": 14434,
    "requirements": [
      {
        "id": 129057,
        "type": "FREE",
        "chain": "ETHEREUM",
        "address": null,
        "data": {}
      }
    ],
    "rolePlatforms": [
      {
        "guildPlatformId": 6257,
        "platformRoleId": "1039022788838707211",
        "guildPlatform": {
          "id": 6257,
          "platformGuildId": "985740354131218472",
          "platformGuildData": {
            "invite": "https://discord.gg/theW5CqpMH",
            "inviteChannel": "1050757894578126949"
          },
          "platformGuildName": "akun's server",
          "invite": "https://discord.gg/theW5CqpMH",
          "platform": {
            "id": 1,
            "name": "DISCORD"
          }
        }
      },
      {
        "guildPlatformId": 7520,
        "platformRoleId": null,
        "guildPlatform": {
          "id": 7520,
          "platformGuildId": "-1001624312779",
          "platformGuildData": {},
          "platformGuildName": "akun & q",
          "platform": {
            "id": 2,
            "name": "TELEGRAM"
          }
        }
      }
    ],
    "members": [
      "0x8dae03ec76ef4f0e4d885d13f1714a62e4c18aa1"
    ]
  }