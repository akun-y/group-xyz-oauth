import {
  Center,
  Flex,
  GridItem,
  Heading,
  HStack,
  SimpleGrid,
  Spinner,
  Stack,
  Tag,
  Text,
  useColorMode,
  usePrevious,
} from "@chakra-ui/react"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import AddCard from "components/common/AddCard"
import Layout from "components/common/Layout"
import useUpvoty from "components/common/Layout/components/NavMenu/hooks/useUpvoty"
import Link from "components/common/Link"
import LinkPreviewHead from "components/common/LinkPreviewHead"
import CategorySection from "components/explorer/CategorySection"
import ExplorerCardMotionWrapper from "components/explorer/ExplorerCardMotionWrapper"
import GroupCard from "components/explorer/GroupCard"
import useMemberships, {
  Memberships,
} from "components/explorer/hooks/useMemberships"
import OrderSelect, { OrderOptions } from "components/explorer/OrderSelect"
import SearchBar from "components/explorer/SearchBar"
import { BATCH_SIZE, useExplorer } from "components/_app/ExplorerProvider"
import { useQueryState } from "hooks/useQueryState"
import useScrollEffect from "hooks/useScrollEffect"
import { GetStaticProps } from "next"
import { useEffect, useMemo, useRef, useState } from "react"
import useSWR from "swr"
import { GroupBase } from "types"
import fetcher from "utils/fetcher"

type Props = {
  groups: GroupBase[]
}

const getUsersGroups = (memberships: Memberships, groupsData: any) => {
  //console.info("memberships------------:", memberships)
  //console.info("groupsData------------:", groupsData)
  if (!memberships?.length || !groupsData?.length) return []
  const usersGroupsIds = memberships.map((membership) => membership.groupId)
  const newUsersGroups = groupsData.filter((group) =>
    usersGroupsIds.includes(group.id)
  )
  return newUsersGroups
}

const Page = ({ groups: groupsInitial }: Props): JSX.Element => {
  const { account } = useWeb25React()

  const [search, setSearch] = useQueryState<string>("search", undefined)
  const prevSearch = usePrevious(search)
  const [order, setOrder] = useQueryState<OrderOptions>("order", "members")
  const { renderedGroupsCount, setRenderedGroupsCount } = useExplorer()

  const query = new URLSearchParams({ order, ...(search && { search }) }).toString()

  useEffect(() => {
    if (prevSearch === search || prevSearch === undefined) return
    setRenderedGroupsCount(BATCH_SIZE)
  }, [search, prevSearch])

  const groupsListEl = useRef(null)

  const {
    data: [allGroups, filteredGroups],
    isValidating: isLoading,
  } = useSWR(
    `/group?${query}`,
    (url: string) =>
      fetcher(url).then((data) => [
        data,
        data.filter(
          (group) =>
            (group.platforms?.length > 0 && group.memberCount > 0) ||
            group.memberCount > 1
        ),
      ]),
    {
      fallbackData: [groupsInitial, groupsInitial],
      dedupingInterval: 60000, // one minute
    }
  )

  useScrollEffect(() => {
    if (
      !groupsListEl.current ||
      groupsListEl.current.getBoundingClientRect().bottom > window.innerHeight ||
      filteredGroups?.length <= renderedGroupsCount
    )
      return

    setRenderedGroupsCount((prevValue) => prevValue + BATCH_SIZE)
  }, [filteredGroups, renderedGroupsCount])

  const renderedGroups = useMemo(
    () => filteredGroups?.slice(0, renderedGroupsCount) || [],
    [filteredGroups, renderedGroupsCount]
  )

  const memberships = useMemberships()
  const [usersGroups, setUsersGroups] = useState<GroupBase[]>(
    getUsersGroups(memberships, allGroups)
  )

  useEffect(() => {
    setUsersGroups(getUsersGroups(memberships, allGroups))
  }, [memberships, allGroups])

  // Setting up the dark mode, because this is a "static" page
  const { setColorMode } = useColorMode()

  useEffect(() => {
    setColorMode("dark")
  }, [])

  const { isRedirecting, upvotyAuthError } = useUpvoty()

  if (isRedirecting)
    return (
      <Flex alignItems="center" justifyContent="center" direction="column" h="100vh">
        <Heading mb={4} fontFamily="display">
          Group - Upvoty authentication
        </Heading>
        {upvotyAuthError ? (
          <Text as="span" fontSize="lg">
            You are not a member of any groups. Please <Link href="/">join one</Link>{" "}
            and you can vote on the roadmap!
          </Text>
        ) : (
          <HStack>
            <Spinner size="sm" />
            <Text as="span" fontSize="lg">
              Redirecting, please wait...
            </Text>
          </HStack>
        )}
      </Flex>
    )

  return (
    <>
      <LinkPreviewHead path="" />
      <Layout
        title="Grouphall"
        description="Automated membership management for the platforms your community already uses."
        showBackButton={false}
      >
        <SimpleGrid
          templateColumns={{ base: "auto 50px", md: "1fr 1fr 1fr" }}
          gap={{ base: 2, md: "6" }}
          mb={16}
        >
          <GridItem colSpan={{ base: 1, md: 2 }}>
            <SearchBar placeholder="Search groups" {...{ search, setSearch }} />
          </GridItem>
          <OrderSelect {...{ isLoading, order, setOrder }} />
        </SimpleGrid>

        <Stack ref={groupsListEl} spacing={12}>
          <CategorySection
            title={
              // usersGroups will be empty in case of unmatched search query, memberships will be empty in case he's owner but not member of groups
              usersGroups?.length || memberships?.length
                ? "Your groups"
                : "You're not part of any groups yet"
            }
            titleRightElement={
              account && (!memberships || isLoading) && <Spinner size="sm" />
            }
            fallbackText={`No results for ${search}`}
          >
            {usersGroups?.length || memberships?.length ? (
              (usersGroups.length || !search) &&
              usersGroups
                .map((group) => (
                  <ExplorerCardMotionWrapper key={group.urlName}>
                    <GroupCard groupData={group} />
                  </ExplorerCardMotionWrapper>
                ))
                .concat(
                  <ExplorerCardMotionWrapper key="create-group">
                    <AddCard text="Create group" link="/create-group" />
                  </ExplorerCardMotionWrapper>
                )
            ) : (
              <ExplorerCardMotionWrapper key="create-group">
                <AddCard text="Create group" link="/create-group" />
              </ExplorerCardMotionWrapper>
            )}
          </CategorySection>

          <CategorySection
            title="All groups"
            titleRightElement={
              isLoading ? (
                <Spinner size="sm" />
              ) : (
                <Tag size="sm">{filteredGroups.length}</Tag>
              )
            }
            fallbackText={
              search?.length
                ? `No results for ${search}`
                : "Can't fetch groups from the backend right now. Check back later!"
            }
          >
            {renderedGroups.length &&
              renderedGroups.map((group) => (
                <ExplorerCardMotionWrapper key={group.urlName}>
                  <GroupCard groupData={group} />
                </ExplorerCardMotionWrapper>
              ))}
          </CategorySection>

          <Center>
            {filteredGroups?.length > renderedGroupsCount && <Spinner />}
          </Center>
        </Stack>
      </Layout>
    </>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const groups = await fetcher(`/group?sort=members`)
    .then((list) => list)
    .catch((_) => [])

  return {
    props: { groups },
    revalidate: 10,
  }
}

export default Page
