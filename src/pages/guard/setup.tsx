import DiscordGroupSetup from "components/common/DiscordGroupSetup"
import Layout from "components/common/Layout"
import DynamicDevTool from "components/create-group/DynamicDevTool"
import EntryChannel from "components/create-group/EntryChannel"
import Disclaimer from "components/guard/setup/ServerSetupCard/components/Disclaimer"
import PickSecurityLevel from "components/guard/setup/ServerSetupCard/components/PickSecurityLevel"
import useIsConnected from "hooks/useIsConnected"
import useServerData from "hooks/useServerData"
import { useRouter } from "next/router"
import { useEffect } from "react"
import {
  FormProvider,
  useForm,
  useFormContext,
  useFormState,
  useWatch,
} from "react-hook-form"
import { GroupFormType } from "types"
import getRandomInt from "utils/getRandomInt"

const defaultValues = {
  name: "",
  description: "",
  imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
  roles: [
    {
      name: "Member",
      logic: "AND",
      requirements: [{ type: "FREE" }],
      rolePlatforms: [
        {
          groupPlatformIndex: 0,
        },
      ],
    },
  ],
  groupPlatforms: [
    {
      platformName: "DISCORD",
      platformGroupId: undefined,
      platformGroupData: {
        inviteChannel: "",
      },
    },
  ],
}

const Page = (): JSX.Element => {
  const router = useRouter()

  const isConnected = useIsConnected("DISCORD")

  useEffect(() => {
    if (!isConnected) {
      router.push("/guard")
    }
  }, [isConnected])

  const methods = useFormContext<GroupFormType>()
  const { errors } = useFormState()

  const selectedServer = useWatch({
    control: methods.control,
    name: "groupPlatforms.0.platformGroupId",
  })

  const {
    data: { channels },
  } = useServerData(selectedServer, {
    refreshInterval: 0,
  })

  return (
    <Layout title={selectedServer ? "Set up Group Guard" : "Select a server"}>
      <FormProvider {...methods}>
        <DiscordGroupSetup
          {...{ defaultValues, selectedServer }}
          fieldName="groupPlatforms.0.platformGroupId"
        >
          <EntryChannel
            channels={channels}
            label="Entry channel"
            tooltip={
              "Newly joined accounts will only see this channel with a join button in it by the Group.xyz bot until they authenticate"
            }
            showCreateOption
            maxW="50%"
            size="lg"
            fieldName="groupPlatforms.0.inviteChannel"
            errorMessage={errors.groupPlatform?.[0]?.inviteChannel}
          />

          <PickSecurityLevel rolePlatformIndex={0} />

          <Disclaimer />
        </DiscordGroupSetup>

        <DynamicDevTool control={methods.control} />
      </FormProvider>
    </Layout>
  )
}

const WrappedPage = () => {
  // TODO: form type
  const methods = useForm<any>({
    mode: "all",
    defaultValues,
  })

  return (
    <FormProvider {...methods}>
      <Page />
    </FormProvider>
  )
}

export default WrappedPage
