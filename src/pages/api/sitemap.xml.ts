import { GroupBase } from "types"
import fetcher from "utils/fetcher"

export default async function handler(_, res) {
  const baseUrl = {
    development: "http://localhost:3000",
    production: "https://group.xyz",
  }[process.env.NODE_ENV]

  const groups = await fetcher(`/group?sort=members`).catch((_) => [])

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${groups
        .map(
          (group: GroupBase) => `
        <url>
          <loc>${baseUrl}/${group.urlName}</loc>
          <changefreq>weekly</changefreq>
          <priority>1.0</priority>
        </url>
      `
        )
        .join("")}
    </urlset>
  `

  res.setHeader("Content-Type", "text/xml")
  res.write(sitemap)
  res.end()
}
