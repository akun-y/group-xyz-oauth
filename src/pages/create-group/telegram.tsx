import { Flex, VStack } from "@chakra-ui/react"
import { WithRumComponentContext } from "@datadog/rum-react-integration"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import Button from "components/common/Button"
import ErrorAlert from "components/common/ErrorAlert"
import ErrorAnimation from "components/common/ErrorAnimation"
import Layout from "components/common/Layout"
import LinkPreviewHead from "components/common/LinkPreviewHead"
import DynamicDevTool from "components/create-group/DynamicDevTool"
import useCreateGroup from "components/create-group/hooks/useCreateMyGroup"
import SetRequirements from "components/create-group/Requirements"
import TelegramGroup from "components/create-group/TelegramGroup"
import { Web3Connection } from "components/_app/Web3ConnectionManager"
import usePinata from "hooks/usePinata"
import useSubmitWithUpload from "hooks/useSubmitWithUpload"
import useWarnIfUnsavedChanges from "hooks/useWarnIfUnsavedChanges"
import { useContext, useEffect, useState } from "react"
import { FormProvider, useForm, useWatch } from "react-hook-form"
import { GroupFormType, PlatformType } from "types"
import getRandomInt from "utils/getRandomInt"

const defaultValues: GroupFormType = {
  name: "",
  description: "",
  imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
  groupPlatforms: [
    {
      platformId: PlatformType.TELEGRAM,
      platformName: "TELEGRAM",
      platformGroupId: "",
    },
  ],
}

const CreateTelegramGroupPage = (): JSX.Element => {
  const { account } = useWeb25React()
  const methods = useForm<GroupFormType>({ mode: "all", defaultValues })
  const [formErrors, setFormErrors] = useState(null)
  const { openWalletSelectorModal, triedEager } = useContext(Web3Connection)

  const { isLoading, isSigning, onSubmit, response, signLoadingText } =
    useCreateGroup()
  const { isUploading, onUpload } = usePinata({
    onSuccess: ({ IpfsHash }) => {
      methods.setValue(
        "imageUrl",
        `${process.env.NEXT_PUBLIC_IPFS_GATEWAY}${IpfsHash}`
      )
    },
    onError: () => {
      methods.setValue("imageUrl", `/groupLogos/${getRandomInt(286)}.svg`)
    },
  })

  const formRequirements = useWatch({
    name: "requirements",
    control: methods.control,
  })

  const { handleSubmit, isUploadingShown, uploadLoadingText } = useSubmitWithUpload(
    (...props) => {
      methods.clearErrors("requirements")
      if (
        !formRequirements ||
        formRequirements?.length === 0 ||
        formRequirements?.every(({ type }) => !type)
      ) {
        methods.setError(
          "requirements",
          {
            message: "Set some requirements, or make the role free",
          },
          { shouldFocus: true }
        )
        document.getElementById("free-entry-checkbox")?.focus()
      } else {
        return methods.handleSubmit(onSubmit, (errors) =>
          setFormErrors(errors ? Object.keys(errors) : null)
        )(...props)
      }
    },
    isUploading
  )

  useWarnIfUnsavedChanges(
    methods.formState?.isDirty && !methods.formState.isSubmitted
  )

  useEffect(() => {
    if (triedEager && !account) openWalletSelectorModal()
  }, [account, triedEager])

  const loadingText = signLoadingText || uploadLoadingText || "Saving data"

  return (
    <>
      <LinkPreviewHead path="" />
      <Layout title="Create Group on Telegram">
        {account ? (
          <FormProvider {...methods}>
            <ErrorAnimation errors={formErrors}>
              <VStack spacing={10} alignItems="start">
                <TelegramGroup
                  onUpload={onUpload}
                  fieldName="groupPlatforms.0.platformGroupId"
                />

                <SetRequirements />
              </VStack>
            </ErrorAnimation>
            <Flex justifyContent="right" mt="-10">
              <Button
                flexShrink={0}
                size="lg"
                w={{ base: "full", sm: "auto" }}
                colorScheme="green"
                disabled={isLoading || isUploadingShown || isSigning || !!response}
                isLoading={isLoading || isUploadingShown || isSigning}
                loadingText={loadingText}
                onClick={handleSubmit}
                data-dd-action-name="Summon"
              >
                {response ? "Success" : "Summon"}
              </Button>
            </Flex>
            <DynamicDevTool control={methods.control} />
          </FormProvider>
        ) : (
          <ErrorAlert label="Please connect your wallet in order to continue!" />
        )}
      </Layout>
    </>
  )
}

export default WithRumComponentContext(
  "Create Telegram group page",
  CreateTelegramGroupPage
)
