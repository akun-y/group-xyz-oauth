import GitHubGroupSetup from "components/common/GitHubGroupSetup"
import Layout from "components/common/Layout"
import useUser from "components/[group]/hooks/useUser"
import { useRouter } from "next/router"
import { useEffect } from "react"

const CreateGithubGroup = () => {
  const { platformUsers } = useUser()
  const isGithubConnected = platformUsers?.some(
    ({ platformName }) => platformName === "GITHUB"
  )
  const router = useRouter()

  useEffect(() => {
    if (platformUsers !== undefined && isGithubConnected === false) {
      router.push("/create-group")
    }
  }, [platformUsers, isGithubConnected])

  return (
    <Layout title="Create Group on GitHub">
      <GitHubGroupSetup />
    </Layout>
  )
}

export default CreateGithubGroup
