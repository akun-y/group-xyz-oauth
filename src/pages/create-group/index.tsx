import { Text } from "@chakra-ui/react"
import { WithRumComponentContext } from "@datadog/rum-react-integration"
import Layout from "components/common/Layout"
import PlatformsGrid from "components/create-group/PlatformsGrid"
import { useRouter } from "next/router"

const CreateGroupPage = (): JSX.Element => {
  const router = useRouter()

  return (
    <Layout title="Choose platform">
      <Text colorScheme={"gray"} fontSize="lg" fontWeight="semibold" mt="-8" mb="10">
        You can connect more platforms later
      </Text>
      <PlatformsGrid
        onSelection={(selectedPlatform) =>
          router.push(`/create-group/${selectedPlatform.toLowerCase()}`)
        }
      />
    </Layout>
  )
}

export default WithRumComponentContext("Create group index page", CreateGroupPage)
