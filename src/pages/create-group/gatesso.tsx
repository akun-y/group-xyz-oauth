import { Props } from "chakra-react-select"
import GatessoGroupSetup from "components/common/GatessoGroupSetup"
import Layout from "components/common/Layout"
import { useUserParse } from "components/[group]/hooks/useUser"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import isEmpty from "utils/isEmpty"

//创建、管理nervos相关的群组
const CreateGateSSOGroup = (props: Props) => {
  const { account, ssoID } = useWeb25React()
  //const { data: userParse } = useUserParse(session?.ssoID)
  //const { platformUsers } = userParse
  // const isConnected = platformUsers?.some(
  //   ({ platformName }) => platformName === "GATESSO"
  // )

  const router = useRouter()

  useEffect(() => {
    if (isEmpty(ssoID) || isEmpty(account)) {
      router.push("/create-group")
    }
  }, [ssoID, account])
  //console.info("session:", props)

  return (
    <Layout title="Create Group with SSO">
      <GatessoGroupSetup />
    </Layout>
  )
}

export default CreateGateSSOGroup
