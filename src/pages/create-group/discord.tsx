import { WithRumComponentContext } from "@datadog/rum-react-integration"
import DiscordGroupSetup from "components/common/DiscordGroupSetup"
import DiscordRoleVideo from "components/common/DiscordRoleVideo"
import Layout from "components/common/Layout"
import DynamicDevTool from "components/create-group/DynamicDevTool"
import useIsConnected from "hooks/useIsConnected"
import { useRouter } from "next/router"
import { useEffect } from "react"
import { FormProvider, useForm, useWatch } from "react-hook-form"
import { GroupFormType } from "types"
import getRandomInt from "utils/getRandomInt"

const defaultValues: GroupFormType = {
  name: "",
  description: "",
  imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
  groupPlatforms: [
    {
      platformName: "DISCORD",
      platformGroupId: "",
      platformGroupData: { inviteChannel: "" },
    },
  ],
  roles: [
    {
      name: "Member",
      logic: "AND",
      imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
      requirements: [{ type: "FREE" }],
      rolePlatforms: [
        {
          groupPlatformIndex: 0,
        },
      ],
    },
  ],
}

const CreateDiscordGroupPage = (): JSX.Element => {
  const router = useRouter()

  const isConnected = useIsConnected("DISCORD")

  useEffect(() => {
    if (!isConnected) {
      router.push("/create-group")
    }
  }, [isConnected])

  const methods = useForm<GroupFormType>({ mode: "all", defaultValues })

  const selectedServer = useWatch({
    control: methods.control,
    name: "groupPlatforms.0.platformGroupId",
  })

  return (
    <Layout title="Create Group on Discord">
      <FormProvider {...methods}>
        <DiscordGroupSetup
          {...{ defaultValues, selectedServer }}
          fieldName="groupPlatforms.0.platformGroupId"
        >
          <DiscordRoleVideo />
        </DiscordGroupSetup>

        <DynamicDevTool control={methods.control} />
      </FormProvider>
    </Layout>
  )
}

export default WithRumComponentContext(
  "Create Discord group page",
  CreateDiscordGroupPage
)
