import { WithRumComponentContext } from "@datadog/rum-react-integration"
import GoogleGroupSetup from "components/common/GoogleGroupSetup"
import Layout from "components/common/Layout"
import DynamicDevTool from "components/create-group/DynamicDevTool"
import { FormProvider, useForm } from "react-hook-form"
import { GroupFormType } from "types"
import getRandomInt from "utils/getRandomInt"

const defaultValues: GroupFormType = {
  name: "",
  description: "",
  imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
  theme: { color: "#3b82f6" },
  groupPlatforms: [
    {
      platformName: "GOOGLE",
      platformGroupId: "",
    },
  ],
  roles: [
    {
      name: "Member",
      logic: "AND",
      imageUrl: `/groupLogos/${getRandomInt(286)}.svg`,
      requirements: [{ type: "FREE" }],
      rolePlatforms: [
        {
          groupPlatformIndex: 0,
        },
      ],
    },
  ],
}

const CreateGroupGooglePage = (): JSX.Element => {
  const methods = useForm<GroupFormType>({ mode: "all", defaultValues })

  return (
    <Layout title="Create Group for Google Workspaces">
      <FormProvider {...methods}>
        <GoogleGroupSetup
          defaultValues={defaultValues}
          fieldNameBase="groupPlatforms.0."
          shouldSetName
          permissionField={"roles.0.rolePlatforms.0.platformRoleData.role"}
        />
        <DynamicDevTool control={methods.control} />
      </FormProvider>
    </Layout>
  )
}

export default WithRumComponentContext(
  "Create Google group page",
  CreateGroupGooglePage
)
