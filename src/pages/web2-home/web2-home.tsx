import { Stack } from "@chakra-ui/react"
import { addressToScript, serializeScript } from "@nervosnetwork/ckb-sdk-utils"
import CkbDefineNFT from "components/ckb/ckbDefineNFT"
import CotaNFTCardList from "components/ckb/cotaNFTCardList"
import AddCard from "components/common/AddCard"
import Layout from "components/common/Layout"
import LinkPreviewHead from "components/common/LinkPreviewHead"
import CategorySection from "components/explorer/CategorySection"
import ExplorerCardMotionWrapper from "components/explorer/ExplorerCardMotionWrapper"
import GroupCard from "components/explorer/GroupCard"
import useGenMPCUser, { useMPCAccessToken } from "components/[group]/hooks/useGenMPCUser"
import { createUserToParse } from "components/[group]/hooks/useUser"
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useToast from "hooks/useToast"
import { useEffect, useRef, useState } from "react"
import { GroupBase, StorgeItemName } from "types"
import { cotaService, isMainnet } from "utils/ckb"
import isEmpty from "utils/isEmpty"
import LoginPrompt from "./login-prompt"


const {SSOID,SSOTOKEN,USERSSO,MPCTOKEN,ACCOUNT,COTAADDRESS,SSOREFRESHTOKEN,SSONAME}=StorgeItemName
type Props = {
  groups: GroupBase[]
}

const Web2HomePage = ({ groups: groupsInitial }: Props): JSX.Element => {
  const { userParse, userSSO, ssoID, ssoName, ssoToken, mutate } = useWeb25React()
  const [lockScript, setLockScript] = useState<string>(null)

  const [isCreateUserLoading, setIsCreateUserLoading] = useState(false)
  const [userParseCreated, setUserParseCreated] = useState(false)
  const {
    access_token: mpc_access_token,
    isLoading: isMPCLoading } = useMPCAccessToken(ssoToken, ssoName)
  const { data: userMPC, isLoading: isGenLoading ,mutate:mpcMutate} = useGenMPCUser(mpc_access_token)
  const { testnetAddr, mainnetAddr } = userMPC
  const cotaAddress = isMainnet ? mainnetAddr : testnetAddr
  const toast = useToast()

  let usersGroups = null
  let memberships = null
  let search = null
  const groupsListEl = useRef(null)

  const [withdrawableNFTs, setWithdrawableNFTs] = useState([])

  //const { data: banlance, isLoading: isBalanceLoading } = useNervosBalance(cotaAddress)
  //const router = useRouter()
  //console.info("userMPC:", userMPC)

  useEffect(() => {
    if (mpc_access_token) {
      if(userMPC?.name) {
        window.localStorage.setItem(MPCTOKEN, mpc_access_token)

        window.localStorage.setItem(COTAADDRESS, cotaAddress)
        window.localStorage.setItem(ACCOUNT, userMPC?.ethAddr)
      } else {
        //console.info("userMPC is null")
        mutate("/ckb/gen_address")
        mpcMutate()
      }
    }
  }, [mpc_access_token, userMPC, isGenLoading])



   const isLoading = !userSSO?.name || isMPCLoading || isGenLoading
  // if (isLoading) {
  //   return (<div>loading...</div>)
  // }

  //判断用户是否存在，如果存在则获取WithdrawableNFTs List
  if (cotaAddress && withdrawableNFTs.length == 0 && lockScript == null) {
    const addr = cotaAddress
    const script = addressToScript(addr)
    const lockScript = serializeScript(script)
    setLockScript(lockScript)

    cotaService.aggregator.getWithdrawCotaNft({ lockScript, page: 0, pageSize: 50, })
      .then((res) => {
        if (res?.nfts)
          setWithdrawableNFTs(res.nfts)
      }).catch((e) => {
        console.error(e)
      })
  }
  //console.info("createUserToParse", userParse, isEmpty(userParse), isCreateUserLoading, userParseCreated)
  //如果用户不存在则在Parse创建用户
  if (!isCreateUserLoading && isEmpty(userParse) && !userParseCreated
    && userSSO?.name && userMPC?.name) {
    setIsCreateUserLoading(true)
    createUserToParse(userSSO, userMPC)
      .then((res) => {
        //console.info("createUserToParse:", res)
        mutate()
        setUserParseCreated(true)
        toast({
          title: "Success",
          description: "Login and Create user success",
          status: "success",
          duration: 6000,
          isClosable: true,
        })
        setIsCreateUserLoading(false)
      })
      .catch((e) => {
        console.error("createUserToParse:", e)
        mutate()
        toast({
          title: "Error",
          description: "Failed to create user:" + e.error,
          status: "error",
          duration: 4000,
          isClosable: true

        })
        setIsCreateUserLoading(false)
      })
  }


  //如果用户未创建或未登录，则提示并给出OAUTH登录按钮
  if (!ssoID || ssoID.length < 36) { return <LoginPrompt /> }

  return (
    <>
      <LinkPreviewHead path="" />
      <Layout
        title="Groups List"
        description="Automated membership management for the platforms your community already uses."
        showBackButton={false}
      >
        {(isLoading) && <div>loading...</div>}

        <Stack ref={groupsListEl} spacing={12}>
          <CategorySection
            title={
              // usersGroups will be empty in case of unmatched search query, memberships will be empty in case he's owner but not member of groups
              usersGroups?.length || memberships?.length
                ? "Your groups"
                : "You're not part of any groups yet"
            }
            // titleRightElement={
            //   account && (!memberships) && <Spinner size="sm" />
            // }
            fallbackText={`No results for ${search}`}
          >
            {usersGroups?.length || memberships?.length ? (
              (usersGroups.length || !search) &&
              usersGroups
                .map((group) => (
                  <ExplorerCardMotionWrapper key={group.urlName}>
                    <GroupCard groupData={group} />
                  </ExplorerCardMotionWrapper>
                ))
                .concat(
                  <ExplorerCardMotionWrapper key="create-group">
                    <AddCard text="Create group" link="/create-group" />
                  </ExplorerCardMotionWrapper>
                )
            ) : (
              <ExplorerCardMotionWrapper key="create-group">
                <AddCard text="Create group" link="/create-group" />
              </ExplorerCardMotionWrapper>
            )}
          </CategorySection>

          {/* {banlance && <p>banlance:{banlance}</p>} */}
          <p>{cotaAddress}</p>
          {cotaAddress && <CotaNFTCardList cotaAddress={cotaAddress} />}

          {/* <CotaNFTBarList /> */}
          <br />
          {cotaAddress && <CkbDefineNFT />}

          <br />


          {/* <Center>
            <Spinner />
          </Center> */}
        </Stack>
      </Layout>
    </>
  )
}



export default Web2HomePage

