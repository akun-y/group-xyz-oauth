import { Alert, AlertDescription, AlertIcon, AlertTitle, Stack, VStack } from "@chakra-ui/react"
import Layout from "components/common/Layout"
import LandingButton from "components/index/LandingButton"
import Link from "next/link"
import { ArrowRight } from "phosphor-react"

//如果用户未登录则显示登录页面，显示相关提示
const LoginPrompt = () => {
  return (
    <>
      <Layout title="Sign in on Nervos">
        <Alert status="error">
          <AlertIcon />
          <VStack alignItems={"start"}>
            <AlertTitle>Please Login First</AlertTitle>
            <AlertDescription>
              It looks like you haven't logged in yet. Sign in and return
              here to gate access to it!
            </AlertDescription>
            <Stack
              direction="column"
              spacing={4}
              align="center"
              justify="center"
              w="100%"
              h="100%"
            >
              <Link passHref href="/web2-login">
                <LandingButton
                  as="a"
                  marginTop="20"
                  w={{ base: "full", sm: "unset" }}
                  colorScheme="DISCORD"
                  rightIcon={<ArrowRight />}
                >
                  Sign in
                </LandingButton>
              </Link>
            </Stack>
          </VStack>
        </Alert></Layout>
    </>
  )
}
export default LoginPrompt