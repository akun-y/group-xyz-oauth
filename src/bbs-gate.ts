// Copyright 2020 The casbin Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Sdk from "casdoor-js-sdk"
import fetcher from "utils/fetcher"
import useSWRImmutable from "swr/immutable"

//Mac Book Pro
export const bbsGateConfig = {
    serverUrl: process.env.NEXT_PUBLIC_GATE_API,
    clientId: process.env.NEXT_PUBLIC_BBSGATE_CLIENT_ID,
    clientSecret: process.env.NEXT_PUBLIC_BBSGATE_CLIENT_SECRET,
    appName: process.env.NEXT_PUBLIC_BBSGATE_APP_NAME,
    organizationName: process.env.NEXT_PUBLIC_BBSGATE_ORG_NAME,
    redirectPath: process.env.NEXT_PUBLIC_BBSGATE_CALLBACK,
}

// export const bbsGateConfig = {
//     serverUrl: process.env.NEXT_PUBLIC_GATE_API,
//     clientId: "100a5c1584d9d12efc75",
//     clientSecret: "5bb717b902d970d5a7f39453701cefe252998452",
//     appName: "app_bbs",
//     organizationName: "org_bbs",
//     redirectPath: "/callback",
// }

// export const ServerUrl = "https://door.bbs.mfull.cn"
// const sdkConfig = {
//     serverUrl: "https://door.bbs.mfull.cn/",
//     clientId: "b1f74e40ad2bcedd8a43",
//     clientSecret: "5bb717b902d970d5a7f39453701cefe252998452",
//     appName: "app_papers_bbs",
//     organizationName: "organization_papers_bbs",
//     redirectPath: "/callback",
// }

export const CasdoorSdk = new Sdk(bbsGateConfig)

export let StaticBaseUrl = "https://bbs.mfull.cn"

export function getSignupUrl() {
    return CasdoorSdk.getSignupUrl()
}

export function getSigninUrl() {
    return CasdoorSdk.getSigninUrl()

}

export function getUserProfileUrl(userName, account) {
    return CasdoorSdk.getUserProfileUrl(userName, account)
}

export function getMyProfileUrl(account) {
    return CasdoorSdk.getMyProfileUrl(account)
}

export function getMyResourcesUrl(account) {
    return CasdoorSdk.getMyProfileUrl(account).replace("/account?", "/resources?")
}
export function signin() {
    return CasdoorSdk.signin(bbsGateConfig.serverUrl).then((res) => {
        if (res.status) {
            if (window !== window.parent) {
                const message = { tag: "Casdoor", type: "SilentSignin", data: "success" }
                window.parent.postMessage(message, "*")
            }
            //console.info("Logged in successfully")
        }

        return res
    }).catch((err) => {
        console.error(err)
        return err
    })
}


export function refresh() {
    window.location.reload()
}

export function goToLink(link) {
    window.location.href = link
}

export function openLink(link) {
    const w = window.open("about:blank")
    w.location.href = link
}





export function addRow(array, row) {
    return [...array, row]
}

export function deleteRow(array, i) {
    return [...array.slice(0, i), ...array.slice(i + 1)]
}

export function getFormattedDate(date) {
    date = date?.replace("T", " ")
    date = date?.replace("+08:00", " +08:00")
    return date
}



export function getStatic(path) {
    //return `https://cdn.casbin.org${path}`;

    const origin = window.location.origin
    return `${origin}${path}`
}

export function getUserAvatar(username, isLarge = false) {
    if (username === undefined) {
        return null
    }


}
