import type { Chain } from "connectors"

type Token = {
  address: string
  name: string
  symbol: string
  decimals: number
}

type DiscordError = { error: string; errorDescription: string }

type WalletError = { code: number; message: string }

type Rest = {
  [x: string]: any
}

type Logic = "AND" | "OR" | "NOR" | "NAND"

type ThemeMode = "LIGHT" | "DARK"

type Theme = {
  color?: string
  mode?: ThemeMode
  backgroundImage?: string
  backgroundCss?: string
}

type CoingeckoToken = {
  chainId: number
  address: string
  name: string
  symbol: string
  decimals: number
  logoURI: string
}

type Poap = {
  id: number
  fancy_id: string
  name: string
  event_url?: string
  image_url: string
  country?: string
  city?: string
  description?: string
  year: number
  start_date: string
  end_date: string
  expiry_date: string
  from_admin: boolean
  virtual_event: boolean
  event_template_id: number
  event_host_id: number
}

type GitPoap = {
  gitPoapEventId: number
  poapEventId: number
  poapEventFancyId: string
  name: string
  year: number
  description: string
  imageUrl: string
  repositories: string[]
  mintedCount: number
}

type NFT = {
  name: string
  type: string
  address: string
  logoUri: string
  slug: string
}

type RequirementType =
  | "COIN"
  | "ERC20"
  | "ERC721"
  | "ERC1155"
  | "CONTRACT"
  | "POAP"
  | "GITPOAP"
  | "MIRROR"
  | "MIRROR_COLLECT"
  | "UNLOCK"
  | "SNAPSHOT"
  | "JUICEBOX"
  | "GALAXY"
  | "ALLOWLIST"
  | "FREE"
  | "TWITTER"
  | "TWITTER_FOLLOW"
  | "TWITTER_NAME"
  | "TWITTER_BIO"
  | "TWITTER_FOLLOWER_COUNT"
  | "GITHUB"
  | "GITHUB_STARRING"
  | "NOUNS"
  | "DISCORD"
  | "DISCORD_ROLE"
  | "NOOX"
  | "DISCO"
  | "LENS"
  | "LENS_PROFILE"
  | "LENS_FOLLOW"
  | "LENS_COLLECT"
  | "LENS_MIRROR"
  | "OTTERSPACE"
  | "ORANGE"
  | "CASK"
  | "101"
  | "RABBITHOLE"
  | "KYC_DAO"
  | "GATESSO"

type NftRequirementType = "AMOUNT" | "ATTRIBUTE" | "CUSTOM_ID"

type PlatformName = "GATESSO" | "TELEGRAM" | "DISCORD" | "GITHUB" | "TWITTER" | "GOOGLE"

type PlatformAccount = {
  platformId: number
  platformName: PlatformName
}
type PlatformAccountDetails = PlatformAccount & {
  platformUserId: string
  username: string
  avatar: string
  platformUserData?: Record<string, any> // TODO: better types once we decide which properties will we store in this object on the backend
}


type GateAccessTokenResponse = {
  access_token: string
  expires_in: number
  id_token: string
  refresh_token: string
  scope: string
  token_type: string
}


export interface Response {
  data: UserSSOBase
  data2: UserSSOOrg
  msg: string
  name: string
  status: string
  sub: string
}
type Web25Session = {
  account?: string//eth address
  ENSName?: string
  cotaAddr?: string
  ssoID?: string
  ssoToken?: string
  ssoRefreshToken?: string
  mpcToken?: string

}
type User = {
  id?: number//服务端生成的id
  ssoID: string
  publicKey: string
  addresses: Array<string>
  cotaAddresses: Array<string>
  platformUsers: Array<PlatformAccountDetails>
  isSuperAdmin?: boolean
  isDeleted?: boolean
}
type UserSSOBase = {
  address: string[]
  affiliation: string
  apple: string
  avatar: string
  azuread: string
  baidu: string
  bio: string
  birthday: string
  createdIp: string
  createdTime: string
  dingtalk: string
  displayName: string
  education: string
  email: string
  facebook: string
  gender: string
  gitee: string
  github: string
  gitlab: string
  google: string
  hash: string
  homepage: string
  id: string
  idCard: string
  idCardType: string
  infoflow: string
  isAdmin: boolean
  isDefaultAvatar: boolean
  isDeleted: boolean
  isForbidden: boolean
  isGlobalAdmin: boolean
  isOnline: boolean
  language: string
  lark: string
  lastSigninIp: string
  lastSigninTime: string
  ldap: string
  linkedin: string
  location: string
  name: string
  owner: string
  password: string
  passwordSalt: string
  permanentAvatar: string
  phone: string
  preHash: string
  properties: { [key: string]: any }
  qq: string
  ranking: number
  region: string
  score: number
  signupApplication: string
  slack: string
  tag: string
  title: string
  type: string
  updatedTime: string
  wechat: string
  wecom: string
  weibo: string
}

type UserSSOOrg = {
  createdTime: string
  defaultAvatar: string
  displayName: string
  enableSoftDeletion: boolean
  favicon: string
  masterPassword: string
  name: string
  owner: string
  passwordSalt: string
  passwordType: string
  phonePrefix: string
  websiteUrl: string
}
type UserSSO = {
  status: string
  msg: string
  sub: string
  name: string
  data: UserSSOBase
  data2: UserSSOOrg
}
//多方计算 (MPC) 密钥管理中心用户
type UserMPC = {
  _id: string
  ethAddr: string
  name: string

  privateKey: string
  publicKey: string

  mainnetAddr: string
  testnetAddr: string
  blake160: string
  email: string
  id: string
  lockHash: string

  username: string
}

// type GroupBase = {
//   id: number
//   name: string
//   urlName: string
//   imageUrl: string
//   roles: Array<string>
//   platforms: Array<PlatformName>
//   memberCount: number
// }

// type GroupAdmin = {
//   id: number
//   address: string
//   isOwner: boolean
// }
type CotaNFTBase = {
  audio: string
  characteristic: string
  configure: string
  cotaId: string
  description: string
  image: string
  metaCharacteristic: string
  model: string
  name: string
  properties: string
  state: string
  symbol: string
  tokenIndex: string
  video: string
}
type CotaNFTDefine = CotaNFTBase & {
  audio: string
  blockNumber: number
  configure: number
  description: string
  image: string
  issued: number
  metaCharacteristic: string
  model: string
  name: string
  properties: string
  symbol: string
  total: number
  video: string
}
type PlatformGroupData = {
  DISCORD: {
    role?: never
    inviteChannel: string
    invite?: string
    joinButton?: boolean
    mimeType?: never
    iconLink?: never
  }
  GOOGLE: {
    role?: "reader" | "commenter" | "writer"
    inviteChannel?: never
    joinButton?: never
    mimeType?: string
    iconLink?: string
  }
}

type PlatformRoleData = {
  DISCORD: {
    isGuarded: boolean
    role?: never
  }
  GOOGLE: {
    isGuarded?: never
    role: "reader" | "commenter" | "writer"
  }
}

type ContractParamType = string[]

type DiscoParamType = {
  credType: string
  credIssuence: "before" | "after"
  credIssuenceDate: string
  credIssuer: string
}

type RabbitholeParamType = {
  trait_type: string
  value: string
}[]

type Requirement = {
  id: number
  data?: {
    hideAllowlist?: boolean
    minAmount?: number
    maxAmount?: number
    addresses?: Array<string> // (ALLOWLIST)
    id?: string // fancy_id (POAP), edition id (MIRROR), id of the project (JUICEBOX)
    name?: string
    provider?: string
    planId?: number
    strategy?: {
      name: string
      params: Record<string, any>
    } // SNAPSHOT
    attribute?: {
      trait_type?: string
      value?: string
      interval?: {
        min: number
        max: number
      }
    }
    galaxyId?: string
    serverId?: string
    roleId?: string
    serverName?: string
    roleName?: string
    // CONTRACT
    expected?: string
    resultIndex?: number
    resultMatch?: string
    params?: ContractParamType | DiscoParamType | RabbitholeParamType
  }
  name: string
  type: RequirementType
  chain: Chain
  roleId: number
  symbol: string
  address: string
  decimals?: number

  // Props used inside the forms on the UI
  active?: boolean
  nftRequirementType?: string

  // These props are only used when we fetch requirements from the backend and display them on the UI
  balancyDecimals?: number
}

type RolePlatform = {
  platformRoleId?: string
  groupPlatformId?: number
  groupPlatform?: GroupPlatform
  index?: number
  isNew?: boolean
  roleId?: number
  platformRoleData?: PlatformRoleData[keyof PlatformRoleData]
}

type Role = {
  id: number
  name: string
  logic: Logic
  members: string[]
  imageUrl?: string
  description?: string
  memberCount: number
  requirements: Requirement[]
  rolePlatforms: RolePlatform[]
}

type GroupPlatform = {
  id: number
  platformId: PlatformType
  platformName?: PlatformName
  platformGroupId: string
  platformGroupData?: PlatformGroupData[keyof PlatformGroupData]
  invite?: string
  platformGroupName: string
}

type PoapContract = {
  id: number
  poapId: number
  chainId: number
  vaultId: number
  contract: string
}

type GroupPoap = {
  id: number
  poapIdentifier: number
  fancyId: string
  activated: boolean
  expiryDate: number
  poapContracts?: PoapContract[]
}

// type Group = {
//   id: number
//   name: string
//   urlName: string
//   description?: string
//   imageUrl: string
//   showMembers: boolean
//   hideFromExplorer: boolean
//   createdAt: string
//   admins: GroupAdmin[]
//   theme: Theme
//   groupPlatforms: GroupPlatform[]
//   roles: Role[]
//   members: Array<string>
//   poaps: Array<GroupPoap>
//   onboardingComplete: boolean
// }
type GroupFormType = Partial<
  Pick<Group, "id" | "urlName" | "name" | "imageUrl" | "description" | "theme">
> & {
  groupPlatforms?: (Partial<GroupPlatform> & { platformName: string })[]
  roles?: Array<
    Partial<
      Omit<Role, "requirements" | "rolePlatforms"> & {
        requirements: Array<Partial<Requirement>>
        rolePlatforms: Array<Partial<RolePlatform> & { groupPlatformIndex: number }>
      }
    >
  >
  logic?: Logic
  requirements?: Requirement[]
}

const RequirementTypeColors = {
  ERC721: "var(--chakra-colors-green-400)",
  ERC1155: "var(--chakra-colors-green-400)",
  CONTRACT: "var(--chakra-colors-gray-400)",
  NOUNS: "var(--chakra-colors-green-400)",
  POAP: "#8076FA",
  GITPOAP: "#307AE8",
  MIRROR: "var(--chakra-colors-gray-300)",
  MIRROR_COLLECT: "var(--chakra-colors-gray-300)",
  ERC20: "var(--chakra-colors-indigo-400)",
  COIN: "var(--chakra-colors-indigo-400)",
  SNAPSHOT: "var(--chakra-colors-orange-400)",
  ALLOWLIST: "var(--chakra-colors-gray-200)",
  UNLOCK: "var(--chakra-colors-salmon-400)",
  JUICEBOX: "var(--chakra-colors-yellow-500)",
  GALAXY: "var(--chakra-colors-black)",
  FREE: "var(--chakra-colors-cyan-400)",
  TWITTER: "var(--chakra-colors-twitter-400)",
  TWITTER_FOLLOW: "var(--chakra-colors-twitter-400)",
  TWITTER_NAME: "var(--chakra-colors-twitter-400)",
  TWITTER_BIO: "var(--chakra-colors-twitter-400)",
  TWITTER_FOLLOWER_COUNT: "var(--chakra-colors-twitter-400)",
  GITHUB: "var(--chakra-colors-GITHUB-400)",
  GITHUB_STARRING: "var(--chakra-colors-GITHUB-400)",
  DISCORD_ROLE: "var(--chakra-colors-DISCORD-400)",
  NOOX: "#7854f7",
  DISCO: "#bee4e0",
  LENS_PROFILE: "#BEFB5A",
  LENS_FOLLOW: "#BEFB5A",
  LENS_COLLECT: "#BEFB5A",
  LENS_MIRROR: "#BEFB5A",
  OTTERSPACE: "#a6ea8e",
  101: "#000000",
  ORANGE: "#ff5d24",
  RABBITHOLE: "#7f23dc",
  KYC_DAO: "#3D65F2",
  CASK: "#7a4db6",
}

type SnapshotStrategy = {
  name: string
  params: Record<string, Record<string, string>>
}

type JuiceboxProject = {
  id: string
  uri: string
  name: string
  logoUri: string
}

type MirrorEdition = {
  editionContractAddress: string
  editionId: number
  title: string
  image: string
}

type SelectOption = {
  label: string
  value: string
  img?: string
} & Rest

// Requested with Discord OAuth token
type DiscordServerData = {
  id: string
  name: string
  icon: string
  owner: boolean
  permissions: number
  features: string[]
  permissions_new: string
}

type Group = {
  objectId: string

  id: number
  name: string
  urlName: string
  description?: string
  imageUrl: string
  showMembers: boolean
  hideFromExplorer: boolean
  createdAt: string
  admins: GroupAdmin[]
  theme: Theme
  groupPlatforms: GroupPlatform[]
  roles: Role[]
  members: Array<string>
  poaps: Array<GroupPoap>
  onboardingComplete: boolean
}
type GroupBase = {
  objectId: string

  id: number
  name: string
  urlName: string
  imageUrl: string
  roles: Array<string>
  platforms: Array<PlatformName>
  memberCount: number
}
type CreateGroupForm = {

  //objectId?: string

  //id?: number
  name: string
  urlName: string
  description?: string
  imageUrl: string
  showMembers: boolean
  hideFromExplorer: boolean
  //createdAt: string
  admins: GroupAdmin[]
  theme: Theme
  groupPlatforms: GroupPlatform[]
  roles: Role[]
  //members: Array<string>
  poaps: Array<GroupPoap>
  onboardingComplete: boolean

  memberCount: number
}
type GroupAdmin = {
  id?: number,
  ssoID: string
  address: string
  cotaAddress: string
  isOwner: boolean
}

type CreatePoapForm = {
  name: string
  description: string
  city: string
  country: string
  start_date: string
  end_date: string
  expiry_date: string
  year: number
  event_url: string
  virtual_event: boolean
  image: File
  secret_code: number
  event_template_id: number
  email: string
  requested_codes: number
  private_event: boolean
}

type CreatedPoapData = {
  id?: number
  fancy_id?: string
  name: string
  description: string
  city: string
  country: string
  start_date: string
  end_date: string
  expiry_date: string
  year: number
  event_url: string
  virtual_event: boolean
  image_url?: string
  event_template_id: number
  private_event: boolean
  event_host_id?: number
}

export enum PlatformType {
  "UNSET" = -1,
  "DISCORD" = 1,
  "TELEGRAM" = 2,
  "GITHUB" = 3,
  "GOOGLE" = 4,
  "TWITTER" = 5,
  "GATESSO" = 6,
}
export enum StorgeItemName {
  SSOID = "group_auth_sso_id",
  SSONAME = "group_auth_sso_name",
  SSOTOKEN = "group_auth_sso_token",
  SSOREFRESHTOKEN = "group_auth_sso_refresh_token",
  SSOEXPIRESIN = "group_auth_sso_expires_in",
  USERSSO = "group_auth_user_sso",

  MPCTOKEN = "group_auth_mpc_token",
  COTAADDRESS = "group_auth_cota_address",
  ACCOUNT = "group_auth_account",
}
type WalletConnectConnectionData = {
  connected: boolean
  accounts: string[]
  chainId: number
  bridge: string
  key: string
  clientId: string
  clientMeta: {
    description: string
    url: string
    icons: string[]
    name: string
  }
  peerId: string
  peerMeta: {
    description: string
    url: string
    icons: string[]
    name: string
  }
  handshakeId: number
  handshakeTopic: string
}

enum ValidationMethod {
  STANDARD = 1,
  KEYPAIR = 2,
  EIP1271 = 3,
}

type GalaxyCampaign = {
  id: string
  numberID: number
  name: string
  thumbnail: string
  chain: Chain
}

type MonetizePoapForm = {
  chainId: number
  token: string
  fee: number
  owner: string
}

type RequestMintLinksForm = {
  event_id: number
  requested_codes: number
  secret_code: string
  redeem_type: string
}

type GoogleFile = {
  name: string
  mimeType: string
  webViewLink: string
  iconLink: string
  platformGroupId: string
}

type VoiceParticipationForm = {
  poapId: number
  voiceChannelId: string
  voiceRequirement: {
    type: "PERCENT" | "MINUTE"
    percentOrMinute: number
  }
}

type VoiceRequirement =
  | {
    percent: number
    minute?: never
  }
  | {
    percent?: never
    minute: number
  }

type PoapEventDetails = {
  id: number
  poapIdentifier: number
  fancyId: string
  groupId: number
  activated: boolean
  createdAt: string
  expiryDate: number
  voiceChannelId?: string
  voiceRequirement?: VoiceRequirement
  voiceEventStartedAt: number
  voiceEventEndedAt: number
  contracts: PoapContract[]
}

type VoiceRequirementParams = {
  poapId: number
  voiceChannelId: string
  voiceRequirement: VoiceRequirement
  voiceEventStartedAt?: number
}

export type {
  WalletConnectConnectionData,
  DiscordServerData,
  //GroupAdmin,
  GroupAdmin,
  Token,
  DiscordError,
  WalletError,
  Rest,
  CoingeckoToken,
  Poap,
  GitPoap,
  PoapContract,
  //GroupPoap,
  GroupPoap,
  User,
  UserSSO,
  UserMPC,
  NFT,
  Role,
  //GroupPlatform,
  //GroupBase,
  //Group,
  GroupPlatform,
  GroupBase,
  Group,
  Requirement,
  RequirementType,
  SnapshotStrategy,
  JuiceboxProject,
  MirrorEdition,
  RolePlatform,
  ThemeMode,
  Logic,
  PlatformAccountDetails,
  SelectOption,
  NftRequirementType,
  //GroupFormType,
  GroupFormType,
  CreatePoapForm,
  CreatedPoapData,
  PlatformName,
  GalaxyCampaign,
  MonetizePoapForm,
  RequestMintLinksForm,
  GoogleFile,
  VoiceRequirement,
  VoiceParticipationForm,
  VoiceRequirementParams,
  PoapEventDetails,
  ContractParamType,
  DiscoParamType,
  RabbitholeParamType,
  CotaNFTBase,
  CotaNFTDefine,
  CreateGroupForm,
  GateAccessTokenResponse,
  Web25Session,
}
export { ValidationMethod, RequirementTypeColors }
