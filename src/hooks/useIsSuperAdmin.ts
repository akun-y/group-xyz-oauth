import { fetcherParse2 } from 'utils/fetcher';
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import useSWRImmutable from "swr/immutable"

const useIsSuperAdmin = () => {
    const { account } = useWeb25React()

    const { data } = useSWRImmutable(account ? ["/parse/functions/isSuperAdmin",
        { method: "POST", body: { address: account } }] : null, fetcherParse2)
    
    return data
}

export default useIsSuperAdmin
