import { useUserParse } from "components/[group]/hooks/useUser"
import useSWR, { SWRConfiguration } from "swr"
import { PlatformName } from "types"
import { fetcherParse2 } from "utils/fetcher"
import useKeyPair from "./useKeyPair"

type Gateables = {
  DISCORD: Array<{ img: string; name: string; owner: boolean; id: string }>
  GITHUB: Array<{
    avatarUrl: string
    description?: string
    platformGroupId: string
    repositoryName: string
    url: string
  }>
  GATESSO: Array<{
    avatarUrl: string
    description?: string
    platformName: string
    name: string
    url: string
    result: any
    objectId: string
  }>
} & Record<PlatformName, unknown>

const useGateablesWeb2 = <K extends keyof Gateables>(
  platformName: K,
  ssoID: string,
  swrConfig?: SWRConfiguration
) => {
  const { keyPair } = useKeyPair()

  const { data: userParse } = useUserParse(ssoID)
  const { platformUsers } = userParse || {}
  console.log("platformUsers:", platformUsers)
  const isConnected = !!platformUsers?.some(
    (platformUser) => platformUser.platformName === platformName
  )

  //const fetcherWithSign = useFetcherWithSign()

  const shouldFetch = isConnected && platformName?.length > 0

  //console.info("platformName:", platformName, shouldFetch, ssoID)
  const { data, isValidating, mutate, error } = useSWR<Gateables[K]>(
    shouldFetch
      ? ["/parse/functions/queryGroups", { method: "POST", body: { platformName, ownerId: ssoID } }]
      : null,
    fetcherParse2,
    swrConfig
  )
  return {
    gateables: data,
    isLoading: !data && !error && isValidating,
    mutate,
    error,
  }
}

export default useGateablesWeb2
