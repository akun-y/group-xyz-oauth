import { useEffect } from 'react'
import { useRumAction, useRumError } from "@datadog/rum-react-integration"
import useGenMPCUser, { useMPCAccessToken } from 'components/[group]/hooks/useGenMPCUser'
import { useWeb25React } from "components/_app/Web25ReactProvider/Web25React"
import { createStore, del, get, set } from "idb-keyval"
import useSWR, { KeyedMutator, mutate } from "swr"
import useSWRImmutable from "swr/immutable"
import { StorgeItemName, User } from "types"
import { bufferToHex } from "utils/bufferUtils"
import { fetcherMPC } from "utils/fetcher"
import { Validation } from "./useSubmit"
import { useSubmitWithSignWithParamKeyPair } from "./useSubmit/useSubmit"
import useToast from "./useToast"

const {SSOID,SSOTOKEN,USERSSO,MPCTOKEN,ACCOUNT,COTAADDRESS,SSOREFRESHTOKEN,SSONAME}=StorgeItemName
type StoredKeyPair = {
    keyPair: CryptoKeyPair
    pubKey: string
}

const getStore = () => createStore("Web25", "signingKeyPairs")

const getKeyPairFromIdb = (userId: string) => get<StoredKeyPair>(userId, getStore())
const deleteKeyPairFromIdb = (userId: string) => del(userId, getStore())
const setKeyPairToIdb = (userId: string, keys: StoredKeyPair) =>
    set(userId, keys, getStore())

const generateKeyPair = () => {
    try {
        return window.crypto.subtle.generateKey(
            {
                name: "ECDSA",
                namedCurve: "P-256",
            },
            false,
            ["sign", "verify"]
        )
    } catch (error) {
        console.error(error)
        throw new Error("Generating a key pair is unsupported in this browser.")
    }
}

const getKeyPair = async (_: string, id: string) => {
    const keyPairAndPubKey = await getKeyPairFromIdb(id)
    //console.info("useKeyPair-getKeyPair:", id, keyPairAndPubKey)
    if (keyPairAndPubKey === undefined) {
        return {
            keyPair: null,
            pubKey: null,
        }
    }

    return keyPairAndPubKey
}

const setKeyPair = async ({
    ssoID,
    mutateKeyPair,
    validation,
    payload,
    mpcAccessToken,
}: {
    ssoID: string
    validation: Validation
    mutateKeyPair: KeyedMutator<StoredKeyPair>
    payload: StoredKeyPair
    mpcAccessToken: string
}) => {
    // //console.info("useKeyPair-setKeyPair:", account)
    // //console.info("useKeyPair-setKeyPair:", validation)
    // //console.info("useKeyPair-setKeyPair:", payload)
    // //console.info("useKeyPair-setKeyPair:4", mpcAccessToken)
    let json
    try {
        json = await fetcherMPC("/users/browse/pubKey", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + mpcAccessToken
            },
            body: {
                pubKey: payload.pubKey,
                ...validation,
            },
        })
    }
    catch (error) {
        console.error(error)
        throw new Error("Failed to set key pair.")
    }

    //const userId = json?.userId
    ////console.info("useKeyPair-setKeyPair:", userId, json)
    /**
     * This rejects, when IndexedDB is not available, like in Firefox private window.
     * Ignoring this error is fine, since we are falling back to just storing it in
     * memory.
     */
    await setKeyPairToIdb(ssoID, payload).catch(() => { })

    await mutate(`/users/${ssoID}`)
    await mutateKeyPair()

    return payload
}

const checkKeyPair = (
    _: string,
    address: string,
    pubKey: string,
    userId: string,
    mpcAccessToken: string
): Promise<[boolean, string]> =>
    fetcherMPC("/users/browse/checkPubKey", {
        method: "POST", headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + mpcAccessToken
        },
        body: { address, pubKey, userId },
    }).then((result) => [result, userId])

const useKeyPair = () => {
    // Using the defauld Datadog implementation here, so the useDatadog, useUser, and useKeypair hooks don't call each other
    const addDatadogAction = useRumAction("trackingAppAction")
    const addDatadogError = useRumError()
    const { account, mpcToken, ssoID, ssoToken, userSSO, userParse } = useWeb25React()

    const {
        access_token: mpc_access_token,
        isLoading: isMPCLoading } = useMPCAccessToken(ssoToken, userSSO?.name)

    const { data: userMPC, mutate, isLoading, error: mpcError } = useGenMPCUser(mpc_access_token)
    const defaultCustomAttributes = {
        userId: userMPC?._id,
        userAddress: account?.toLowerCase(),
    }
    const {
        data: { keyPair, pubKey },
        mutate: mutateKeyPair,
        error: keyPairError,
    } = useSWR(!!ssoID ? ["keyPair", ssoID] : null, getKeyPair, {
        revalidateOnMount: true,
        revalidateIfStale: true,
        revalidateOnFocus: true,
        revalidateOnReconnect: true,
        refreshInterval: 0,
        fallbackData: { pubKey: undefined, keyPair: undefined },
    })

    const toast = useToast()
    useEffect(() => {
        if (isMPCLoading) return
        if (mpc_access_token)//更新了登录时获取的mpcAccessToken
            window.localStorage.setItem(MPCTOKEN, mpc_access_token)
    }, [isMPCLoading, mpc_access_token])
    const { data: isKeyPairValidData } = useSWRImmutable(
        keyPair && userMPC?._id ? ["isKeyPairValid", account, pubKey, userMPC?._id, mpcToken] : null,
        checkKeyPair,
        {
            fallbackData: [false, undefined],
            revalidateOnMount: true,
            onSuccess: ([isKeyPairValid, userId]) => {
                //console.info("useKeyPair:3", isKeyPairValid, userId)
                if (!isKeyPairValid) {
                    addDatadogAction("Invalid keypair", {
                        ...defaultCustomAttributes,
                        data: { userId, pubKey: keyPair.publicKey },
                    })

                    toast({
                        status: "warning",
                        title: "Session expired",
                        description:
                            "You've connected your account from a new device, so you have to sign a new message to stay logged in",
                        duration: 5000,
                    })

                    deleteKeyPairFromIdb(userId).then(() => {
                        mutateKeyPair({ pubKey: undefined, keyPair: undefined })
                    })
                }
            },
            onError: (error) => {
                console.error("useKeyPair:4", error)
            }

        }
    )

    const setSubmitResponse = useSubmitWithSignWithParamKeyPair<
        StoredKeyPair,
        StoredKeyPair
    >(
        ({ data, validation }) =>
            setKeyPair({ ssoID, mutateKeyPair, validation, payload: data, mpcAccessToken: mpcToken }),
        {
            keyPair,
            forcePrompt: true,
            message:
                "Please sign this message, so we can generate, and assign you a signing key pair. This is needed so you don't have to sign every Group interaction.",
            onError: (error) => {
                console.error("setKeyPair error", error)
                if (error?.code !== 4001) {
                    addDatadogError(
                        `Failed to set keypair`,
                        {
                            ...defaultCustomAttributes,
                            error: error?.message || error?.toString?.() || error,
                        },
                        "custom"
                    )
                }
            },
            onSuccess: (generatedKeyPair) => mutateKeyPair(generatedKeyPair),
        }
    )
    ////console.info("useKeyPair:22", setSubmitResponse,keyPair, pubKey, isKeyPairValidData)

    const ready = !(keyPair === undefined && keyPairError === undefined) || !isLoading

    return {
        ready,
        pubKey,
        keyPair,
        isValid: !!(isKeyPairValidData?.[0]) ?? false,
        set: {
            ...setSubmitResponse,
            onSubmit: async () => {
                const body: StoredKeyPair = {
                    pubKey: undefined,
                    keyPair: undefined,
                }
                try {
                    const generatedKeys = await generateKeyPair()

                    try {
                        const generatedPubKey = await window.crypto.subtle.exportKey(
                            "raw",
                            generatedKeys.publicKey
                        )

                        const generatedPubKeyHex = bufferToHex(generatedPubKey)
                        body.pubKey = generatedPubKeyHex
                        body.keyPair = generatedKeys
                    } catch {
                        throw new Error("Pubkey export error")
                    }
                } catch (error) {
                    if (error?.code !== 4001) {
                        addDatadogError(
                            `Keypair generation error`,
                            {
                                ...defaultCustomAttributes,
                                error: error?.message || error?.toString?.() || error,
                            },
                            "custom"
                        )
                    }
                    throw error
                }
                return setSubmitResponse.onSubmit(body)
            },
        },
    }
}

const manageKeyPairAfterUserMerge = async (fetcherWithSign, prevUser, account) => {
    try {
        const [prevKeys, newUser] = await Promise.all([
            getKeyPairFromIdb(prevUser?.ssoID),
            fetcherWithSign(`/user/details/${account}`, {
                method: "POST",
                body: {},
            }) as Promise<User>,
        ])

        if (prevUser?.ssoID !== newUser?.ssoID && !!prevKeys) {
            await Promise.all([
                setKeyPairToIdb(newUser?.ssoID, prevKeys),
                mutate(["keyPair", newUser?.ssoID], prevKeys),
                mutate(["isKeyPairValid", account, prevKeys.pubKey, newUser?.ssoID], true),
                mutate([`/user/details/${account}`, { method: "POST", body: {} }]),
                deleteKeyPairFromIdb(prevUser?.ssoID),
            ])
        }
    } catch { }
}

export {
    getKeyPairFromIdb,
    setKeyPairToIdb,
    deleteKeyPairFromIdb,
    manageKeyPairAfterUserMerge,
}
export default useKeyPair
