import { bbsGateConfig } from "bbs-gate"
import useSWRImmutable from "swr/immutable"
import { User, UserSSO } from "types"
import fetcher, { fetcherGate } from "utils/fetcher"

const fetchAccessToken = (code: string) => {
    const myHeaders = new Headers()
    myHeaders.append("Cookie", "casdoor_session_id=6c0d1fc96d0fe43964ab14627bae0f86")

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    }
    const param = "grant_type=authorization_code&client_id=" + bbsGateConfig.clientId
        + "&client_secret=" + bbsGateConfig.clientSecret

    return fetcherGate(`/api/login/oauth/access_token?code=${code}&${param}`, requestOptions)


}

const useSSOAccessToken = (code: string): { data: any; isLoading: boolean } => {
    const myHeaders = new Headers()
    myHeaders.append("Cookie", "casdoor_session_id=6c0d1fc96d0fe43964ab14627bae0f86")

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    }
    const param = "grant_type=authorization_code&client_id=" + bbsGateConfig.clientId
        + "&client_secret=" + bbsGateConfig.clientSecret
    const { data, isValidating: isLoading } = useSWRImmutable(
        code ? [`/api/login/oauth/access_token?code=${code}&${param}`,
            requestOptions
        ] : [],
        fetcherGate
        // `http://localhost:8000/api/login/oauth/access_token?code=${code}&${param}`
    )
    //console.info("x==:", data, isLoading)

    return { data, isLoading }
}
export default useSSOAccessToken

export const useUserSSOByCode = async (code: string): Promise<{
    data: UserSSO; isLoading: boolean
    access_token: string; refresh_token: string
}> => {
    const myHeaders = new Headers()
    myHeaders.append("Cookie", "casdoor_session_id=6c0d1fc96d0fe43964ab14627bae0f86")

    const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    }
    const param = "grant_type=authorization_code&client_id=" + bbsGateConfig.clientId
        + "&client_secret=" + bbsGateConfig.clientSecret
    const token = await fetcherGate(`/api/login/oauth/access_token?code=${code}&${param}`, requestOptions)

    if (!token || !token.id_token) {
        console.error("token error:", token)
        return { data: null, isLoading: false, access_token: "", refresh_token: "" }
    }

    const data2: UserSSO = await fetcherGate(`/api/get-account?accessToken=${token?.access_token}`)
    //console.info("y==:", data2)

    return { data: data2, isLoading: false, access_token: token?.access_token, refresh_token: token?.refresh_token }

}
