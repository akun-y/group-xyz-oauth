import { addressToScript } from "@nervosnetwork/ckb-sdk-utils"
import useSWR from "swr"
import { hexToBalance } from "utils/ckb"
const ckbIndexerUrl = process.env.NEXT_PUBLIC_CKB_INDEXER_URL
const getCellsCapacity = async (url: string, script: any) => {
    const res = await fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({
            id: 1000,
            jsonrpc: '2.0',
            method: 'get_cells_capacity',
            params: [
                {
                    script: {
                        code_hash: script.codeHash,
                        hash_type: script.hashType,
                        args: script.args,
                    },
                    script_type: 'lock'
                }
            ]
        })
    })
    const data = await res.json()

    if (res.status !== 200) {
        throw new Error(data.message)
    }
    return data
}
const useNervosBalance = (cotaAddress: string): { data: any; isLoading: boolean } => {
    if (!cotaAddress) {
        return { data: null, isLoading: false }
    }
    const script = addressToScript(cotaAddress)
    const { data, isValidating } = useSWR(() => [ckbIndexerUrl, script], getCellsCapacity)
    const balance = hexToBalance(data?.result?.capacity)
    return { data: balance, isLoading: isValidating }
}
export default useNervosBalance

