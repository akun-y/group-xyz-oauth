import type { NextRequest } from "next/server"
import { NextResponse } from "next/server"

export function middleware(request: NextRequest) {
  const isGroupPage = request.nextUrl.pathname.split("/")?.length <= 2

  if (
    !isGroupPage ||
    request.nextUrl.pathname === request.nextUrl.pathname.toLowerCase()
  )
    return NextResponse.next()

  return NextResponse.redirect(
    new URL(request.nextUrl.origin + request.nextUrl.pathname.toLowerCase())
    //new URL(request.nextUrl.origin + request.nextUrl.pathname)
  )
}

export const config = {
  matcher: "/:path*",
}
